//
//  DGXService.h
//  magiedilatte
//
//  Created by Etinet on 26/01/16.
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGXService : NSObject
@property (strong, nonatomic) NSString *token;

+ (id)sharedManager;

//USER
//-(void)authDevice:(NSString*)deviceId;

-(void)getCustom:(NSString*)model withParams:(NSArray*)params
 onSuccess:(void (^)(NSArray *array))success
 onFailure:(void (^)(NSError *error))failure;

-(void)get:(NSString*)path withParams:(NSArray*)params
                                onSuccess:(void (^)(id object))success
                                onFailure:(void (^)(NSError *error))failure;

-(void)getToken:(NSString*)path withParams:(NSArray*)params onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure;

-(void)post:(NSString*)path body:(NSDictionary*)body onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure;
-(void)postRemoveFavorite:(NSString*)path codArticolo:(NSString*)codArticolo body:(NSDictionary*)body onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure;


-(void)getKromeda:(NSString*)path withParams:(NSArray*)params onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure;
/*
-(void)getCampaignById:(NSString*)campaignId token:(NSString*)token;
-(void)getAllUserCampaigns:(NSArray*)campaignIds token:(NSString*)token;
-(void)shareCampaign:(NSString*)campaignId deviceId:(NSString*)deviceId token:(NSString*)token;
-(void)addFavCampaign:(NSString*)campaignId deviceId:(NSString*)deviceId token:(NSString*)token;
-(void)removeFavCampaign:(NSString*)campaignId deviceId:(NSString*)deviceId token:(NSString*)token;
-(void)getRandomCampaign:(NSString*)token;

//NEWS
-(void)getAllNews:(NSString*)campaignId token:(NSString*)token;

//DONATIONS
-(void)postReservation:(FUGReservation*)reservation;

//COMMONS
-(void)getAllOptions;
-(void)getAllCategories;
*/
@end
