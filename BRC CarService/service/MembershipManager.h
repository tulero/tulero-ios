//
//  MembershipManager.h
//  shooppispesa
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Cart.h"
#import "DGXService.h"

@import UIKit;

@interface MembershipManager : NSObject

@property(nonatomic, strong) User* actualUser;
@property(strong, nonatomic) Cart* cart;
@property(nonatomic, strong) NSMutableArray* pendingRequests;
@property(nonatomic, strong) NSArray*productList;


-(void)initCommonts:(void (^)(NSArray *array))success
          onFailure:(void (^)(NSError *error))failure;

/* **** to copy ***** **/

-(NSDictionary*)getLocalUser;

-(void)signInWithUsername:(NSString *)username password:(NSString *)password isAgente:(BOOL)isAgente rememberMe:(BOOL)rememberMe onSuccess:(void (^)(NSDictionary *array))success
                onFailure:(void (^)(NSError *error))failure;

-(void)loginWithUsername:(NSString *)username password:(NSString *)password remember:(BOOL)rememberMe onSuccess:(void (^)(NSDictionary *array))success
               onFailure:(void (^)(NSError *error))failure;

-(void)addFavorite:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success
               onFailure:(void (^)(NSError *error))failure;

-(void)deleteFavorite:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success
         onFailure:(void (^)(NSError *error))failure;

-(void)searchProductByBRCCode:(NSArray*)codes onSuccess:(void (^)(NSArray *array))success onFailure:(void (^)(NSError *error)) failure;

-(void)searchProductByEANCode:(NSArray*)codes onSuccess:(void (^)(NSArray *array))success onFailure:(void (^)(NSError *error)) failure;

-(void)confirmCorrection:(NSArray*)codes codCliente:(NSString*)codiceCliente rifCliente:(NSString*)rifCliente onSuccess:(void (^)(NSString *array))success onFailure:(void (^)(NSError *error)) failure;

-(void)buyCatalog:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure;

-(void)downloadExpositor:(int)quantity codArticolo:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure;

-(void)passwordRecovery:(NSString*)mail onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure;

-(void)getToken:(void (^)(NSDictionary *dict))success onFailure:(void (^)(NSError *error))failure;
-(void)getPromotions:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getReorderValue:(void (^)(double val))success onFailure:(void (^)(NSError *error))failure;
-(void)getProducts:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getProductById:(NSString*)_id onSuccess:(void (^)(NSArray *object))success onFailure:(void (^)(NSError *error))failure;
-(void)getFavoriteProducts:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getCategoryListCatalogo:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getCategoryListEspositore:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getOrders:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getInventory:(NSString*)client_code onSuccess:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)getOfficineAgente:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure;
-(void)createGuestUser:(NSString *)name surname:(NSString *)surname officina:(NSString*)officina city:(NSString*)city address:(NSString*)address pIva:(NSString*)pIva email:(NSString*)email phone:(NSString*) phone password:(NSString*)password onSuccess:(void (^)(NSString *string))success onFailure:(void (^)(NSError *error))failure;

-(void)loginKromeda:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure;
-(void)getPlate:(NSString*)key plate:(NSString*)plate onSuccess:(void (^)(NSArray *arr))success onFailure:(void (^)(NSError *error))failure;

/********** signin *********/


/* ******** signin after signup *********
-(void)signInAfterSignUpWithUsername:(NSString *)username password:(NSString *)password rememberMe:(BOOL)rememberMe type:(DGXSignUpType)type onSuccess:(void (^)(NSDictionary *array))success
                           onFailure:(void (^)(NSError *error))failure;
**/

/********** signup *********

-(void)signupWithUser:(User *)user andType:(DGXSignUpType)type onSuccess:(void (^)(NSDictionary *array))success
                onFailure:(void (^)(NSError *error))failure;


-(void)signupWithUsername:(NSString *)username password:(NSString *)password firstName:(NSString*)firstName lastName:(NSString*)lastName phoneNumber:(NSString *)phoneNumber city:(NSString *)city  rememberMe:(BOOL)rememberMe type:(DGXSignUpType)type onSuccess:(void (^)(NSDictionary *array))success
                onFailure:(void (^)(NSError *error))failure;

-(void)autologin:(void (^)(NSDictionary *array))success
       onFailure:(void (^)(NSError *error))failure;
    
-(void)logout:(void (^)(NSDictionary *array))success
    onFailure:(void (^)(NSError *error))failure;

-(void)updateUser:(NSDictionary*)user onSuccess:(void (^)(NSDictionary *return_user))success
        onFailure:(void (^)(NSError *error))failure;
 **/

+ (id)sharedManager;

@end
