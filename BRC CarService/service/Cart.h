//
//  Cart.h
//  BRC CarService
//
//  Created by Etinet on 13/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Cart : NSObject
@property (strong, nonatomic) UITabBarItem* tabCartItem;

-(void)addProduct:(NSDictionary*)product;
-(void)removeProduct:(NSDictionary*)product;
-(void)removeAllProducts;
-(NSMutableArray<NSDictionary*>*)getProducts;
-(NSMutableArray*)generateCodes;
//-(NSArray<NSDictionary*>*)generateCodes;
+ (id)sharedCart;

@end
