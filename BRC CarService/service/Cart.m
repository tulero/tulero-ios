//
//  Cart.m
//  BRC CarService
//
//  Created by Etinet on 13/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "Cart.h"
#import "Settings.h"

@implementation Cart{
    NSMutableArray<NSDictionary*>* _products;
}

+ (id)sharedCart {
    static Cart *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (id)init {
    if (self = [super init]) {
        [self initialize];
    }
    
    return self;
}

-(void)initialize {

    [self.tabCartItem setBadgeColor:DEFAULT_COLOR];
}

-(void)addProduct:(NSDictionary *)product {
    if(!_products){
        _products = @[].mutableCopy;
    }
    
    int i = 0;
    BOOL trovato = NO;
    for(NSDictionary* d in _products){
        if([d[@"CodArticolo"] isEqualToString:product[@"CodArticolo"]]){
            if(!d[@"QtaOrder"]){
                NSMutableDictionary* mut = product.mutableCopy;
                mut[@"QtaOrder"] = @(1);
                NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:mut];
                [_products replaceObjectAtIndex:i withObject:dic];
                trovato = YES;
            } else {
                [_products replaceObjectAtIndex:i withObject:product];
                trovato = YES;
            }
        }
        i++;
    }
    if(!trovato){
        if(!product[@"QtaOrder"]){
            NSMutableDictionary* mut = product.mutableCopy;
            mut[@"QtaOrder"] = @(1);
            NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:mut];
            [_products addObject:dic];
            [self.tabCartItem setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)_products.count]];
        } else {
            [_products addObject:product];
            [self.tabCartItem setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)_products.count]];
        }
//        [_products addObject:product];
//        [self.tabCartItem setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)_products.count]];
    }
}

-(void)removeProduct:(NSDictionary*)product{
    [_products removeObject:product];
    if(_products.count != 0) {
        [self.tabCartItem setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)_products.count]];
    } else {
        //[self.tabCartItem setBadgeValue:@""];
        self.tabCartItem.badgeValue = nil;
    }
}

-(void)removeAllProducts{
    [_products removeAllObjects];
    self.tabCartItem.badgeValue = nil;
}

-(NSMutableArray<NSDictionary*>*)getProducts {
    return _products;
}

-(NSMutableArray*)generateCodes{
    NSMutableArray* codes = @[].mutableCopy;
    //[[NSMutableArray alloc] init];
    
//    for(NSDictionary* d in _products){
//        NSMutableArray* codes = @[].mutableCopy;
//
//        for(NSDictionary* elem in self.inventario){
//
//            if(elem[@"Conta"]){
//                [codes addObject:@{
//                                   @"codArticolo":elem[@"CodArticolo"],
//                                   @"qtaprevista":elem[@"QtaEspositore"],
//                                   @"qtaconta": elem[@"Conta"]
//                                   }];
//            }
    for(NSDictionary* d in _products){
        [codes addObject:@{
                           @"codArticolo" : d[@"CodArticolo"],
                           @"quantita" : d[@"QtaOrder"]
                           }];
    }
    return codes;
}
@end
