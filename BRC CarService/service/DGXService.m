//
//  DGXService.m
//  magiedilatte
//
//  Created by Etinet on 26/01/16.
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import "DGXService.h"
#import "Settings.h"
#import <RestKit/RestKit.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

@implementation DGXService {
    RKObjectManager* _objectManager;
    BOOL _networkOk;
    NSUInteger _MAX_PAGE_SIZE;
    
}
    
+ (id)sharedManager {
    static DGXService *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
    
- (id)init {
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

-(void)setXAccessToken:(NSString * )tokenAuth{
    self.token = tokenAuth;

}

-(void)commonInit {
    
}

-(void)getKromeda:(NSString*)path withParams:(NSArray*)params onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString* urlStr = [NSString stringWithFormat:@"%@%@", SERVICE_KROMEDA_URL, path];
    [manager GET: urlStr  parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id result;
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *resultArray = [json objectForKey:@"result"];
        result = resultArray;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(result);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

-(void)getToken:(NSString*)path withParams:(NSArray*)params onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure{
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager.requestSerializer setValue:SERVICE_USER_ID forHTTPHeaderField:@"user"];
    [manager.requestSerializer setValue:SERVICE_PASSWORD forHTTPHeaderField:@"psw"];

    
    [manager GET:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]  parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id result;
        
        if([responseObject isKindOfClass:[NSArray class]]){
            result = [[NSArray alloc] initWithArray:responseObject];
        }else if([responseObject isKindOfClass:[NSDictionary class]]){
            result = [[NSDictionary alloc] initWithDictionary:responseObject];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(result);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

-(void)get:(NSString*)path withParams:(NSArray*)params onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure{
    
    int index = 0;
    
    for(NSString * str in params ){
        if(index == 0){
            path = [path stringByAppendingString:[NSString stringWithFormat:@"?%@", str]];
        }else{
            path = [path stringByAppendingString:[NSString stringWithFormat:@"&%@", str]];
        }
        
        index ++;
    }
     
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    if(self.token){
        [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"tokenID"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    //NSLog(@"LINGUA: %@",language);
    if([language isEqualToString:@"italiano (Italia)"]){
        [manager.requestSerializer setValue:@"ITA" forHTTPHeaderField:@"Language"];
    } else {
        [manager.requestSerializer setValue:@"ENG" forHTTPHeaderField:@"Language"];
    }
    
    
    [manager GET:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]  parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id result;
        
        if([responseObject isKindOfClass:[NSArray class]]){
            result = [[NSArray alloc] initWithArray:responseObject];
        }else if([responseObject isKindOfClass:[NSDictionary class]]){
            result = [[NSDictionary alloc] initWithDictionary:responseObject];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(result);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];

}

-(void)post:(NSString*)path body:(NSDictionary*)body onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure{
    
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    if(self.token){
        [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"tokenID"];
    }
    /*
    if([path isEqualToString:@"/acquisto_catalogo"]){
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    }
     */
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id result;
        responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if([responseObject isKindOfClass:[NSArray class]]){
            result = [[NSArray alloc] initWithArray:responseObject];
        }else if([responseObject isKindOfClass:[NSDictionary class]]){
            result = [[NSDictionary alloc] initWithDictionary:responseObject];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(result);
        });
        
        return;
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
        
    }];
    
}

-(void)postRemoveFavorite:(NSString*)path codArticolo:(NSString*)codArticolo body:(NSDictionary*)body onSuccess:(void (^)(id object))success onFailure:(void (^)(NSError *error))failure{
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    if(self.token){
        [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"tokenID"];
    }
    [manager.requestSerializer setValue:codArticolo forHTTPHeaderField:@"codArticolo"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@", SERVICE_GENERAL_URL, path] parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id result;
        
        if([responseObject isKindOfClass:[NSArray class]]){
            result = [[NSArray alloc] initWithArray:responseObject];
        }else if([responseObject isKindOfClass:[NSDictionary class]]){
            result = [[NSDictionary alloc] initWithDictionary:responseObject];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            success(result);
        });
        
        return;
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
            
        });
        
    }];
    
}


@end
