//
//  MembershipManager.m
//  shooppispesa
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import "MembershipManager.h"
#import "Tolo.h"
#import "AppDelegate.h"
#import "SSKeychain.h"
#import <NSHash/NSString+NSHash.h>
#import <NSHash/NSData+NSHash.h>
#import "Settings.h"
#import "API.h"

static NSString *const kSSKeychainServiceName = @"BRC-Service";
static NSString *const kSSKeychainLabel = @"BRC-Credentials";
static NSUInteger const kMaxAuthRequestErrors = 4;

@interface MembershipManager()

@end

@implementation MembershipManager {
    
    //AuthResponse* _authResponse;
    //BOOL _isRegistered;
    
    UIViewController* _loginController;

    NSUInteger _nAuthReqErrors;
    BOOL _isFromSignup;
    BOOL _isFromAutologin;
    BOOL _isFromSignIn;
    DGXService *_dgxService;
    NSArray* _tempOrders;
}

+ (id)sharedManager {
    static MembershipManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        //self.channels = @[];
        //self.categories = @[];
        
        [self initialize];
    }
    
    return self;
}

-(void)initialize
{
    REGISTER();
    _dgxService = [DGXService sharedManager];
    _cart = [Cart sharedCart];
    
    _pendingRequests = [NSMutableArray new];
    
    // FIXME: to get from local storage
    //GET ACTUAL USER FROM LOCAL STORAGE IF EXISTS
    _actualUser = [[User alloc] init];
    
}

-(void)initCommonts:(void (^)(NSArray *array))success
          onFailure:(void (^)(NSError *error))failure{
    
    MembershipManager *_self = self;
    
    
    [self getToken:^(NSDictionary *dict) {
        success(@[]);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
    /*
    
    [_dgxService get:[Category new] withParams:@[DEFAULT_CITY] onSuccess:^(NSArray *array) {
        _self.categories = [[NSArray alloc] initWithArray:array];
    } onFailure:^(NSError *error) {
        NSLog(@"Error getting categories");
    }];
    
    [_dgxService get:[Channel new] withParams:@[DEFAULT_CITY] onSuccess:^(NSArray *array) {
        _self.channels = [[NSArray alloc] initWithArray:array];
        success(_self.channels);
    } onFailure:^(NSError *error) {
        failure(error);
        NSLog(@"Error getting channels");
    }];
    */
}

#pragma User Address
-(void)removeUserAddressAtIndex:(NSUInteger)index {
    
}

#pragma DGX BACK-END

-(void)loginKromeda:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure {
    NSString* _path = [LOGIN_KROMEDA stringByReplacingOccurrencesOfString:@"user" withString:SERVICE_KROMEDA_USERNAME];
    _path = [_path stringByReplacingOccurrencesOfString:@"pwd" withString:SERVICE_KROMEDA_PASSWORD];
    
    [_dgxService getKromeda:_path withParams:nil onSuccess:^(id object) {
        
        NSArray* result  = [[NSArray alloc] initWithArray:object];
        if([result[0] isEqualToString:@""]){
            success(result[1]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSLoginKromedaError" code:888 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)getPlate:(NSString*)key plate:(NSString*)plate onSuccess:(void (^)(NSArray *arr))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [SEARCH_PLATE stringByReplacingOccurrencesOfString:@":key" withString:key];
    _path = [_path stringByReplacingOccurrencesOfString:@":plate" withString:plate];
    
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    //NSLog(@"LINGUA: %@",language);
    if([language isEqualToString:@"italiano (Italia)"]){
        _path = [_path stringByReplacingOccurrencesOfString:@":lang" withString:@"ITA"];
    } else {
        _path = [_path stringByReplacingOccurrencesOfString:@":lang" withString:@"ENG"];
    }
    
    [_dgxService getKromeda:_path withParams:nil onSuccess:^(id object) {
        
        NSArray* result  = [[NSArray alloc] initWithArray:object];
        if([result[0] isEqualToString:@""]){
            success(result[1][@"vehicles"]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSLoginKromedaError" code:888 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getToken:(void (^)(NSDictionary *dict))success onFailure:(void (^)(NSError *error))failure{
    
    [_dgxService getToken:GET_TOKEN withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        
        if(result[@"tokenID"]){
            _dgxService.token = result[@"tokenID"];
            success(result);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSToken" code:101 userInfo:@{@"msg":@"NO TOKEN FOUNDED"}]);
        }
    
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)signInWithUsername:(NSString *)username password:(NSString *)password isAgente:(BOOL)isAgente rememberMe:(BOOL)rememberMe onSuccess:(void (^)(NSDictionary *array))success
                onFailure:(void (^)(NSError *error))failure{
    
    _isFromSignIn = YES;
    
    //[self loginWithUsername:username password:password isAgente:isAgente rememberMe:rememberMe onSuccess:success onFailure:failure];
}

-(void)loginWithUsername:(NSString *)username password:(NSString *)password remember:(BOOL)rememberMe onSuccess:(void (^)(NSDictionary *array))success
               onFailure:(void (^)(NSError *error))failure{
    if (!_actualUser)
        _actualUser = [User new];
    
    _actualUser.username = username;
    
    if(!username || !password){
        failure([[NSError alloc] initWithDomain:@"NSToken" code:102 userInfo:@{@"msg":@"PARAMETERS NULL"}]);
    }
    
    [_dgxService post:USER_LOGIN body:@{
                                        @"mail": username,
                                        @"password": password
                                        } onSuccess:^(id object) {

        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        
        if(result[@"data"] && [result[@"data"] isKindOfClass:[NSDictionary class]]){
            self.actualUser = [[User alloc] initWithDictionary:result[@"data"]];
            self.actualUser.isRememberMe = rememberMe;
            NSMutableDictionary* d = [[NSMutableDictionary alloc] initWithDictionary:result[@"data"]];
            [d setObject:@(rememberMe) forKey:@"isRememberMe"];
            
            NSUserDefaults* userDef = [NSUserDefaults standardUserDefaults];
            [userDef setObject:d forKey:@"user"];
            [userDef synchronize];
            //NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
            success(result);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSLogin" code:101 userInfo:@{@"msg":@"MAIL/PASSWORD WRONG"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)passwordRecovery:(NSString*)mail onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure{
    [_dgxService post:PASSWORD_RECOVERY body:@{
                                               @"mail": mail//self.actualUser.codcliente
                                          } onSuccess:^(id object) {
                                              NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                              if(result[@"data"]){
                                                  success(result[@"data"]);
                                              } else {
                                                  failure([[NSError alloc] initWithDomain:@"NSRecoveryPassword" code:149 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                              }
                                          } onFailure:^(NSError *error) {
                                              failure(error);
                                          }];
}

/**
 Prende la lista delle promozioni
 **/
-(void)getPromotions:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure{

    NSString* _path = [PROMOTION_LIST stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];

    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSPromoListError" code:88 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)getReorderValue:(void (^)(double val))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [REORDER_VALUE stringByReplacingOccurrencesOfString:@":client_code" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([result[@"data"] doubleValue]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSPromoListError" code:88 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

/**
 Prende la lista dei prodotti
 **/
-(void)getProducts:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure {
    NSString* _path = [PRODUCTS_LIST stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];
    
    if(self.productList){
        success(self.productList);
        return;
    }
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            self.productList = [[NSArray alloc] initWithArray:result[@"data"]];
            
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSProductListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getProductById:(NSString*)_id onSuccess:(void (^)(NSArray *object))success onFailure:(void (^)(NSError *error))failure{

    [self getProducts:^(NSArray *list) {
        
        NSPredicate *predicateToShow = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            if( [[evaluatedObject[@"CodBreve"] stringValue] isEqualToString:_id]){
                return YES;
            }
            return NO;
        }];
        
        NSArray  *filterd = [list filteredArrayUsingPredicate:predicateToShow];
        if(filterd.count > 0){
            success(filterd);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSFilterNotWorking" code:10123 userInfo:@{}]);
        }
        
    } onFailure:failure];
}

-(void)getFavoriteProducts:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure {
    NSString* _path = [FAVORITE_PRODUCTS_LIST stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSFavoriteProductListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getCategoryListCatalogo:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [LISTA_CATEGORIE_CATALOGO stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSProductListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getCategoryListEspositore:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [LISTA_CATEGORIE_ESPOSITORE stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSProductListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getOrders:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [ORDERS_LIST stringByReplacingOccurrencesOfString:@":client_code" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSOrdersListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getOfficineAgente:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure {
    NSString* _path = [WORKSHOP_AGENT_LIST stringByReplacingOccurrencesOfString:@":user_id" withString:self.actualUser.codcliente];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSWorkshopAgentListError" code:85 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getInventory:(NSString*)client_code onSuccess:(void (^)(NSArray *list))success onFailure:(void (^)(NSError *error))failure{
    NSString* _path = [INVENTARIO stringByReplacingOccurrencesOfString:@":client_code" withString:client_code];
    
    [_dgxService get:_path withParams:nil onSuccess:^(id object) {
        
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success([[NSArray alloc] initWithArray:result[@"data"]]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSInventoryError" code:347 userInfo:@{@"msg": @"No data result"}]);
        }
        
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)createGuestUser:(NSString *)name surname:(NSString *)surname officina:(NSString*)officina city:(NSString*)city address:(NSString*)address pIva:(NSString*)pIva email:(NSString*)email phone:(NSString*) phone password:(NSString*)password onSuccess:(void (^)(NSString *string))success onFailure:(void (^)(NSError *error))failure{
    
    [_dgxService post:CREATION_USER body:@{
                                           @"nome": name,
                                           @"cognome": surname,
                                           @"nomeAlias": officina,
                                           @"citta": city,
                                           @"indirizzo": address,
                                           @"telefono": phone,
                                           @"pIva": pIva,
                                           @"mail": email,
                                           @"password": password
                                        } onSuccess:^(id object) {
                                            
                                            NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                            
                                            if(result[@"data"]){
                                                success(result[@"data"]);
                                            }else{
                                                failure([[NSError alloc] initWithDomain:@"NSToken" code:101 userInfo:@{@"msg":@"NO TOKEN FOUNDED"}]);
                                            }
                                            
                                        } onFailure:^(NSError *error) {
                                            failure(error);
                                        }];
}

-(void)addFavorite:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure{
    
    [_dgxService post:ADD_FAVORITE body:@{
                                           @"codCliente": self.actualUser.codcliente,
                                           @"codArticolo": codArticolo
                                           } onSuccess:^(id object) {
                                               NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                               if(result[@"data"]){
                                                   success(result[@"data"]);
                                               }else{
                                                   failure([[NSError alloc] initWithDomain:@"NSAddFavorite" code:148 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                               }
                                           } onFailure:^(NSError *error) {
                                               failure(error);
                                           }];
}

-(void)deleteFavorite:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure {
    NSString* _path = [REMOVE_FAVORITE stringByReplacingOccurrencesOfString:@":client_code" withString:self.actualUser.codcliente];
    [_dgxService postRemoveFavorite:_path codArticolo:codArticolo body:nil onSuccess:^(id object) {
        NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
        if(result[@"data"]){
            success(result[@"data"]);
        }else{
            failure([[NSError alloc] initWithDomain:@"NSRemoveFavorite" code:149 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
        }
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)searchProductByBRCCode:(NSArray*)codes onSuccess:(void (^)(NSArray *array))success onFailure:(void (^)(NSError *error)) failure{
    [_dgxService post:SEARCH_BRC_CODE body:@{
                                          @"codCliente": self.actualUser.codcliente,
                                          @"codici": codes
                                          } onSuccess:^(id object) {
                                              NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                              if(result[@"data"]){
                                                  success(result[@"data"]);
                                              }else{
                                                  failure([[NSError alloc] initWithDomain:@"NSSearchBRCCodeError" code:172 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                              }
                                          } onFailure:^(NSError *error) {
                                              failure(error);
                                          }];
}

-(void)searchProductByEANCode:(NSArray*)codes onSuccess:(void (^)(NSArray *array))success onFailure:(void (^)(NSError *error)) failure{
    [_dgxService post:SEARCH_EAN_CODE body:@{
                                             @"codCliente": self.actualUser.codcliente,
                                             @"codici": codes
                                             } onSuccess:^(id object) {
                                                 NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                                 if(result[@"data"]){
                                                     success(result[@"data"]);
                                                 }else{
                                                     failure([[NSError alloc] initWithDomain:@"NSSearchEANCodeError" code:173 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                                 }
                                             } onFailure:^(NSError *error) {
                                                 failure(error);
                                             }];
}

-(void)confirmCorrection:(NSArray*)codes codCliente:(NSString*)codiceCliente rifCliente:(NSString*)rifCliente onSuccess:(void (^)(NSString *array))success onFailure:(void (^)(NSError *error)) failure{
    [_dgxService post:CONFERMA_RETTIFICHE body:@{
                                             @"codCliente": codiceCliente,
                                             @"rifCliente": rifCliente,
                                             @"rifAgente": self.actualUser.username,
                                             @"codici": codes
                                             } onSuccess:^(id object) {
                                                 NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                                 if(result[@"data"]){
                                                     success(result[@"data"]);
                                                 }else{
                                                     failure([[NSError alloc] initWithDomain:@"NSRettificaCodeError" code:173 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                                 }
                                             } onFailure:^(NSError *error) {
                                                 failure(error);
                                             }];
}

-(void)buyCatalog:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure{
    
    NSMutableArray* codes = [[NSMutableArray alloc] initWithArray: [self.cart generateCodes]];
    //NSMutableArray* codici = [[NSMutableArray alloc] initWithArray:codes];
    
    NSDictionary* tmp = @{
                          @"codCliente": @([self.actualUser.codcliente intValue]),
                          @"codici": codes
                          };
    
    [_dgxService post:BUY_CATALOG body:@{
                                         @"codCliente": @([self.actualUser.codcliente intValue]),
                                         @"codici": [[NSArray alloc] initWithArray:codes]
                                         } onSuccess:^(id object) {
                                             NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                             if(result[@"data"]){
                                                 success(result[@"data"]);
                                             }else{
                                                 failure([[NSError alloc] initWithDomain:@"NSBuyCatalogError" code:177 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                             }
                                         } onFailure:^(NSError *error) {
                                             failure(error);
                                         }];
}

-(void)downloadExpositor:(int)quantity codArticolo:(NSString*)codArticolo onSuccess:(void (^)(NSString *str))success onFailure:(void (^)(NSError *error))failure{
    [_dgxService post:DOWNLOAD_EXPOSITOR body:@{
                                         @"codCliente": self.actualUser.codcliente,
                                         @"quantita": @(quantity),
                                         @"codArticolo": codArticolo
                                         } onSuccess:^(id object) {
                                             NSDictionary* result  = [[NSDictionary alloc] initWithDictionary:object];
                                             if(result[@"data"]){
                                                 success(result[@"data"]);
                                             }else{
                                                 failure([[NSError alloc] initWithDomain:@"NSBuyCatalogError" code:177 userInfo:@{@"msg":@"PARAMETERS NO VALID"}]);
                                             }
                                         } onFailure:^(NSError *error) {
                                             failure(error);
                                         }];
}

#pragma INTERNAL METHODS
-(NSDictionary*)getLocalUser {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"user"];
}


-(void)updateLocalUser {
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     //[defaults setObject:[_actualUser convertIntoDictionary] forKey:@"user"];
     [defaults synchronize];
    
}

-(void)updateDevices{
    
}

-(void)performInvocations:(NSMutableArray*)invocations {
    for (NSInvocation* inv in invocations) {
        [inv invoke];
    }
}

-(NSString*)encryptString:(NSString*)string {
    NSString* str = string;
    if (_isFromSignup || _isFromSignIn) {
        //Password encryption...
        NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding];
        str = [[data SHA256] base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
    return str;
}

#pragma COMMUNICATIONS

-(void)initSocketCommunication:(NSString*)token {
}

-(void)subscribeUserForRemoteNotifications {

    //[[PushNotificationManager pushManager] registerForPushNotifications];
}

-(void)unsubscribeUserForRemoteNotifications {
    
    //[[PushNotificationManager pushManager] unregisterForPushNotifications];
    
    //register for push to our BE
    //[_dgxService deleteDeviceId:_actualUser.deviceToken userId:_actualUser._id locale:_localStoreManager.userPreferences.preferredLanguage token:_actualUser.token];
}
static id ObjectOrEmpty(id object){
    return object ? : @[];
}
@end
