//
//  AppDelegate.h
//  BRC CarService
//
//  Created by Etinet on 25/01/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#define APP_DELEGATE ((AppDelegate *)[UIApplication sharedApplication].delegate)
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, strong) UIStoryboard* mainStoryboard;
@property (retain, strong) NSString* globalToken;

-(void)setRootViewControllerByName:(NSString*)vcName;

-(void)sendPageViewGA:(NSString*) page;

@end

