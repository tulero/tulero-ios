//
//  AppDelegate.m
//  BRC CarService
//
//  Created by Etinet on 25/01/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashVC.h"
#import <Google/Analytics.h>
@import Firebase;
@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    _mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    [[UINavigationBar appearance] setTranslucent:NO];
    
    [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([SplashVC class])];
    [FIRApp configure];
    [self setupAnalytics:application options:launchOptions];
    
    /*
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:MESTRUAZIONI_COLOR];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    */
    
    // set custom delegate for push handling, in our case AppDelegate
    /*
    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
    pushManager.delegate = self;
    
    // set default Pushwoosh delegate for iOS10 foreground push handling
    if (@available(iOS 10.0, *)) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = [PushNotificationManager pushManager].notificationCenterDelegate;
    } else {
        // Fallback on earlier versions
    }
    
    memberShip = [MembershipManager sharedManager];

    // track application open statistics
    [[PushNotificationManager pushManager] sendAppOpen];
    
    // register for push notifications!
    [[PushNotificationManager pushManager] registerForPushNotifications];
    
    [self setupAnalytics:application options:launchOptions];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    [self loadFonts];
     */
    
    return YES;
}

-(void)setupAnalytics:(UIApplication*)application options:(NSDictionary*)options {
    
    /*
     [Fabric with:@[[Crashlytics class]]];
     NSLog(@"Fabric Crashlitycs initialized.");
     */
    /*
     NSError* configureError;
     [[GGLContext sharedInstance] configureWithError: &configureError];
     NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
     
     [GIDSignIn sharedInstance].delegate = self;
     
     NSLog(@"Google Analitycs initialized");
     */
    
    
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-121728326-1"];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = YES;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)setRootViewControllerByName:(NSString *)vcName {
    
    id vc = [_mainStoryboard instantiateViewControllerWithIdentifier:vcName];
    [self setRootViewController:vc];
}

-(void)setRootViewController:(UIViewController*)viewController {
    [_window setRootViewController:viewController];
    _window.backgroundColor = [UIColor blackColor];
    [_window makeKeyAndVisible];
    
    [UIView transitionWithView:_window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}


-(void)sendPageViewGA:(NSString*) page{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:page];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end

