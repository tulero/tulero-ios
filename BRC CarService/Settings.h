//
//  Settings.h
//  CommunitySmart
//
//  Created by Gianluca Tursi on 26/11/2017.
//  Copyright © 2017 Gianluca Tursi. All rights reserved.
//

#ifndef Settings_h
#define Settings_h

/*PROD*/

#define SERVICE_IMAGE_URL @"http://www.brccarservice.com"
#define SERVICE_IMAGE_UPLOAD @"https://claudiaimages.azurewebsites.net/api/users/5a059de2443979eb1e266f28/projects/5a05b760045c15349d78f61d/images"
//#define SERVICE_GENERAL_URL @"https://communitysmart-services.azurewebsites.net/api/v1"
//#define SERVICE_GENERAL_URL @"http://test.brccarservice.com/WebAPI/AppServices"
#define SERVICE_GENERAL_URL @"https://www.brccarservice.com/WebAPI/AppServices"

//#define SERVICE_BRC_URL @"http://test.brccarservice.com"
//#define SERVICE_BRC_URL2 @"http://test.brccarservice.com/"

#define SERVICE_BRC_URL @"http://brccarservice.com"
#define SERVICE_BRC_URL2 @"http://brccarservice.com/"

#define SERVICE_KROMEDA_URL @"http://80.94.117.223/ws/krwsrest_v12.dll/datasnap/rest/tkrm"
#define SERVICE_KROMEDA_USERNAME @"b2c-brc"
#define SERVICE_KROMEDA_PASSWORD @"brc923632"

#define SERVICE_USER_ID @"u_digitalX"
#define SERVICE_PASSWORD @"rDY4VhXunk"

/*******/

#define SPLASH_IMAGE @"splash"
#define ABUSE_MAIL @"info@info.it"
//#define ASSISTENZA_MAIL @"info@info.it"

#define ASSISTENZA_MAIL @"info@brc.it"
#define ASSISTENZA_NUMBER @"tel:017248681"

#define ESITO_RETTIFICA @"The inventory has been correctly registered but must be approved by the BRC Car Service Back Office."
//#define RESET_PWD_URL @"http://specchiodeitempi.digx.it/reset-password"
//#define ASSISTENZA_PHONE @"+39017248681"

/********* MORE URLS *******
#define SHARE_BASE_URL @"https://communitysmart-services.azurewebsites.net/article/"
#define DOMANDE_FAQ @"https://communitysmart.it/app/domande-frequenti"
#define RICHIED_ASSISTENZA @"https://communitysmart.it/app/assistenza/?app=savigliano"
#define COME_FARE_PUB @"https://communitysmart.it/app/adv/?app=savigliano"
#define SEGUICI_FACEBOOK @"https://www.facebook.com/saviglianosmart/"
#define APP_COMMUNITY @"https://communitysmart.it/app/"
#define PRIVACY_URL @"https://communitysmart.it/app/legal/?app=savigliano"
**/

#define DISCLAIMER_URL @"http://brc.digx.it/tos"

#define COLOR(r,g,b,a) ([UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a])

//user=email@email.com
//#define RESTORE_PASSWORD @"https://community-admin.azurewebsites.net/restore.html"

#define PRIMARY_COLOR COLOR(233, 71, 75, 1)
#define SECONDARY_COLOR COLOR(62,109,195,1)
#define TEXT_BROWN_COLOR COLOR(60, 47, 47,1)
#define EXPIRE_COLOR COLOR(101,181,65,1)
#define WIZARD_COLOR_BUTTON COLOR(99,197,28,1)

#define DEFAULT_COLOR [UIColor colorWithRed:0.82 green:0.21 blue:0.33 alpha:1.00]

#define COLOR_BLACK COLOR (146, 154, 156, 1)
#define COLOR_GREY COLOR (146, 154, 156, 1)
#define COLOR_LIGHT_GREY COLOR (232, 238, 239, 1)
#define COLOR_GREEN COLOR (9, 178, 84, 1)
#define COLOR_BROWN COLOR(60, 47, 47,1)
#define COLOR_RED COLOR (233, 71, 75, 1)
#define COLOR_YELLOW COLOR (239, 174, 0, 1)
#define COLOR_WHITE COLOR (255, 255, 255, 1)
#define COLOR_BLUE COLOR (15, 143, 214, 1)

#define MESTRUAZIONI_COLOR COLOR (156, 38, 61, 1)

#define COLOR_GREEN_SUCCESS COLOR (135,198,102, 1)
#define COLOR_RED_FAIL COLOR (242,74,80,1)
#define SCENARY_GREEN_COLOR COLOR(135, 198, 102, 1)

#define NO_IMAGE @"no-image"
#define STORE_URL @"https://itunes.apple.com/app/id1126382641"
#define SITE_URL @"http://www.brc.it"
#define SITE_URL_PROMO @"http://www.brccarservice.com"
#define SITE_URL_PROMO2 @"http://www.brccarservice.com/"
#define KEYCHAIN_CREDENTIAL_NAME @"BrcCredentials"
#define GOOGLE_MAPS_SDK_KEY @"AIzaSyBEG0aE45YMZ9s49xBFbOsjUycBboz_ikk"
#define GOOGLE_STREET_MAPS_SDK_KEY @"AIzaSyDZQxk0PaByh0L2yijmKi22A7vuUryQPH8"
#define MIXPANEL_TOKEN @"c1cfe173fbbb1587cb13867643ccb973"
#define SERVICE_DONATION_URL @""

#define FONT_LIGHT @"ProximaNova-Light"
#define FONT_REGULAR @"ProximaNova-Regular"
#define FONT_MEDIUM @"ProximaNova-Semibold"
#define FONT_BOLD @"ProximaNova-Bold"

#define kPOSITION @""
#define kFACEBOOK @"https://www.facebook.com/communitysmart/"
#define kTWITTER @"https://twitter.com/communitysmart"
#define kYOUTUBE @"https://www.youtube.com/user/communitysmart"
#define kINSTAGRAM @"https://www.instagram.com/communitysmart/"
#define kLOCALE [[[NSLocale currentLocale] localeIdentifier] componentsSeparatedByString:@"_"][0]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define k_KEYBOARD_OFFSET 80.0

#define COLOR(r,g,b,a) ([UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a])

#import "AppDelegate.h"
//#import "Tolo.h"
//#import "Utils.h"
#import <UIColor+HexString.h>
#import "KVNProgress.h"
#import "Utils.h"
#import "UIColor+HexString.h"
//#import <Google/Analytics.h>
#endif /* Settings_h */

