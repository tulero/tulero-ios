/* localizable.strings
 Copyright © 2016 DigitalX srl. All rights reserved. */

/* *************** MODAL LOGIN *************** */
"LOGIN_MODAL_TITLE" = "Welcome!";
"LOGIN_MODAL_DESCR" = "With BRC Car Service app, you can explore the complete BRC catalog, replenish your workshop and check the status of your orders.";
"LOGIN_MODAL_BTN" = "START";
"LOGIN_MODAL_AGENTE_DESCR" = "With BRC Car Service app, you can quickly refill BRC exhibitors inventory and order news spare parts.";
"LOGIN_MODAL_GUEST_TITLE" = "Thank you!";
"LOGIN_MODAL_GUEST_DESCR" = "Benvenuto in BRC CAR SERVICE. By now you are able to explore the catalog and put an order that will be verified by our staff. ";
"LOGIN_MODAL_GUEST_BTN" = "CONTINUE";
"REGISTRATION_ERROR" = "You entered invalid values!";

"LOGIN_PRIVACY" = "You must read and accept services terms of use.";

"TITLE_CATALOG" = "CATALOG";

"TITLE_CART" = "CART";
"EMPTY_CART" = "Your shopping cart is empty";
"EMPTY_CART_DESCR" = "Add catalog products to complete the order.";

"TITLE_ORDER" = "ORDERS";
"EMPTY_ORDER" = "No orders placed";
"EMPTY_ORDER_DESCR" = "Past orders will appear here when a purchase is completed";

"TITLE_EXPOSITOR" = "EXHIBITOR";
"EMPTY_EXPOSITOR" = "Inactive Exhibitor";
"EMPTY_EXPOSITOR_DESCR" = "Your account does not currently provide the exhibitor management provided by BRC CAR SERVICE.\n\nFind out the advantages for your workshop by contacting our support staff.";

"TITLE_PROMOTION" = "OFFERS";
"EMPTY_PROMOTION" = "No offers available";
"EMPTY_PROMOTION_DESCR" = "No offer is currently active. Come back soon!";

"GENERIC_ERROR" = "Ooops! Something went wrong. Please try again later.";

"PROMO_TIME" = "Offer valid until: ";
"PROMO_TIME_DETAIL" = "Offer valid until stocks are exhausted.";
"PROMO_DETAIL_TITLE" = "OFFER DETAIL";

"PUBLICATION_DATE" = "Publication date:";
"SITE_LINK" = "Website link:";
"PDF_LINK" = "Download PDF:";

"CALL" = "CALL NOW";

"STATUS_SHIPPED" = "SHIPPED";
"STATUS_RECEIVED" = "PROCESSING";
"STATUS_SHIPPING_PROGRESS" = "ACCEPTED";

"INFO_ORDER" = "€ %0.2f - %@ items";

"USER_DATA" = "USER DATA";
"HELP" = "HELP DESK";
"HELP_DESCR1" = "See our ";
"HELP_DESCR2" = "sale conditions";
"HELP_DESCR3" = " and privacy ";
"HELP_DESCR4" = "policy";

"TITLE_SEARCH" = "SEARCH";
"TITLE_SEARCH_CAR_LICENSE_PLATE_1" = "License Plate Scanner";
"TITLE_SEARCH_CAR_LICENSE_PLATE_2" = "License Plate Input";
"TITLE_SEARCH_EAN_1" = "EAN Scanner";
"TITLE_SEARCH_EAN_2" = "EAN Input";
"TITLE_SEARCH_CODE" = "Type a product code";
"TITLE_SEARCH_MANUAL" = "Manual Search";
"SEARCH_DESCR_CAR_LICENSE_PLATE_1" = "Use the camera to get info about vehicle";
"SEARCH_DESCR_CAR_LICENSE_PLATE_2" = "Enter vehicle license plate number";
"SEARCH_TARGA_DESCR" = "Enter 7 characters that are shown on the license plate to get vehicle info.";
"SEARCH_DESCR_EAN_1" = "Use the camera as a barcode reader.";
"SEARCH_DESCR_EAN_2" = "Manually enter the product EAN code.";
"SEARCH_EAN_DETAIL" = "Enter 13 characters that are shown on the EAN code to get product info.";
"SEARCH_DESCR_CODE" = "Manually enter the BRC or other brand product code.";
"SEARCH_DESCR_MANUAL" = "Enter vehicle name to find compatible products.";
"SEARCH_CODE_DETAIL" = "Insert the BRC or other brand product code.";
"SEARCH_TITLE_SECTION_CODE" = "SEARCH BY CODE";
"SEARCH_TITLE_SECTION_CAR_LICENSE_PLATE" = "SEARCH BY LICENSE PLATE(%d remaining uses)";
"SEARCH_TITLE_SECTION_MANUAL" = "SEARCH BY VEHICLE";
"SEARCH_TITLE_SECTION_EAN" = "SEARCH BY EAN CODE";
"TITLE_SEARCH_LICENSE_PLATE_MANUAL" = "ENTER LICENSE PLATE";
"TITLE_SEARCH_EAN_MANUAL" = "ENTER EAN CODE";
"TITLE_SEARCH_BRC_CODE" = "ENTER BRC CODE";
"TITLE_SEARCH_BRAND" = "BRAND";
"TITLE_SEARCH_MODEL" = "MODEL";
"TITLE_SEARCH_VERSION" = "VERSION";

"ASSISTENZA_DESCR" = "Any problem? Contact us by phone or mail!";
"ASSISTENZA_CALL" = "Call now";
"ASSISTENZA_MSG" = "Type a message";

"TITLE_WORKSHOP_AGENT" = "WORKSHOPS";

"FAVORITE_LIST_TITLE" = "FAVORITE LIST";
"FAVORITE_PRODUCT_CODE" = "Code: ";
"FAVORITE_PRODUCT_EAN" = "EAN: ";
"FAVORITE_PRODUCT_PRICE" = "€ ";

"BTN_ESPOSITORE" = "EXHIBITOR";
"BTN_CATALOGO" = "CATALOG";

"ORGANIZATION_DETAIL" = "BRC Car Service";
"DETAIL_PRODUCT" = "PRODUCT DETAIL";
"CATEGORY_LABEL" = "Category: ";
"CODE_LABEL" = "Code: ";
"EAN_LABEL" = "EAN: ";
"DELIVERY_LABEL" = "Delivery: ";
"DELIVERY_TIME" = "%@ hours";
"DISCOUNTS_LABEL" = "Discounts: ";
"POSITION_LABEL" = "Position: ";
"POSITION_DETAIL" = "CATALOG";
"POSITION_DETAIL_EXPOSITOR" = "EXHIBITOR";

"ORDER_NUMBER" = "N. %@";
"ORDER_ID" = "Order: %@";
"ORDER_PRICE_TOT" = "€ %0.2f - %@ products - %@";
"STATUS_TITLE" = "Status: %@";
"ORDER_INFO" = "€ %@ (%d items)";
"ORDER_INFO_2" = "€ %0.2f (%d items)";
"ORDER_ID_TITLE" = "ORDINE %@";
"QUANTITY_ORDER" = "Order quantity: ";
"FULL_PRICE_ORDER" = "Order amount: ";
"QUANTITY_ORDERED" = "%d items";
"QUANTITY_ORDERED_PRICE" = "€ %0.2f";
"QUANTITY_ORDERED_PRICE_2" = "€ %@";

"INVENTORY_PRODUCT_CODE" = "Code: %@";
"INVENTORY_PRODUCT_EAN" = "EAN: %@";
"INVENTORY_TITLE" = "INVENTORY";

"CODE_INPUT_ERROR" = "Invalid code entered";
"CODE_NOT_FOUND" = "The code you entered did not match to any products";
"PRODUCTS_NOT_FOUND" = "No products found for the searched vehicle model";
"PRODUCTS_NOT_FOUND2" = "No products found at the moment for the searched vehicle model";
"PRODUCTS_FOUND" = "AVAILABLE PRODUCTS";

"VARIAZIONE_RETTIFICA" = "Change:";
"VARIAZIONE_RETTIFICA_NON_DISPONIBILE" = "Change: N.D.";
"RETTIFICA_TITLE" = "CORRECTIONS CONFIRMATION";

"EXPECTED_QUANTITY" = "EXPECTED:";
"COUNTED_QUANTITY" = "COUNTS:";
"VARIATION_QUANTITY" = "CHANGE:";

"OFFICINE" = "Workshops";

"SCANNER_TITLE" = "BARCODE SCANNER";
"SCANNER_DESCR_1" = "Searching for a compatible barcode";
"SCANNER_DESCR_2" = "Move your device close to a barcode";
"SCANNER_DESCR_3" = "Find barcodes over items and packages";
"SCANNER_NOT_SUPPORTED" = "Scanner is not supported by device camera";
"SCANNER_BTN_TITLE" = "CONFIRM %@";

"ADD_TO_CART" = "Add to cart € %.2f";
"PRICE_CART" = "€ %0.2f";
"PRICE_CART_IVA" = "€ %0.2f vat included";

"INVIA_ORDINE" = "Confirm the order?";
"DELETE" = "Cancel";
"CONFERMA_ORDINE" = "CONFIRM ORDER";
"ELIMINA_PRODOTTO_DESCR" = "Remove this item from your cart?";
"ELIMINA_PRODOTTO" = "REMOVE ITEM";
"INSERIMENTO_RIUSCITO" = "Item added to your cart";

"DOWNLOAD_COMPLETATO" = "Items successfully unloaded";
"DOWNLOAD_ERROR_QUANTITY" = "%d items are not available in stock at the moment";

"SUBJECT_MAIL" = "BRC CAR SERVICE SUPPORT REQUEST (iOS APP)";
"MAIL_DESCR" = "Explain your problem. Our service will contact you as soon as possible.";
"MAIL_SENDED" = "Email successfully sent";
"MAIL_NOT_ENABLED" = "This device is not configured to send an email!";

"PSW_RECOVERY_NOT_MAIL" = "Insert your email";
"ERRORE_LOGIN" = "Login error";
"ERRORE_CREDENZIALI_LOGIN" = "Wrong email and/or password.";
"LOGIN_INPUT" = "Missing email or password";
"RESET_PSW_DESCR" = "If you don't remember your password, enter the email address of your Car Service account to retrieve it.";
"RESET_PSW_TITLE" = "password lost?";
"BTN_RECUPERO_PSW" = "PASSWORD RECOVERY";

"BTN_CONFERMA" = "CONFIRM";

"ORDINE_INVIATO" = "Check the progress in the Orders tab";
"RETTIFICA_INVIATA" = "Return to the Officine list to continue your tasks";
"ORDINI" = "Orders";

"ESPOSITORE_VALORE_RIORDINO" = "€ %0.2f remainig for exhibitor products free shipping";
"UTENTE_ESISTENTE" = "The email you entered has already been used.";

"EMPTY_CATEGORY_LIST" = "Products not found";
"EMPTY_CATEGORY_LIST_DESCRIPTION" = "Products in this category are currently not available";

"AGGIUNTO_PREFERITO" = "Items added to your favorites";

"TEMPO_PROMO" = "Expires in %0.0f days!";

"RECUPERO_PSW_RIUSCITO" = "Check your email to retrieve your password.";

"ASSISTENZA_INFO" = "Help desk";
"LISTA_PREFERITI" = "Favourite list";

"SCARICO_PROD" = "Unload exhibitor product";
"ACQUISTO_DA_CATALOGO" = "Buy from the catalog";

"BENZINA" = "PETROL";
"DIESEL" = "DIESEL";
"GPL" = "LPG";
"METANO" = "METHANE";

"TERMINI_SERVIZIO" = "terms of service";

"INPUT_TARGA_WRONG" = "The plate must have 7 characters.";
"RICERCA_AUTO_FALLITA" = "No compatible products found for this vehicle.";

"CONDIZIONI_PRIVACY" = "I have read and accepted the terms as expressly indicated in BRC Car Service terms of use.";

"PASSWORD_DIMENTICATA" = "I forgot my password";

"INFO_VEICOLO_BENZINA" = "%@ - Petrol";
"INFO_VEICOLO_DIESEL" = "%@ - Diesel";
"INFO_VEICOLO_METANO" = "%@ - Methane";
"INFO_VEICOLO_GPL" = "%@ - Lpg";

"DETTAGLI_VEICOLO" = "Engine %@ - %@ Valves";
"DETTAGLI_VEICOLI_BODY_BENZINA" = "%@ - %@ doors - Petrol";
"DETTAGLI_VEICOLI_BODY_DIESEL" = "%@ - %@ doors - Diesel";
"DETTAGLI_VEICOLI_BODY_METANO" = "%@ - %@ doors - Methane";
"DETTAGLI_VEICOLI_BODY_GPL" = "%@ - %@ doors - Lpg";
"PRODOTTI_TROVATI" = "AVAILABLE PRODUCTS";
