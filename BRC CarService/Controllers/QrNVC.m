//
//  QrNVC.m
//  arredanet
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import "QrNVC.h"
#import "Settings.h"

@interface QrNVC ()

@end

@implementation QrNVC

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)applyTransparentNavBar {
    
    //Custom BackButton NavBar
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage* backbuttonImage = [[UIImage imageNamed:@"back_white"] imageWithAlignmentRectInsets:insets];
    [self.navigationBar setBackIndicatorImage:backbuttonImage];
    [self.navigationBar setBackIndicatorTransitionMaskImage:backbuttonImage];
    
    [self.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage new]];
    [self.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor whiteColor], NSForegroundColorAttributeName,
                                                [UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil]];
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UINavigationBar class]]] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.1 animations:^{
        self.navigationBar.barStyle = UIStatusBarStyleLightContent;
    }];
    self.navigationBar.translucent = YES;
}

-(void)applyTranslucentNavBar {
    
    //Custom BackButton NavBar
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage* backbuttonImage = [[UIImage imageNamed:@"back_white"] imageWithAlignmentRectInsets:insets];
    [self.navigationBar setBackIndicatorImage:backbuttonImage];
    [self.navigationBar setBackIndicatorTransitionMaskImage:backbuttonImage];
    
    [self.navigationBar setBackgroundColor:PRIMARY_COLOR];
    [self.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    //[self.navigationController.navigationBar setBackgroundImage:[[UINavigationBar appearance] backgroundImageForBarMetrics:UIBarMetricsDefault] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UINavigationBar appearance].shadowImage];
    [self.navigationBar setBarTintColor:PRIMARY_COLOR];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    //[self.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor whiteColor], NSForegroundColorAttributeName,
                                                [UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UINavigationBar class]]] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    
    [UIView animateWithDuration:0.1 animations:^{
        self.navigationBar.barStyle = UIStatusBarStyleLightContent;
    }];
    
    
    self.navigationBar.translucent = NO;
}

-(void)applyWhiteNavBar {
    
    //Custom BackButton NavBar
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage* backbuttonImage = [[UIImage imageNamed:@"back_black"] imageWithAlignmentRectInsets:insets];
    [self.navigationBar setBackIndicatorImage:backbuttonImage];
    [self.navigationBar setBackIndicatorTransitionMaskImage:backbuttonImage];
    
    [self.navigationBar setBackgroundColor:[UIColor whiteColor]];
    [self.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    //[self.navigationController.navigationBar setBackgroundImage:[[UINavigationBar appearance] backgroundImageForBarMetrics:UIBarMetricsDefault] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UINavigationBar appearance].shadowImage];
    [self.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationBar setTintColor:COLOR_BLACK];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                COLOR_BLACK, NSForegroundColorAttributeName,
                                                [UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UINavigationBar class]]] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16 weight:UIFontWeightRegular], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.1 animations:^{
        self.navigationBar.barStyle = UIStatusBarStyleDefault;
    }];
    
    self.navigationBar.translucent = NO;
}

@end
