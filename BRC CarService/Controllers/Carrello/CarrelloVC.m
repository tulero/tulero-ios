//
//  CarrelloVC.m
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "CarrelloVC.h"
#import "Settings.h"
#import "Cart.h"
#import "ProductCell.h"
#import "MembershipManager.h"
#import "DetailProductCatalogoVC.h"
#import "EsitoRettificaVC.h"

@interface CarrelloVC ()

@end

@implementation CarrelloVC{
    MembershipManager* _manager;
    NSMutableArray<NSDictionary*>* _list;
    BOOL _isLoaded;
    double _priceCart;
    double _priceCartWithIva;
    NSString* _acquisto;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = self.btnConferma.frame.size.height/2;
    [self.btnConferma setTitle:NSLocalizedString(@"BTN_CONFERMA", @"BTN_CONFERMA") forState:UIControlStateNormal];
    
    _manager = [MembershipManager sharedManager];
    _isLoaded = NO;
    [self initList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_CART", @"TITLE_CART");
    [APP_DELEGATE sendPageViewGA:@"Cart"];
    [self initList];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(void)initList {
    _list = [_manager.cart getProducts];
    if(_list && _list.count > 0){
        self.emptyCartView.hidden = YES;
        _priceCart = 0;
        for(NSDictionary* d in _list){
            int sconto = 0;
            if(d[@"Sconto"]){
                sconto = [d[@"Sconto"] intValue];
            }
            double prz = [d[@"Prezzo"] doubleValue];
            _priceCart = _priceCart + (prz - (prz * sconto)/100) * [d[@"QtaOrder"] doubleValue];
        }
        _priceCartWithIva = (_priceCart * 22/100) + _priceCart;
        self.price.text = [NSString stringWithFormat:NSLocalizedString(@"PRICE_CART", @"PRICE_CART"),_priceCart];
        self.priceWithIva.text = [NSString stringWithFormat:NSLocalizedString(@"PRICE_CART_IVA", @"PRICE_CART_IVA"),_priceCartWithIva];
    } else {
        self.emptyCartView.hidden = NO;
    }
    _isLoaded = YES;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView reloadData];
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
    [cell configureCarrello:_list[indexPath.row]];
    cell.delegate = self;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    DetailProductCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductCatalogoVC class])];
    
    detail.product = _list[indexPath.row];
    detail.productCategory = _list[indexPath.row][@"Commodity"];
    [self.navigationController pushViewController:detail animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}

#pragma mark DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"carrello_placeholder_grey_icon"];
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = NSLocalizedString(@"EMPTY_CART", @"EMPTY_CART");
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return self.tableView.backgroundColor;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"EMPTY_CART_DESCR", @"EMPTY_CART_DESCR");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
{
    return YES;
}

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return _isLoaded;
}

- (IBAction)acquista:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"INVIA_ORDINE", @"INVIA_ORDINE") preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor redColor];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"DELETE", @"DELETE") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CONFERMA_ORDINE", @"CONFERMA_ORDINE") style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        _acquisto = @"";
    
        [KVNProgress show];
        
        //NSArray* codes = [_manager.cart generateCodes];

        [_manager buyCatalog:^(NSString *str) {
            [KVNProgress dismiss];
            _acquisto = str;
            NSLog(@"%@",str);
            EsitoRettificaVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EsitoRettificaVC"];
            controller.navController = self.navigationController;
            [self presentViewController:controller animated:YES completion:nil];
            [_manager.cart removeAllProducts];
            [self initList];
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    
        }];
    
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma deleteProduct delegate
-(void)deleteProduct:(NSDictionary*)prod {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"ELIMINA_PRODOTTO_DESCR", @"ELIMINA_PRODOTTO_DESCR") preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor redColor];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"DELETE", @"DELETE") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ELIMINA_PRODOTTO", @"ELIMINA_PRODOTTO") style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [_manager.cart removeProduct:prod];
        [self initList];
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
@end
