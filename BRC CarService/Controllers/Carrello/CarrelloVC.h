//
//  CarrelloVC.h
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"
#import "ProductCell.h"

@interface CarrelloVC : UIViewController<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, DeleteProductDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *priceWithIva;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;
@property (strong, nonatomic) IBOutlet UIView *emptyCartView;

- (IBAction)acquista:(id)sender;
@end
