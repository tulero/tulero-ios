//
//  DocumentazioneCell.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DocumentazioneCell.h"
#import "Settings.h"

@implementation DocumentazioneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.info.text = [[[NSLocalizedString(@"HELP_DESCR1", @"HELP_DESCR1") stringByAppendingString:NSLocalizedString(@"HELP_DESCR2", @"HELP_DESCR2")] stringByAppendingString:NSLocalizedString(@"HELP_DESCR3", @"HELP_DESCR3")] stringByAppendingString:NSLocalizedString(@"HELP_DESCR4", @"HELP_DESCR4")];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.info.attributedText];
    [attributedString addAttribute:NSLinkAttributeName value:DISCLAIMER_URL range:[[attributedString string] rangeOfString:NSLocalizedString(@"HELP_DESCR2", @"HELP_DESCR2")]];
    [attributedString addAttribute:NSUnderlineStyleAttributeName
                         value:@(NSUnderlineStyleSingle)
                         range:[[attributedString string] rangeOfString:NSLocalizedString(@"HELP_DESCR2", @"HELP_DESCR2")]];
    [attributedString addAttribute:NSLinkAttributeName value:DISCLAIMER_URL range:[[attributedString string] rangeOfString:NSLocalizedString(@"HELP_DESCR4", @"HELP_DESCR4")]];
    [attributedString addAttribute:NSUnderlineStyleAttributeName
                             value:@(NSUnderlineStyleSingle)
                             range:[[attributedString string] rangeOfString:NSLocalizedString(@"HELP_DESCR4", @"HELP_DESCR4")]];
//    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"HELP_DESCR1", @"HELP_DESCR1")
//                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
//    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"HELP_DESCR2", @"HELP_DESCR2")
//                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}]];
//    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"HELP_DESCR3", @"HELP_DESCR3")
//                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
//    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"HELP_DESCR4", @"HELP_DESCR4")
//                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}]];
//    [attributedString addAttribute:NSLinkAttributeName value:DISCLAIMER_URL range:[[attributedString string] rangeOfString:NSLocalizedString(@"HELP_DESCR2", @"HELP_DESCR2")]];
    
    NSDictionary *linkAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor],
                                                                        NSUnderlineColorAttributeName: [[UIColor blackColor] colorWithAlphaComponent:0.55],
                                                                        NSUnderlineStyleAttributeName: @(NSUnderlinePatternSolid)};
    self.info.linkTextAttributes = linkAttributes;
    self.info.attributedText = attributedString;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
