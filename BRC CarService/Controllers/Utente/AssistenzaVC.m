//
//  AssistenzaVC.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "AssistenzaVC.h"
#import "Settings.h"


@interface AssistenzaVC ()

@end

@implementation AssistenzaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.assistenzaView.clipsToBounds = YES;
    self.assistenzaView.layer.cornerRadius = 8;
    
    self.descrLabel.text = NSLocalizedString(@"ASSISTENZA_DESCR",@"ASSISTENZA_DESCR");
    self.callLabel.text = NSLocalizedString(@"ASSISTENZA_CALL", @"ASSISTENZA_CALL");
    self.msgLabel.text = NSLocalizedString(@"ASSISTENZA_MSG", @"ASSISTENZA_MSG");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = NSLocalizedString(@"HELP", @"HELP");
    [APP_DELEGATE sendPageViewGA:@"Help Desk"];
}

- (IBAction)call:(id)sender {
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:3476480095"]];
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString: ASSISTENZA_NUMBER] options:@{} completionHandler:nil];
}

- (IBAction)writeMessage:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:NSLocalizedString(@"SUBJECT_MAIL", @"SUBJECT_MAIL")];
        [mail setMessageBody:NSLocalizedString(@"MAIL_DESCR", @"MAIL_DESCR") isHTML:NO];
        [mail setToRecipients:@[ASSISTENZA_MAIL]];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else {
        NSLog(@"This device cannot send email");
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"MAIL_NOT_ENABLED", @"MAIL_NOT_ENABLED")];
    }
    //[KVNProgress showSuccessWithStatus:NSLocalizedString(@"MAIL_SENDED", @"MAIL_SENDED")];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            [KVNProgress showSuccessWithStatus:NSLocalizedString(@"MAIL_SENDED", @"MAIL_SENDED")];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            //[KVNProgress showErrorWithStatus:NSLocalizedString(@"MAIL_DRAFT", @"MAIL_DRAFT")];
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            //[KVNProgress showErrorWithStatus:NSLocalizedString(@"MAIL_CANCELLED_SENDING", @"MAIL_CANCELLED_SENDING")];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            //[KVNProgress showErrorWithStatus:NSLocalizedString(@"MAIL_ERROR", @"MAIL_ERROR")];
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            //[KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
