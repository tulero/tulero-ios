//
//  InfoUserCell.h
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoUserCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *infoImg;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;

-(void)configure:(NSDictionary*)elem;

@end
