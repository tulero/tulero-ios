//
//  DocumentazioneCell.h
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentazioneCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *documentationInfo;
@property (strong, nonatomic) IBOutlet UITextView *info;

@end
