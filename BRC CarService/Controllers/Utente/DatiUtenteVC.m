//
//  DatiUtenteVC.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DatiUtenteVC.h"
#import "UserCell.h"
#import "InfoUserCell.h"
#import "AssistenzaVC.h"
#import "DocumentazioneCell.h"
#import "ListaPreferitiVC.h"
#import "MembershipManager.h"
#import "LoginNVC.h"
#import "Settings.h"

@interface DatiUtenteVC ()

@end

@implementation DatiUtenteVC{
    NSArray* newList;
    MembershipManager* _manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    @"infoImg": [UIImage imageNamed:@"star_outline_black_icon"],
                    @"info": NSLocalizedString(@"LISTA_PREFERITI", @"LISTA_PREFERITI")
                    },
                @{
                    @"infoImg": [UIImage imageNamed:@"info_black_icon"],
                    @"info": NSLocalizedString(@"ASSISTENZA_INFO", @"ASSISTENZA_INFO")
                    }
                ];
    
     _manager = [MembershipManager sharedManager];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([UserCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([UserCell class])];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([InfoUserCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoUserCell class])];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([DocumentazioneCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DocumentazioneCell class])];
    
    self.btnLogout.clipsToBounds = YES;
    self.btnLogout.layer.cornerRadius = 22;
    
    self.userTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /** back image nav bar **/
    UIImage *backButtonImage = [[UIImage imageNamed:@"back_white_icon-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationController.navigationBar.backIndicatorImage = backButtonImage;
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = backButtonImage;
    
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"USER_DATA", @"USER_DATA");
    [APP_DELEGATE sendPageViewGA:@"Info User"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @" ";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)logout:(id)sender {
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([LoginNVC class])];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if(section == 1) {
        return [newList count];
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        
        NSDictionary* user = @{
                                 @"UserImg": [UIImage imageNamed:@"user_profile_red_icon"],
                                 @"UserName": _manager.actualUser.username,
                                 @"UserMail": _manager.actualUser.mail
                                 };
        
        UserCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UserCell class]) forIndexPath:indexPath];
        [cell configure:user];
        return cell;
    } else if(indexPath.section == 1){
        InfoUserCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoUserCell class]) forIndexPath:indexPath];
        [cell configure:newList[indexPath.row]];
        return cell;
    } else {
        DocumentazioneCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DocumentazioneCell class]) forIndexPath:indexPath];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section==1 && indexPath.row==0){
        ListaPreferitiVC *assistenza = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ListaPreferitiVC class])];
        [self.navigationController pushViewController:assistenza animated:YES];
    } else if(indexPath.section == 1 && indexPath.row == 1){
        AssistenzaVC *assistenza = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([AssistenzaVC class])];
        [self.navigationController pushViewController:assistenza animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        return 197;
    } else if(indexPath.section == 1){
        return 52;
    } else {
        return 60;
    }
}

@end
