//
//  UserCell.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.userImg.image = [elem objectForKey:@"UserImg"];
    self.userName.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"UserName"]];
    self.userMail.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"UserMail"]];
}

@end
