//
//  InfoUserCell.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "InfoUserCell.h"

@implementation InfoUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.infoImg.image = [elem objectForKey:@"infoImg"];
    self.infoLabel.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"info"]];
}

@end
