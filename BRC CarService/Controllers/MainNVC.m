//
//  MainVC.m
//  BRC CarService
//
//  Created by Etinet on 06/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "MainNVC.h"
#import "MembershipManager.h"
@interface MainNVC ()

@end

@implementation MainNVC{
    MembershipManager *_manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _manager = [MembershipManager sharedManager];
    _manager.cart.tabCartItem = self.tabBar.items[2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
