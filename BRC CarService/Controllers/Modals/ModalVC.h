//
//  ModalVC.h
//  botteroski
//
//  Created by Gianluca Tursi on 02/10/2017.
//  Copyright © 2017 Gianluca Tursi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalVC : UIViewController
    @property (strong, nonatomic) IBOutlet UIView *viewModal;
- (IBAction)onContinue:(id)sender;
    @property (strong, nonatomic) IBOutlet UIButton *btnContinua;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDescr;

@property (strong, nonatomic) NSString* my_title;
@property (strong, nonatomic) NSString* my_description;
@property (strong, nonatomic) NSString* btnLbl;

-(void)configureWithTitle:(NSString* )title description:(NSString* )description andBtnLabel:(NSString*) btnLbl;
@end
