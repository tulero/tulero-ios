//
//  RecuperoPasswordVC.m
//  BRC CarService
//
//  Created by Etinet on 15/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "RecuperoPasswordVC.h"
#import "MembershipManager.h"
#import "Settings.h"

@interface RecuperoPasswordVC ()

@end

@implementation RecuperoPasswordVC{
    MembershipManager* _manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.recPassView.clipsToBounds = YES;
    self.recPassView.layer.cornerRadius = 8.0f;
    self.btnResetPassword.clipsToBounds = YES;
    self.btnResetPassword.layer.cornerRadius = self.btnResetPassword.frame.size.height/2;
    
    self.recPassView.alpha = 0.0;
    
    UIGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
//    self.lblTitle.text = self.my_title;
//    self.lblDescr.text = self.my_description;
//    [self.btnContinua setTitle:self.btnLbl forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.3 animations:^{
        self.recPassView.alpha = 1.0;
    }];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (IBAction)resetPassword:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        if([self.recPassTextField.text isEqualToString:@""]){
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"PSW_RECOVERY_NOT_MAIL", @"PSW_RECOVERY_NOT_MAIL")];
        } else {
            [KVNProgress show];
            _manager = [MembershipManager sharedManager];
            [_manager passwordRecovery:self.recPassTextField.text onSuccess:^(NSString *str) {
                if([str isEqualToString:@"Operation complete"]){
                    [KVNProgress showSuccessWithStatus:NSLocalizedString(@"RECUPERO_PSW_RIUSCITO", @"RECUPERO_PSW_RIUSCITO")];
                    NSLog(@"Password recuperata");
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"GENERIC_ERROR")];
            }];
        }
    }];
}
@end
