//
//  ModalVC.m
//  botteroski
//
//  Created by Gianluca Tursi on 02/10/2017.
//  Copyright © 2017 Gianluca Tursi. All rights reserved.
//

#import "ModalVC.h"
#import "AgenteNVC.h"
#import "MainNVC.h"
#import "Settings.h"
#import "MembershipManager.h"

@interface ModalVC ()

@end

@implementation ModalVC{
    MembershipManager* _manager;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.viewModal.clipsToBounds = YES;
    self.viewModal.layer.cornerRadius = 8.0f;
    self.btnContinua.clipsToBounds = YES;
    self.btnContinua.layer.cornerRadius = self.btnContinua.frame.size.height/2;
    
    self.viewModal.alpha = 0.0;
    
    self.lblTitle.text = self.my_title;
    self.lblDescr.text = self.my_description;
    [self.btnContinua setTitle:self.btnLbl forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated{
    [UIView animateWithDuration:0.3 animations:^{
        self.viewModal.alpha = 1.0;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureWithTitle:(NSString* )title description:(NSString* )description andBtnLabel:(NSString*) btnLbl{
    
    self.my_title = title;
    self.my_description = description;
    self.btnLbl = btnLbl;
    
    //[self.btnContinua setTitle:btnLbl forState:UIControlStateNormal];
    
}

- (IBAction)onContinue:(id)sender {
    if([self.btnContinua.titleLabel.text isEqualToString:NSLocalizedString(@"LOGIN_MODAL_GUEST_BTN", @"LOGIN_MODAL_GUEST_BTN")]){
        [self dismissViewControllerAnimated:NO completion:^{
            _manager = [MembershipManager sharedManager];
            if(_manager.actualUser.isAgente){
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
            } else {
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
            }
        }];
    } else {
        [self dismissViewControllerAnimated:NO completion: nil];
    }
}
   
@end
