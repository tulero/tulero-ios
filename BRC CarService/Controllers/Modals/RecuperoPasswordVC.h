//
//  RecuperoPasswordVC.h
//  BRC CarService
//
//  Created by Etinet on 15/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecuperoPasswordVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *recPassView;
@property (strong, nonatomic) IBOutlet UILabel *recPassTitle;
@property (strong, nonatomic) IBOutlet UILabel *recPassDescr;
@property (strong, nonatomic) IBOutlet UILabel *recPassEmail;
@property (strong, nonatomic) IBOutlet UITextField *recPassTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnResetPassword;

- (IBAction)resetPassword:(id)sender;
@end
