//
//  WellcomeVC.m
//  BRC CarService
//
//  Created by Etinet on 29/01/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "WellcomeVC.h"
#import "Settings.h"

@interface WellcomeVC ()

@end

@implementation WellcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.btnAccedi.clipsToBounds = YES;
    self.btnOspite.clipsToBounds = YES;
    
    self.btnAccedi.layer.cornerRadius = self.btnAccedi.frame.size.height/2;
    self.btnOspite.layer.cornerRadius = self.btnOspite.frame.size.height/2;
    
    self.btnOspite.layer.borderColor = COLOR_BROWN.CGColor;
    self.btnOspite.layer.borderWidth = 1.0f;
    self.btnOspite.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
