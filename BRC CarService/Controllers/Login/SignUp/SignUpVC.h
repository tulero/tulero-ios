//
//  SignUpVC.h
//  BRC CarService
//
//  Created by Etinet on 01/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Settings.h"
#import "MembershipManager.h"

@interface SignUpVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtSurname;
@property (weak, nonatomic) IBOutlet UITextField *txtMail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtOfficina;
@property (strong, nonatomic) IBOutlet UITextField *txtPIva;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtPasswordCheck;
@property (strong, nonatomic) IBOutlet UITextField *txtAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnConditions;

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (strong, nonatomic) IBOutlet UITextView *tosTxt;

- (IBAction)onBack:(id)sender;
-(IBAction)btnSignUpTapped:(id)sender;
- (IBAction)acceptConditions:(id)sender;

@end
