//
//  SignUpVC.m
//  BRC CarService
//
//  Created by Etinet on 01/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SignUpVC.h"
#import "Settings.h"
#import "AgenteNVC.h"
#import "MainNVC.h"
#import "ModalVC.h"
#import "YCFirstTime.h"

@interface SignUpVC ()

@end

@implementation SignUpVC {
    MembershipManager* _member;
    NSString* _str;
    UITextField* _actualFirstResponder;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _member = [MembershipManager sharedManager];
    
    [self setupViews];
    
    UIGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    //[APP_DELEGATE trackGoogleScreenOpen:[NSString stringWithFormat:@"signup"]];
    
}


-(void)setupViews {
    
    self.btnConditions.tag = 0;
    [self.btnConditions setImage:[UIImage imageNamed:@"check_outline_red_icon"] forState:UIControlStateNormal];
    
    self.btnSignUp.clipsToBounds = YES;
    self.btnSignUp.layer.cornerRadius = self.btnSignUp.frame.size.height/2;
    
    self.tosTxt.text = NSLocalizedString(@"CONDIZIONI_PRIVACY", @"CONDIZIONI_PRIVACY");
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    NSMutableAttributedString* attrStr2 = [[NSMutableAttributedString alloc] initWithAttributedString:_tosTxt.attributedText];
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    //NSLog(@"LINGUA: %@",language);
    if([language isEqualToString:@"italiano (Italia)"]){
        [attrStr2 addAttribute:NSLinkAttributeName value:DISCLAIMER_URL range:[[attrStr2 string] rangeOfString:@"termini del servizio"]];
        [attrStr2 addAttribute:NSUnderlineStyleAttributeName
                         value:@(NSUnderlineStyleSingle)
                         range:[[attrStr2 string] rangeOfString:@"termini del servizio"]];
    } else {
        [attrStr2 addAttribute:NSLinkAttributeName value:DISCLAIMER_URL range:[[attrStr2 string] rangeOfString:@"terms of use"]];
        [attrStr2 addAttribute:NSUnderlineStyleAttributeName
                         value:@(NSUnderlineStyleSingle)
                         range:[[attrStr2 string] rangeOfString:@"terms of use"]];
    }
    
    
    NSDictionary *linkAttributes2 = @{NSForegroundColorAttributeName: PRIMARY_COLOR,
                                      NSUnderlineColorAttributeName: [PRIMARY_COLOR colorWithAlphaComponent:0.55],
                                      NSUnderlineStyleAttributeName: @(NSUnderlinePatternSolid)};
    
    
    _tosTxt.linkTextAttributes = linkAttributes2;
    _tosTxt.attributedText = attrStr2;
    
    
    _txtMail.delegate = self;
    _txtName.delegate = self;
    _txtOfficina.delegate = self;
    _txtPhone.delegate = self;
    _txtSurname.delegate = self;
    _txtCity.delegate = self;
    _txtPIva.delegate = self;
    _txtPassword.delegate = self;
    _txtPasswordCheck.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = @"Registrati";
    
    [APP_DELEGATE sendPageViewGA:@"Registration"];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnSignUpTapped:(id)sender {
    if(![self isValid]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"REGISTRATION_ERROR", @"REGISTRATION_ERROR")];
        //NSLog(@"PARAMETRI NON VALIDI!");
    } else {
        _str = @"";
        
        [KVNProgress show];
        
        if(self.btnConditions.tag == 1){
        
            [_member createGuestUser:self.txtName.text surname:self.txtSurname.text officina:self.txtOfficina.text city:self.txtCity.text address:self.txtAddress.text pIva:self.txtPIva.text email:self.txtMail.text phone:self.txtPhone.text password:self.txtPassword.text onSuccess: ^(NSString *string) {
                [KVNProgress dismiss];
                //_isLoaded = YES;
                _str = [[NSString alloc] initWithFormat:@"%@",string];
                //[[NSArray alloc] initWithArray:list];
                
                if([_str isEqualToString:@"Operation complete"]){
                    [_member loginWithUsername:self.txtMail.text password:self.txtPassword.text remember:NO onSuccess:^(NSDictionary *array) {
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        //NSDictionary* userSaved = [defaults objectForKey:@"user"];
                        //User* _actualUser = [[User alloc] initWithDictionary:[defaults objectForKey:@"user"]];
                        [[YCFirstTime shared] executeOnce:^{
                            
                            ModalVC* model = [[ModalVC alloc] initWithNibName:NSStringFromClass([ModalVC class]) bundle:nil];
                            
                            [model configureWithTitle:NSLocalizedString(@"LOGIN_MODAL_GUEST_TITLE", @"LOGIN_MODAL_GUEST_TITLE") description:NSLocalizedString(@"LOGIN_MODAL_GUEST_DESCR", @"LOGIN_MODAL_GUEST_DESCR") andBtnLabel:NSLocalizedString(@"LOGIN_MODAL_GUEST_BTN", @"LOGIN_MODAL_GUEST_BTN")];
                            
                            model.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                            
                            [self presentViewController:model animated:NO completion:nil];
                            
                        } forKey:@"MODAL_GUEST"];
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        User* _actualUser = [[User alloc] initWithDictionary:[defaults objectForKey:@"user"]];
                        if(_actualUser.isAgente){
                            [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
                        } else {
                            [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
                        }
                    } onFailure:^(NSError *error) {
                       [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
                    }];
                } else {
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"UTENTE_ESISTENTE", @"UTENTE_ESISTENTE")];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
                //_isLoaded = YES;
                
            }];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"LOGIN_PRIVACY", @"LOGIN_PRIVACY")];
        }
    }
}

- (IBAction)acceptConditions:(id)sender {
    if(self.btnConditions.tag == 0){
        self.btnConditions.tag = 1;
        [self.btnConditions setImage:[UIImage imageNamed:@"check_red_icon"] forState:UIControlStateNormal];
    } else {
        self.btnConditions.tag = 0;
        [self.btnConditions setImage:[UIImage imageNamed:@"check_outline_red_icon"] forState:UIControlStateNormal];
    }
}

-(BOOL) isValid {
    NSString *pattern = @"^[\\D]+$";
    NSError  *error = nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    
    //Check name
    NSUInteger matches = [regex numberOfMatchesInString:self.txtName.text options:0 range:NSMakeRange(0,[self.txtName.text length])];
    if(matches != 1) {
        return NO;
    }
    
    //Check surname
    matches = [regex numberOfMatchesInString:self.txtSurname.text options:0 range:NSMakeRange(0,[self.txtSurname.text length])];
    if(matches != 1) {
        return NO;
    }
    
    //Check city
    matches = [regex numberOfMatchesInString:self.txtCity.text options:0 range:NSMakeRange(0,[self.txtCity.text length])];
    if(matches != 1) {
        return NO;
    }
    
    //Check mail
    pattern = @"^[a-zA-Z0-9_]+\\.{0,1}[a-zA_Z0-9_]*@[a-zA-Z0-9\\-]+\\.[a-zA-Z]{2,6}$";
    regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    matches = [regex numberOfMatchesInString:self.txtMail.text options:0 range:NSMakeRange(0,[self.txtMail.text length])];
    if(matches != 1) {
        NSLog(@"EMAIL NON VALIDA!");
        return NO;
    }
    
    //Check telefono
    if(![self.txtPhone.text isEqualToString:@""]){
        pattern = @"^[0-9]{9,10}$";
        regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        matches = [regex numberOfMatchesInString:self.txtPhone.text options:0 range:NSMakeRange(0,[self.txtPhone.text length])];
        if(matches != 1) {
            NSLog(@"TELEFONO NON VALIDO!  %@",self.txtPhone.text);
            return NO;
        }
    }
    
    //Check partita iva
    pattern = @"^[0-9][a-zA-Z]{6,20}$";
    regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    matches = [regex numberOfMatchesInString:self.txtPIva.text options:0 range:NSMakeRange(0,[self.txtPIva.text length])];
    if(matches != 1) {
        NSLog(@"PARTITA IVA  NON VALIDA!");
        return NO;
    }
    
    //Check password
    pattern = @"^[\\S]+$";
    regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    matches = [regex numberOfMatchesInString:self.txtPassword.text options:0 range:NSMakeRange(0,[self.txtPassword.text length])];
    if(matches != 1) {
        NSLog(@"PASSWORD NON VALIDA!");
        return NO;
    }
    if(![self.txtPassword.text isEqualToString:self.txtPasswordCheck.text]){
        return NO;
    }
    
    //Check nome officina
    if([self.txtOfficina.text isEqualToString:@""]){
        return NO;
    }
    
    if([self.txtAddress.text isEqualToString:@""]){
        return NO;
    }
    
    return YES;
}

#pragma UITextView
-(void)dismissKeyboard {
    [_actualFirstResponder resignFirstResponder];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == _txtPhone)
        textField.keyboardType = UIKeyboardTypeNumberPad;
    else if (textField == _txtMail)
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    else
        textField.keyboardType = UIKeyboardTypeDefault;
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    _actualFirstResponder = sender;
}


-(void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    _actualFirstResponder = nil;
}

@end
