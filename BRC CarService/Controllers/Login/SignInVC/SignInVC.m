//
//  SignInVC.m
//  BRC CarService
//
//  Created by Etinet on 01/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SignInVC.h"
#import "ModalVC.h"
#import "MembershipManager.h"
#import "AgenteNVC.h"
#import "RecuperoPasswordVC.h"
#import "Settings.h"

@interface SignInVC ()

@end

@implementation SignInVC{
    MembershipManager *member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    member = [MembershipManager sharedManager];
    // Do any additional setup after loading the view.
    
    self.btnSignIn.clipsToBounds = YES;
    self.btnSignIn.layer.cornerRadius = self.btnSignIn.frame.size.height/2;
    
    UIGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    [self.btnForgotPsw setTitle:NSLocalizedString(@"PASSWORD_DIMENTICATA", @"PASSWORD_DIMENTICATA") forState:UIControlStateNormal];
    
    self.txtMail.delegate = self;
    self.txtPSW.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE sendPageViewGA:@"Login"];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if (textField == self.txtMail) {
        [textField resignFirstResponder];
        [self.txtPSW becomeFirstResponder];
    } else if (textField == self.txtPSW) {
        [textField resignFirstResponder];
        [self login];
    }
    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openInfo:(id)sender {
    ModalVC* info = [[ModalVC alloc] initWithNibName:@"ModalVC" bundle:nil];
    
    [info configureWithTitle:NSLocalizedString(@"LOGIN_MODAL_TITLE", @"LOGIN_MODAL_TITLE") description:NSLocalizedString(@"LOGIN_MODAL_DESCR", @"LOGIN_MODAL_DESCR") andBtnLabel:NSLocalizedString(@"LOGIN_MODAL_BTN", @"LOGIN_MODAL_BTN")];
    
    info.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self.navigationController presentViewController:info animated:NO completion:^{
        
    }];
}

- (IBAction)onBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnResetPSW:(id)sender{
    RecuperoPasswordVC* info = [[RecuperoPasswordVC alloc] initWithNibName:@"RecuperoPasswordVC" bundle:nil];
    
    //[info configureWithTitle:NSLocalizedString(@"LOGIN_MODAL_TITLE", @"LOGIN_MODAL_TITLE") description:NSLocalizedString(@"LOGIN_MODAL_DESCR", @"LOGIN_MODAL_DESCR") andBtnLabel:NSLocalizedString(@"LOGIN_MODAL_BTN", @"LOGIN_MODAL_BTN")];
    
    info.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self.navigationController presentViewController:info animated:NO completion:^{
        
    }];
}

/** fixme: guest in signup **/
-(IBAction)btnSignInTapped:(id)sender{
    [self login];
    //[member loginWithUsername: @"assistenza@etinet.it" password: @"UGpqX32q" remember:self.chkRemember.isOn onSuccess:^(NSDictionary *array)
//    if([self.txtMail.text isEqualToString:@""] || [self.txtPSW.text isEqualToString:@""]){
//        [KVNProgress showErrorWithStatus: NSLocalizedString(@"LOGIN_INPUT", @"LOGIN_INPUT")];
//    } else {
//        [KVNProgress show];
//        [member loginWithUsername: self.txtMail.text password: self.txtPSW.text remember:self.chkRemember.isOn onSuccess:^(NSDictionary *array) {
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            //NSDictionary* userSaved = [defaults objectForKey:@"user"];
//            User * _actualUser = [[User alloc] initWithDictionary:[defaults objectForKey:@"user"]];
//            [KVNProgress dismiss];
//            if(_actualUser.isAgente){
//                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
//            } else {
//                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
//            }
//        } onFailure:^(NSError *error) {
//            if([error.userInfo[@"msg"] isEqualToString:@"MAIL/PASSWORD WRONG"]){
//                [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERRORE_CREDENZIALI_LOGIN", @"ERRORE_CREDENZIALI_LOGIN")];
//            } else {
//                [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERRORE_LOGIN", @"ERRORE_LOGIN")];
//                NSLog(@"ERRORE LOGIN");
//            }
//        }];
//    }
    
    
//    UIAlertController * alert = [UIAlertController
//                                 alertControllerWithTitle:@"Login"
//                                 message:@"Vuoi entrare come?"
//                                 preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//    UIAlertAction* yesButton = [UIAlertAction
//                                actionWithTitle:@"Agente"
//                                style:UIAlertActionStyleDefault
//                                handler:^(UIAlertAction * action) {
//                                    [member signInWithUsername:@"" password:@"" isAgente:YES rememberMe:NO onSuccess:^(NSDictionary *array) {
//                                        //agente
//
//                                        [self.navigationController pushViewController:[APP_DELEGATE.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([MainNVC class])] animated:YES];
//
//                                    } onFailure:^(NSError *error) {
//
//                                    }];
//                                }];
//
//    UIAlertAction* noButton = [UIAlertAction
//                               actionWithTitle:@"Officina"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action) {
//                                   [member signInWithUsername:@"" password:@"" isAgente:NO rememberMe:NO onSuccess:^(NSDictionary *array) {
//                                       //officina
//                                       [self.navigationController pushViewController:[APP_DELEGATE.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([MainNVC class])] animated:YES];
//
//                                   } onFailure:^(NSError *error) {
//
//                                   }];
//                               }];
//
//    [alert addAction:yesButton];
//    [alert addAction:noButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)login{
    if([self.txtMail.text isEqualToString:@""] || [self.txtPSW.text isEqualToString:@""]){
        [KVNProgress showErrorWithStatus: NSLocalizedString(@"LOGIN_INPUT", @"LOGIN_INPUT")];
    } else {
        [KVNProgress show];
        [member loginWithUsername: self.txtMail.text password: self.txtPSW.text remember:self.chkRemember.isOn onSuccess:^(NSDictionary *array) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //NSDictionary* userSaved = [defaults objectForKey:@"user"];
            User * _actualUser = [[User alloc] initWithDictionary:[defaults objectForKey:@"user"]];
            [KVNProgress dismiss];
            if(_actualUser.isAgente){
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
            } else {
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
            }
        } onFailure:^(NSError *error) {
            if([error.userInfo[@"msg"] isEqualToString:@"MAIL/PASSWORD WRONG"]){
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERRORE_CREDENZIALI_LOGIN", @"ERRORE_CREDENZIALI_LOGIN")];
            } else {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERRORE_LOGIN", @"ERRORE_LOGIN")];
                NSLog(@"ERRORE LOGIN");
            }
        }];
    }
}

@end
