//
//  SignInVC.h
//  BRC CarService
//
//  Created by Etinet on 01/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainNVC.h"
#import "Settings.h"

@interface SignInVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtMail;
@property (weak, nonatomic) IBOutlet UITextField *txtPSW;


@property (weak, nonatomic) IBOutlet UISwitch *chkRemember;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPsw;

- (IBAction)onBack:(id)sender;
-(IBAction)btnSignInTapped:(id)sender;

-(IBAction)btnResetPSW:(id)sender;

- (IBAction)openInfo:(id)sender;

@end
