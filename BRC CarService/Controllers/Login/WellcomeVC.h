//
//  WellcomeVC.h
//  BRC CarService
//
//  Created by Etinet on 29/01/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WellcomeVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnAccedi;
@property (strong, nonatomic) IBOutlet UIButton *btnOspite;

@end
