//
//  infoCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface infoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *detailName;
@property (strong, nonatomic) IBOutlet UILabel *detailInfo;

-(void)configureCatalogo:(NSDictionary*)elem index:(NSInteger)row category:(NSString*)productCategory;
-(void)configureEspositore:(NSDictionary*)elem index:(NSInteger)row category:(NSString*)productCategory;
-(void)configurePromo:(NSDictionary*)elem index:(NSInteger)row;
-(void)configureOrdine:(NSDictionary*)elem index:(NSInteger)row;
-(void)configureInfoOrdine:(NSDictionary*)elem index:(NSInteger)row quantity:(int)qnt;
-(void)configureInventory:(NSDictionary*)elem index:(NSInteger)row;
-(void)configureQuantityInventory:(NSDictionary*)elem index:(NSInteger)row variation:(int)variation;

@end
