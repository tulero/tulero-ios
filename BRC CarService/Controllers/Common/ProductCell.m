//
//  ProductCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ProductCell.h"
#import "Utils.h"
#import "Settings.h"
#import "MembershipManager.h"

@implementation ProductCell{
    MembershipManager* _manager;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureOrdine:(NSDictionary*)elem quantity:(int)qnt{
    
    self.btnImg.tag = 5; //Immagine cestino
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    
    [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_CATALOGO", @"BTN_CATALOGO") forState:UIControlStateNormal];
    self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:162.0f/255.0f blue:67.0f/255.0f alpha:1.0f].CGColor;
    
    self.productName.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    self.productCode.text = [NSLocalizedString(@"CODE_LABEL", @"CODE_LABEL") stringByAppendingString: [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.productEAN.text = [NSLocalizedString(@"EAN_LABEL", @"EAN_LABEL") stringByAppendingString: [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    
    [self.btnImg setImage:[UIImage imageNamed: @"arrow_grey_icon"] forState:UIControlStateNormal];
    
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* prezzo = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([prezzo isEqualToString:@"N.D."]){
        self.productPrice.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_INFO", @"ORDER_INFO"), prezzo, qnt];
    } else {
        double price = [prezzo doubleValue];
        self.productPrice.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_INFO_2", @"ORDER_INFO_2"),price - (price*sconto)/100, qnt];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    
    self.preferitiImg.hidden = YES;
}

-(void)configurePreferiti:(NSDictionary*)elem {
    self.btnImg.tag = 3; //Immagine Carrello
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    
    [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_CATALOGO", @"BTN_CATALOGO") forState:UIControlStateNormal];
    self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:162.0f/255.0f blue:67.0f/255.0f alpha:1.0f].CGColor;
    
    self.productName.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    self.productCode.text = [NSLocalizedString(@"FAVORITE_PRODUCT_CODE", @"FAVORITE_PRODUCT_CODE") stringByAppendingString:[Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.productEAN.text = [NSLocalizedString(@"FAVORITE_PRODUCT_EAN", @"FAVORITE_PRODUCT_EAN") stringByAppendingString:[Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* prezzo = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([prezzo isEqualToString:@"N.D."]){
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%@",prezzo]];
    } else {
        double price = [prezzo doubleValue];
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%0.2f",price-(price*sconto)/100]];
    }
    //[NSString stringWithFormat:@"€ %@",[elem objectForKey:@"PrzUnitario"]];
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    //self.productImage =[elem objectForKey:@"ImgProdotto"];
    [self.btnImg setImage:[UIImage imageNamed:@"cart_red_icon"] forState:UIControlStateNormal];
    self.productCart = elem;
}

-(void)configureEspositore:(NSDictionary*)elem {
    self.btnImg.tag = 1; //immagine download
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_ESPOSITORE", @"BTN_ESPOSITORE") forState:UIControlStateNormal];
    self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:188.0f/255.0f blue:27.0f/255.0f alpha:1.0].CGColor;
    
    self.productName.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    self.productCode.text = [NSLocalizedString(@"FAVORITE_PRODUCT_CODE", @"FAVORITE_PRODUCT_CODE") stringByAppendingString:[Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.productEAN.text = [NSLocalizedString(@"FAVORITE_PRODUCT_EAN", @"FAVORITE_PRODUCT_EAN") stringByAppendingString:[Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%@",price]];
    } else {
        double prezzo = [price doubleValue];
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%0.2f",prezzo - (prezzo*sconto)/100]];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    [self.btnImg setImage:[UIImage imageNamed:@"download_red_icon"] forState:UIControlStateNormal];
    self.preferitiImg.hidden = YES;
    
    self.productCart = elem;
}

-(void)configureCatalogo:(NSDictionary*)elem {
    self.btnImg.tag = 3; //immagine carrello
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;

    if([[elem objectForKey:@"IsEspositore"] integerValue] == 1) {
        [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_ESPOSITORE", @"BTN_ESPOSITORE") forState:UIControlStateNormal];
        self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:188.0f/255.0f blue:27.0f/255.0f alpha:1.0f].CGColor;
    } else {
        [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_CATALOGO", @"BTN_CATALOGO") forState:UIControlStateNormal];
        self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:162.0f/255.0f blue:67.0f/255.0f alpha:1.0f].CGColor;
    }
    
    self.productName.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    self.productCode.text = [NSLocalizedString(@"FAVORITE_PRODUCT_CODE", @"FAVORITE_PRODUCT_CODE") stringByAppendingString:[Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.productEAN.text = [NSLocalizedString(@"FAVORITE_PRODUCT_EAN", @"FAVORITE_PRODUCT_EAN") stringByAppendingString:[Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* prezzo = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([prezzo isEqualToString:@"N.D."]){
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%@",prezzo]];
    } else {
        float prz = [prezzo floatValue];
        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%0.2f",prz - (prz*sconto)/100]];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    
    if([[elem objectForKey:@"Preferito"] integerValue] == 0) {
        self.preferitiImg.hidden = YES;
    } else {
        self.preferitiImg.hidden = NO;
    }
    
    [self.btnImg setImage:[UIImage imageNamed:@"cart_red_icon"] forState:UIControlStateNormal];
    
    self.productCart = elem;
}

-(void)configureCarrello:(NSDictionary*)elem{
    self.btnImg.tag = 0; //immagine cestino
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    
    [self.btnCatalogo setTitle:NSLocalizedString(@"BTN_CATALOGO", @"BTN_CATALOGO") forState:UIControlStateNormal];
    self.btnCatalogo.layer.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:162.0f/255.0f blue:67.0f/255.0f alpha:1.0f].CGColor;
    
    self.productName.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    self.productCode.text = [NSLocalizedString(@"FAVORITE_PRODUCT_CODE", @"FAVORITE_PRODUCT_CODE") stringByAppendingString:[Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.productEAN.text = [NSLocalizedString(@"FAVORITE_PRODUCT_EAN", @"FAVORITE_PRODUCT_EAN") stringByAppendingString:[Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
//    NSString* prezzo = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
//    if([prezzo isEqualToString:@"N.D."]){
//        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%@",prezzo]];
//    } else {
//        float prz = [prezzo floatValue];
//        self.productPrice.text = [NSLocalizedString(@"FAVORITE_PRODUCT_PRICE", @"FAVORITE_PRODUCT_PRICE") stringByAppendingString:[NSString stringWithFormat:@"%0.2f",prz]];
//    }
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* prezzo = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([prezzo isEqualToString:@"N.D."]){
        self.productPrice.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_INFO", @"ORDER_INFO"), prezzo, [elem[@"QtaOrder"] intValue]];
    } else {
        double price = [prezzo doubleValue];
        self.productPrice.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_INFO_2", @"ORDER_INFO_2"),price - (price*sconto)/100, [elem[@"QtaOrder"] intValue]];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    self.preferitiImg.hidden = YES;
    
    self.productCart = elem;
}

- (IBAction)deleteProduct:(UIButton*)sender {
    if(sender.tag == 0) {
        //[KVNProgress show];
        [self.delegate deleteProduct:self.productCart];
        //[KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSERIMENTO_RIUSCITO", @"INSERIMENTO_RIUSCITO")];
    } else if(sender.tag == 3) {
        [KVNProgress show];
        _manager = [MembershipManager sharedManager];
        [_manager.cart addProduct:self.productCart];
        [KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSERIMENTO_RIUSCITO", @"INSERIMENTO_RIUSCITO")];
    } else if(sender.tag == 1) {
        [KVNProgress show];
        _manager = [MembershipManager sharedManager];
        [_manager downloadExpositor:1 codArticolo:self.productCart[@"CodArticolo"] onSuccess:^(NSString *str) {
            if([str hasPrefix:@"Operation complete. New stock is"]){
                [KVNProgress showSuccessWithStatus:NSLocalizedString(@"DOWNLOAD_COMPLETATO", @"DOWNLOAD_COMPLETATO")];
            } else if ([str hasPrefix:@"Stock is"]){
                [KVNProgress showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"DOWNLOAD_ERROR_QUANTITY", @"DOWNLOAD_ERROR_QUANTITY"),[self.productCart[@"QtaOrder"] intValue]]];
            } else {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
            }
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
        }];
    }
}

@end
