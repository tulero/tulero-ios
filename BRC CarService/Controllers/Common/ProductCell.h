//
//  ProductCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol DeleteProductDelegate;
//@protocol AddProductDelegate;

@interface ProductCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCatalogo;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productCode;
@property (strong, nonatomic) IBOutlet UILabel *productEAN;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UIImageView *preferitiImg;
@property (strong, nonatomic) IBOutlet UIButton *btnImg;
@property NSDictionary* productCart;

@property (strong, nonatomic) id<DeleteProductDelegate> delegate;
//@property (strong, nonatomic) id<AddProductDelegate> delegateAdd;

@property BOOL isOrder;

-(void)configureOrdine:(NSDictionary*)elem quantity:(int)qnt;
-(void)configurePreferiti:(NSDictionary*)elem;
-(void)configureEspositore:(NSDictionary*)elem;
-(void)configureCatalogo:(NSDictionary*)elem;
-(void)configureCarrello:(NSDictionary*)elem;
- (IBAction)deleteProduct:(UIButton*)sender;

@end

@protocol DeleteProductDelegate

-(void)deleteProduct:(NSDictionary*)prod;

@end


//@protocol AddProductDelegate
//
//-(void)addProduct:(NSDictionary*)prod;

//@end
