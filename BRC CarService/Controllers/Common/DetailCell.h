//
//  DetailCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol AddProductDelegate;
@protocol DownloadProductDelegate;

@interface DetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *detailImage;
@property (strong, nonatomic) IBOutlet UIButton *detailCarrello;
@property (strong, nonatomic) IBOutlet UILabel *detailService;
@property (strong, nonatomic) IBOutlet UILabel *detailProduct;
@property (strong, nonatomic) IBOutlet UILabel *detailPrice;

@property NSDictionary* product;
@property (strong, nonatomic) id<AddProductDelegate> delegate;
@property (strong, nonatomic) id<DownloadProductDelegate> downloadDelegate;

-(void)configureCatalogo:(NSDictionary*)elem;
-(void)configureEspositore:(NSDictionary*)elem;
-(void)configureOrdine:(NSDictionary*)elem;
-(void)configureInventory:(NSDictionary*)elem;
- (IBAction)addProduct:(UIButton*)sender;

@end

@protocol AddProductDelegate

-(void)addProduct:(NSDictionary*)prod;

@end


@protocol DownloadProductDelegate

-(void)downloadProduct:(NSDictionary*)prod;

@end
