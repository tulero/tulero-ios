//
//  infoCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "infoCell.h"
#import "Settings.h"

@implementation infoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCatalogo:(NSDictionary*)elem index:(NSInteger)row category:(NSString*)productCategory{
    
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"POSITION_LABEL", @"POSITION_LABEL");
            self.detailInfo.text = NSLocalizedString(@"POSITION_DETAIL", @"POSITION_DETAIL");
            self.detailInfo.textColor = [UIColor colorWithRed:243.0/255.0f green:162.0/255.0f blue:67.0/255.0f alpha:1.0f];
            break;
        case 1:
            self.detailName.text = NSLocalizedString(@"CATEGORY_LABEL", @"CATEGORY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@",[Utils Object:productCategory orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = NSLocalizedString(@"CODE_LABEL", @"CODE_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 3:
            self.detailName.text = NSLocalizedString(@"EAN_LABEL", @"EAN_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 4:
            self.detailName.text = NSLocalizedString(@"DELIVERY_LABEL", @"DELIVERY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"DELIVERY_TIME", @"DELIVERY_TIME"), [Utils Object:[elem objectForKey:@"Consegna"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 5: {
            self.detailName.text = NSLocalizedString(@"DISCOUNTS_LABEL", @"DISCOUNTS_LABEL");
            int sconto = 0;
            NSString* price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
            if([price isEqualToString:@"N.D."]){
                self.detailInfo.text = [NSString stringWithFormat:@"(%@) -%@%%", price, [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [self.detailInfo setAttributedText:string];
            } else {
                self.detailInfo.text = [NSString stringWithFormat:@"(%0.2f) -%@%%", [price floatValue], [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [self.detailInfo setAttributedText:string];
            }
            break;
        }
//        case 6: {
//            self.detailName.text = @"Quantità ordinata:";
//            self.detailInfo.text = [elem objectForKey:@"QtaOrder"];
//            self.detailInfo.textColor = [UIColor blackColor];
//            break;
//        }
//        case 7: {
//            self.detailName.text = @"Totale Ordine:";
//            self.detailInfo.text = [NSString stringWithFormat:@"€ %0.2f",[[elem objectForKey:@"QtaOrder"] intValue]*[[elem objectForKey:@"PrzUnitario"] floatValue]];
//        }
        default:
            break;
    }
    
}

-(void)configureOrdine:(NSDictionary*)elem index:(NSInteger)row {
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"POSITION_LABEL", @"POSITION_LABEL");
            self.detailInfo.text = NSLocalizedString(@"POSITION_DETAIL", @"POSITION_DETAIL");
            self.detailInfo.textColor = [UIColor colorWithRed:243.0/255.0f green:162.0/255.0f blue:67.0/255.0f alpha:1.0f];
            break;
        case 1:
            self.detailName.text = NSLocalizedString(@"CATEGORY_LABEL", @"CATEGORY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Commodity"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = NSLocalizedString(@"CODE_LABEL", @"CODE_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 3:
            self.detailName.text = NSLocalizedString(@"EAN_LABEL", @"EAN_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 4:
            self.detailName.text = NSLocalizedString(@"DELIVERY_LABEL", @"DELIVERY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"DELIVERY_TIME", @"DELIVERY_TIME"),[Utils Object:[elem objectForKey:@"Consegna"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 5: {
            self.detailName.text = NSLocalizedString(@"DISCOUNTS_LABEL", @"DISCOUNTS_LABEL");
            NSString* price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
            if([price isEqualToString:@"N.D."]){
                self.detailInfo.text = [NSString stringWithFormat:@"(%@) -%@%%", price, [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [self.detailInfo setAttributedText:string];
            } else {
                self.detailInfo.text = [NSString stringWithFormat:@"(%0.2f) -%@%%", [price floatValue], [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [self.detailInfo setAttributedText:string];
            }
            break;
        }
        default:
            break;
    }
}

-(void)configureInfoOrdine:(NSDictionary*)elem index:(NSInteger)row quantity:(int)qnt {
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"QUANTITY_ORDER", @"QUANTITY_ORDER");
            self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_ORDERED", @"QUANTITY_ORDERED"), qnt];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 1:{
            self.detailName.text = NSLocalizedString(@"FULL_PRICE_ORDER", @"FULL_PRICE_ORDER");
            int sconto = 0;
            if(elem[@"Sconto"]){
                sconto = [elem[@"Sconto"] intValue];
            }
            NSString *price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
            if([price isEqualToString:@"N.D."]){
                self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_ORDERED_PRICE_2", @"QUANTITY_ORDERED_PRICE_2"), price];
            } else {
                double prezzo = [price doubleValue];
                self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_ORDERED_PRICE", @"QUANTITY_ORDERED_PRICE"), (prezzo - (prezzo * sconto)/100) * qnt];
            }
            self.detailInfo.textColor = [UIColor colorWithRed:233.0/255.0f green:71.0/255.0f blue:75.0/255.0f alpha:1.0f];;
        }
        default:
            break;
    }
}

-(void)configureEspositore:(NSDictionary*)elem index:(NSInteger)row category:(NSString*)productCategory{
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"CATEGORY_LABEL", @"CATEGORY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", productCategory];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 1:
            self.detailName.text = NSLocalizedString(@"CODE_LABEL", @"CODE_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = NSLocalizedString(@"EAN_LABEL", @"EAN_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 3:
            self.detailName.text = NSLocalizedString(@"DELIVERY_LABEL", @"DELIVERY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"DELIVERY_TIME", @"DELIVERY_TIME"),[Utils Object:[elem objectForKey:@"Consegna"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 4: {
            self.detailName.text = NSLocalizedString(@"DISCOUNTS_LABEL", @"DISCOUNTS_LABEL");
            NSString* price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
            if([price isEqualToString:@"N.D."]){
                self.detailInfo.text = [NSString stringWithFormat:@"(%@) -%@%%", price, [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [self.detailInfo setAttributedText:string];
            } else {
                self.detailInfo.text = [NSString stringWithFormat:@"(%0.2f) -%@%%", [price floatValue], [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [self.detailInfo setAttributedText:string];
            }
            break;
        }
        default:
            break;
        }
}

-(void)configurePromo:(NSDictionary*)elem index:(NSInteger)row {
    switch (row) {
        case 0: {
            self.detailName.text = NSLocalizedString(@"PUBLICATION_DATE", @"PUBLICATION_DATE");
            self.detailInfo.text = [Utils formatISODateOrders:elem[@"DataPubblicazione"] toFormat:@"dd/MM/yyyy"];
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        }
        case 1: {
            self.detailName.text = NSLocalizedString(@"SITE_LINK", @"SITE_LINK");
            if(elem[@"Allegato2"] && ![elem[@"Allegato2"] isEqualToString:@"NULL"]){
                NSString* link = elem[@"Allegato2"];
                if([link hasPrefix:@"/"]){
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[SERVICE_BRC_URL stringByAppendingString:link]];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    self.detailInfo.attributedText = attributeString;
                    
                    self.selectionStyle = UITableViewCellSelectionStyleDefault;
                    self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
                } else {
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[SERVICE_BRC_URL2 stringByAppendingString:link]];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    self.detailInfo.attributedText = attributeString;
                    
                    self.selectionStyle = UITableViewCellSelectionStyleDefault;
                    self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
                }
            } else {
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:SERVICE_BRC_URL];
                [attributeString addAttribute:NSUnderlineStyleAttributeName
                                        value:[NSNumber numberWithInt:1]
                                        range:(NSRange){0,[attributeString length]}];
                self.detailInfo.attributedText = attributeString;
            
                self.selectionStyle = UITableViewCellSelectionStyleDefault;
                self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
            }
            break;
        }
        case 2: {
            self.detailName.text = NSLocalizedString(@"PDF_LINK", @"PDF_LINK");
            if(elem[@"Allegato1"] && ![elem[@"Allegato1"] isEqualToString:@"NULL"]){
                NSString* link = elem[@"Allegato1"];
                if([link hasPrefix:@"/"]){
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[SERVICE_BRC_URL stringByAppendingString:link]];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    self.detailInfo.attributedText = attributeString;
                    
                    self.selectionStyle = UITableViewCellSelectionStyleDefault;
                    self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
                } else {
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[SERVICE_BRC_URL2 stringByAppendingString:link]];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    self.detailInfo.attributedText = attributeString;
                    
                    self.selectionStyle = UITableViewCellSelectionStyleDefault;
                    self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
                }
            } else {
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:SERVICE_BRC_URL];
                [attributeString addAttribute:NSUnderlineStyleAttributeName
                                        value:[NSNumber numberWithInt:1]
                                        range:(NSRange){0,[attributeString length]}];
                self.detailInfo.attributedText = attributeString;
                
                self.selectionStyle = UITableViewCellSelectionStyleDefault;
                self.detailInfo.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
            }
            break;
        }
        default:
            break;
    }
}

-(void)configureInventory:(NSDictionary*)elem index:(NSInteger)row{
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"POSITION_LABEL", @"POSITION_LABEL");
            self.detailInfo.text = NSLocalizedString(@"POSITION_DETAIL_EXPOSITOR", @"POSITION_DETAIL_EXPOSITOR");
            self.detailInfo.textColor = [UIColor colorWithRed:95.0/255.0f green:188.0/255.0f blue:27.0/255.0f alpha:1.0f];
            break;
        case 1:
            self.detailName.text = NSLocalizedString(@"CATEGORY_LABEL", @"CATEGORY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Commodity"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = NSLocalizedString(@"CODE_LABEL", @"CODE_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 3:
            self.detailName.text = NSLocalizedString(@"EAN_LABEL", @"EAN_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 4:
            self.detailName.text = NSLocalizedString(@"DELIVERY_LABEL", @"DELIVERY_LABEL");
            self.detailInfo.text = [NSString stringWithFormat:NSLocalizedString(@"DELIVERY_TIME", @"DELIVERY_TIME"), [Utils Object:[elem objectForKey:@"Consegna"] orPlaceholder:@"-"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 5: {
            self.detailName.text = NSLocalizedString(@"DISCOUNTS_LABEL", @"DISCOUNTS_LABEL");
            NSString* price = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
            if([price isEqualToString:@"N.D."]){
                self.detailInfo.text = [NSString stringWithFormat:@"(%@) -%@%%", price, [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", price]]];
                [self.detailInfo setAttributedText:string];
            } else {
                self.detailInfo.text = [NSString stringWithFormat:@"(%0.2f) -%@%%", [price floatValue], [Utils Object:[elem objectForKey:@"Sconto"] orPlaceholder:@"N.D."]];
                NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%0.2f)", [price floatValue]]]];
                [self.detailInfo setAttributedText:string];
            }
            break;
        }
    }
}

-(void)configureQuantityInventory:(NSDictionary*)elem index:(NSInteger)row variation:(int)variation{
    switch (row) {
        case 0:
            self.detailName.text = NSLocalizedString(@"EXPECTED_QUANTITY", @"EXPECTED_QUANTITY");
            self.detailInfo.text = [NSString stringWithFormat:@"%d", [[elem objectForKey:@"QtaEspositore"] intValue]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 1:
            self.detailName.text = NSLocalizedString(@"COUNTED_QUANTITY", @"COUNTED_QUANTITY");
            self.detailInfo.text = [NSString stringWithFormat:@"%d", [[elem objectForKey:@"QtaEspositore"] intValue]+variation];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = NSLocalizedString(@"VARIATION_QUANTITY", @"VARIATION_QUANTITY");
            self.detailInfo.text = [NSString stringWithFormat:@"%d", variation];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        default:
            break;
    }
}

@end
