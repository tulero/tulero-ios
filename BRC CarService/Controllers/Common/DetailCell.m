//
//  DetailCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailCell.h"
#import "Settings.h"

@implementation DetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCatalogo:(NSDictionary*)elem {
    self.detailCarrello.tag = 0; //Immagine Carrello
    self.detailCarrello.clipsToBounds = YES;
    self.detailCarrello.layer.cornerRadius = 30;

    self.detailService.text = NSLocalizedString(@"ORGANIZATION_DETAIL", @"ORGANIZATION_DETAIL");
    self.detailProduct.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."]];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* price = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.detailPrice.text = price;
    } else {
        double prezzo = [[elem objectForKey:@"Prezzo"] doubleValue];
        self.detailPrice.text = [NSString stringWithFormat:@"€ %0.2f",prezzo-(prezzo*sconto)/100];
    }
  
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.detailImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_product_detail"]];
    
    self.product = elem;
}

-(void)configureEspositore:(NSDictionary *)elem {
    self.detailCarrello.tag = 1; //Immagine download
    [self.detailCarrello setImage:[UIImage imageNamed: @"download_white_icon"] forState:UIControlStateNormal];
    self.detailCarrello.clipsToBounds = YES;
    self.detailCarrello.layer.cornerRadius = 30;
    self.detailCarrello.layer.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:188.0f/255.0f blue:27.0f/255.0f alpha:1.0].CGColor;
    
    self.detailService.text = NSLocalizedString(@"ORGANIZATION_DETAIL", @"ORGANIZATION_DETAIL");
    self.detailProduct.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* price = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.detailPrice.text = price;
    } else {
        double prezzo = [[elem objectForKey:@"Prezzo"] doubleValue];
        self.detailPrice.text = [NSString stringWithFormat:@"€ %0.2f",prezzo-(prezzo*sconto)/100];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.detailImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_product_detail"]];
    
    self.product = elem;
}

-(void)configureOrdine:(NSDictionary *)elem {
    self.detailCarrello.hidden = YES;
    self.detailCarrello.enabled = NO;
    self.detailService.text = NSLocalizedString(@"ORGANIZATION_DETAIL", @"ORGANIZATION_DETAIL");
    self.detailProduct.text = [Utils Object:[elem objectForKey:@"Descrizione1"] orPlaceholder:@"N.D."];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* price = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.detailPrice.text = price;
    } else {
        double prezzo = [[elem objectForKey:@"Prezzo"] doubleValue];
        self.detailPrice.text = [NSString stringWithFormat:@"€ %0.2f",prezzo-(prezzo*sconto)/100];
    }
    
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.detailImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_product_detail"]];
}

-(void)configureInventory:(NSDictionary*)elem{
    self.detailCarrello.hidden = YES;
    self.detailCarrello.enabled = NO;
    self.detailService.text = NSLocalizedString(@"ORGANIZATION_DETAIL", @"ORGANIZATION_DETAIL");
    self.detailProduct.text = [Utils Object:[elem objectForKey:@"Descrizione"] orPlaceholder:@"N.D."];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString* price = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.detailPrice.text = price;
    } else {
        double prezzo = [[elem objectForKey:@"Prezzo"] doubleValue];
        self.detailPrice.text = [NSString stringWithFormat:@"€ %0.2f",prezzo-(prezzo*sconto)/100];
    }
    
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Image"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.detailImage sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_product_detail"]];
}

- (IBAction)addProduct:(UIButton*)sender {
    if(sender.tag == 0){
        [KVNProgress show];
        [self.delegate addProduct:self.product];
        [KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSERIMENTO_RIUSCITO", @"INSERIMENTO_RIUSCITO")];
    } else if(sender.tag == 1){
        [self.downloadDelegate downloadProduct:self.product];
    }
}

@end
