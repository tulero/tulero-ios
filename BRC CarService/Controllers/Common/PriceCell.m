//
//  PriceCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//


#import "PriceCell.h"
#import "DetailProductCatalogoVC.h"
#import "MembershipManager.h"
#import "Settings.h"

@implementation PriceCell{
    MembershipManager *_manager;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.detailStepper.wraps = true;
    self.detailStepper.autorepeat = true;
    self.detailStepper.maximumValue = 100;
    self.detailStepper.minimumValue = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {

    self.product = elem;
    
    self.detailShop.clipsToBounds = YES;
    self.detailShop.layer.cornerRadius = 20;
    
    self.detailQuantity.layer.borderWidth = 1.0;
    self.detailQuantity.clipsToBounds = YES;
    self.detailQuantity.layer.cornerRadius = 4;
    if([self.product objectForKey:@"QtaOrder"]){
        [self.detailStepper setValue:[[self.product objectForKey:@"QtaOrder"] intValue]];
    } else {
        [self.detailStepper setValue:1];
    }
    self.detailQuantity.text = [NSString stringWithFormat:@"%.0f", self.detailStepper.value];
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    double fullPrice = [[elem objectForKey:@"Prezzo"] doubleValue];
    double price = (fullPrice - (fullPrice * sconto)/100) * self.detailStepper.value;
    
    NSString* str = [NSString stringWithFormat:NSLocalizedString(@"ADD_TO_CART", @"ADD_TO_CART"), price];
    UIFont *semiBoldFont = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    NSRange semiBoldedRange;
    
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    //NSLog(@"LINGUA: %@",language);
    if([language isEqualToString:@"italiano (Italia)"]){
        semiBoldedRange = NSMakeRange(20, str.length - 20);
    } else {
        semiBoldedRange = NSMakeRange(12, str.length - 12);
    }
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str];
    
    [attrString beginEditing];
    [attrString addAttribute:NSFontAttributeName
                       value:semiBoldFont
                       range:semiBoldedRange];
    
    [attrString endEditing];
    
    self.detailShop.titleLabel.attributedText = attrString;
    
    [self.detailShop setTitle:self.detailShop.titleLabel.text /*[NSString stringWithFormat:NSLocalizedString(@"ADD_TO_CART", @"ADD_TO_CART"), price]*/ forState:UIControlStateNormal];
   
}

- (IBAction)btnCnt:(UIStepper *)sender {
    double value = [sender value];
  
    if(value == sender.maximumValue){
        [sender setValue:value-1];
        value = value - 1;
    } else if(value == sender.minimumValue){
        [sender setValue:value + 1];
        value = value + 1;
    }
    NSMutableDictionary* _mut = self.product.mutableCopy;
    [_mut setObject:@(value) forKey:@"QtaOrder"];
    self.product = [[NSDictionary alloc] initWithDictionary:_mut];
    
    [self.delegate priceUpdated:value];
}

- (IBAction)addToCart:(id)sender {
    [KVNProgress show];
    _manager = [MembershipManager sharedManager];
    [_manager.cart addProduct:self.product];
    [KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSERIMENTO_RIUSCITO", @"INSERIMENTO_RIUSCITO")];
}

@end
