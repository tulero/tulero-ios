//
//  EspositoreVC.m
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EspositoreVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "InfoEspCell.h"
#import "EspositoreCell.h"
#import "DetailEspositoreVC.h"

@interface EspositoreVC ()

@end

@implementation EspositoreVC{
    MembershipManager* _manager;
    NSArray* _list;
    BOOL _isLoaded;
    double _valRiordino;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    _manager = [MembershipManager sharedManager];
    _isLoaded = NO;
    
    if(_manager.actualUser.isEspositore){
    
        [self initList];
        
    } else {
        //_list = _list = @[];
        _isLoaded = YES;
    }

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InfoEspCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoEspCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EspositoreCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EspositoreCell class])];
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_EXPOSITOR", @"TITLE_EXPOSITOR");
    [APP_DELEGATE sendPageViewGA:@"Exhibitor"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(void)initList{
    
    [KVNProgress show];
    
    [_manager getReorderValue:^(double val) {
        [KVNProgress dismiss];
        _valRiordino = val;
        _list = @[];
        
        [KVNProgress show];
        
        [_manager getCategoryListEspositore:^(NSArray *list) {
            [KVNProgress dismiss];
            _isLoaded = YES;
            _list = [[NSArray alloc] initWithArray:list];
            
            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
            [self.tableView reloadData];
            
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            _isLoaded = YES;
            
        }];
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
    
    
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(_manager.actualUser.isEspositore)
        return 2;
    else
        return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0) 
        return 1;
    else
        return _list.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        //tipo uno di cella
        InfoEspCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoEspCell class]) forIndexPath:indexPath];
        [cell configure: _valRiordino];
        return cell;
    } else {
        // tipo due
        EspositoreCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EspositoreCell class]) forIndexPath:indexPath];
        if(indexPath.row == 0) {
            [cell configureFirstCell];
            cell.hidden = YES;
        } else {
            [cell configure:_list[indexPath.row - 1]];
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 1 && indexPath.row!=0){
        //newList[indexPath.row]
        DetailEspositoreVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailEspositoreVC class])];
        
        detail.commodity = [_list[indexPath.row-1] objectForKey:@"Commodity"];
        detail.productCategory = [_list[indexPath.row-1] objectForKey:@"Descrizione"];
        
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 1 && indexPath.row == 0){
        return 0;
    } else if(indexPath.section == 0){
        return 90;
    } else {
        return 85;
    }
}

#pragma mark DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"espositore_placeholder_grey_icon"];
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = NSLocalizedString(@"EMPTY_EXPOSITOR", @"EMPTY_EXPOSITOR");
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return self.tableView.backgroundColor;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"EMPTY_EXPOSITOR_DESCR", @"EMPTY_EXPOSITOR_DESCR");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold],
                                 NSForegroundColorAttributeName: COLOR(255, 255, 255, 1),
                                 NSBackgroundColorAttributeName: COLOR(140, 140, 140, 1),
                                 };

    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"CALL", @"CALL") attributes:attributes];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    
    UIEdgeInsets capInsets = UIEdgeInsetsMake(22.0, 22.0, 22.0, 22.0);
    UIEdgeInsets rectInsets = UIEdgeInsetsMake(0.0, 0, 0.0, 0);
    
    UIImage *image = [UIImage imageNamed:@"button_orange_background"];

    return [[image resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch] imageWithAlignmentRectInsets:rectInsets];
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString: ASSISTENZA_NUMBER] options:@{} completionHandler:nil];
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
{
    return YES;
}

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return _isLoaded;
}

@end
