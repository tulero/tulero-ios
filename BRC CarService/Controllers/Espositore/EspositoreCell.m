//
//  EspositoreCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EspositoreCell.h"
#import "Settings.h"

@implementation EspositoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)drawRect:(CGRect)rect{
    
    /*** background view
    UIView *bg = [[UIView alloc] initWithFrame: self.frame];
    bg.backgroundColor = [UIColor clearColor];
    
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"].CGColor;
    sublayer.frame = self.frame;
    [bg.layer addSublayer:sublayer];
    
    self.selectedBackgroundView = bg;
    **/
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureFirstCell {
    self.productEspImg.image = [UIImage imageNamed:@"scarico_green_icon"];
    self.productName.text = @"Scarichi";
    self.productDescription.text = @"Descrizione";
    self.btnBadge.hidden = YES;
    //self.badge.text = @"";
}

-(void)configure:(NSDictionary*)elem {
//    NSString *path = [elem objectForKey:@"Immagine"];
//    if(!path || [path isKindOfClass:[NSNull class]]){
//        path = @"";
//    }
//
//    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
//    [self.productEspImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"edit_grey_icon"]];
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    
    [self.productEspImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    
    //self.productEspImg.image = [elem objectForKey:@"Immagine"];
    self.productName.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Descrizione"] orPlaceholder:@"-"]];
    self.productDescription.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Commodity"] orPlaceholder:@"-"]];
    [self.btnBadge setTitle:[NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"NCodici"] orPlaceholder:@"-"]] forState:UIControlStateNormal];
    //self.badge.text = [NSString stringWithFormat:@"%@", [elem objectForKey:@"Badge"]];
    self.btnBadge.layer.borderWidth = 1.0;
    self.btnBadge.clipsToBounds = YES;
    self.btnBadge.layer.cornerRadius = 8;
    CGFloat red = 233.0f/255.0f;
    CGFloat green = 71.0f/255.0f;
    CGFloat blue = 75.0f/255.0f;
    self.btnBadge.layer.borderColor = [[UIColor colorWithRed:red green:green blue:blue alpha:1.0f] CGColor];
}

-(void)configureForCatalogoCategory:(NSDictionary *)elem{
    /*{
        "Commodity": "BAT",
        "Descrizione": "Batteries",
        "Immagine": null,
        "NCodici": 42
    }*/
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    
    [self.productEspImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    
    //self.productEspImg.image = [elem objectForKey:@"Immagine"];
    self.productName.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Descrizione"] orPlaceholder:@"-"]];
    self.productDescription.text = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"Commodity"] orPlaceholder:@"-"]];
    [self.btnBadge setTitle:[NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"NCodici"] orPlaceholder:@"0"]] forState:UIControlStateNormal];
    //self.badge.text = [NSString stringWithFormat:@"%@", [elem objectForKey:@"Badge"]];
    self.btnBadge.layer.borderWidth = 1.0;
    self.btnBadge.clipsToBounds = YES;
    self.btnBadge.layer.cornerRadius = 8;
    CGFloat red = 233.0f/255.0f;
    CGFloat green = 71.0f/255.0f;
    CGFloat blue = 75.0f/255.0f;
    self.btnBadge.layer.borderColor = [[UIColor colorWithRed:red green:green blue:blue alpha:1.0f] CGColor];
    
}

@end
