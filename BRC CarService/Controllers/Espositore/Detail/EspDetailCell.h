//
//  EspDetailCell.h
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuantityUpdateDelegate;

@interface EspDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *quantityTextView;
@property (strong, nonatomic) IBOutlet UIStepper *espStepper;
@property (strong, nonatomic) IBOutlet UIButton *btnScaricaProd;
@property (strong, nonatomic) IBOutlet UIButton *btnAcquista;
@property NSString* codiceArticolo;
@property int quantita;
@property (strong, nonatomic) id<QuantityUpdateDelegate> delegate;

@property (strong, nonatomic) NSDictionary *product;

- (void)configure:(NSDictionary*)elem;
- (IBAction)btnCnt:(UIStepper *)sender;
- (IBAction)AcquistaCatalogo:(id)sender;
- (IBAction)ScaricoProdEsp:(id)sender;

@end

@protocol QuantityUpdateDelegate

-(void)updateQuantity:(int)qnt;

@end
