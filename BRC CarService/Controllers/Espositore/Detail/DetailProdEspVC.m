//
//  DetailProdEspVC.m
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailProdEspVC.h"
#import "DetailCell.h"
#import "infoCell.h"
#import "EspDetailCell.h"
#import "MembershipManager.h"
#import "Settings.h"

@interface DetailProdEspVC ()

@end

@implementation DetailProdEspVC{
    MembershipManager* _manager;
}

@synthesize product;
@synthesize productCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EspDetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EspDetailCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 474;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"DETAIL_PRODUCT", @"DETAIL_PRODUCT");
    [APP_DELEGATE sendPageViewGA:[NSString stringWithFormat:@"Detail Product Exhibitor: %@", product[@"CodArticolo"]]];
    //self.title = [[product objectForKey:@"NomeProdotto"] uppercaseString];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configureEspositore:product];
        cell.downloadDelegate = self;
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureEspositore:product index:indexPath.row category:productCategory];
        return cell;
    } else {
        EspDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EspDetailCell class]) forIndexPath:indexPath];
        [cell configure:product];
        cell.delegate = self;
        //NSLog(@"RICARICO CELLAAAAAAAA!!!!!!!!!");
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1) {
        return 44;
    } else {
        return 207;
    }
}

#pragma Quantity update delegate

-(void)updateQuantity:(int)qnt{
    //NSLog(@"RICARICO TABELLAAAAAAAA!!!!!!!!!");
    
    NSMutableDictionary * _mut = product.mutableCopy;
    [_mut setObject:@(qnt) forKey:@"QtaOrder"];
    product = [[NSDictionary alloc] initWithDictionary:_mut];
    [self.tableView reloadData];
    
}


#pragma Download delegate

-(void)downloadProduct:(NSDictionary *)prod{
    [KVNProgress show];
    _manager = [MembershipManager sharedManager];
    int qnt = 1;
    if(prod[@"QtaOrder"]){
        qnt = [prod[@"QtaOrder"] intValue];
    }
    [_manager downloadExpositor:qnt codArticolo:prod[@"CodArticolo"] onSuccess:^(NSString *str) {
        if([str hasPrefix:@"Operation complete. New stock is"]){
            [KVNProgress showSuccessWithStatus:NSLocalizedString(@"DOWNLOAD_COMPLETATO", @"DOWNLOAD_COMPLETATO")];
        } else if ([str hasPrefix:@"Stock is"]){
            [KVNProgress showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"DOWNLOAD_ERROR_QUANTITY", @"DOWNLOAD_ERROR_QUANTITY"),qnt]];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
        }
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
    }];
}

@end
