//
//  DetailProdEspVC.h
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCell.h"
#import "EspDetailCell.h"

@interface DetailProdEspVC : UIViewController<UITableViewDelegate, UITableViewDataSource,DownloadProductDelegate,QuantityUpdateDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSDictionary* product;
@property NSString* productCategory;

@end
