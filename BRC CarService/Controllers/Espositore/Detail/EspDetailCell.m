//
//  EspDetailCell.m
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EspDetailCell.h"
#import "Settings.h"
#import "MembershipManager.h"

@implementation EspDetailCell{
    MembershipManager* _manager;
    NSString* _acquisto;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.espStepper.wraps = true;
    self.espStepper.autorepeat = true;
    self.espStepper.maximumValue = 100;
    self.espStepper.minimumValue = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.product = elem;
    
    self.btnScaricaProd.clipsToBounds = YES;
    self.btnScaricaProd.layer.cornerRadius = 18;
    [self.btnScaricaProd setTitle:NSLocalizedString(@"SCARICO_PROD", @"SCARICO_PROD") forState: UIControlStateNormal];
    
    self.btnAcquista.clipsToBounds = YES;
    self.btnAcquista.layer.cornerRadius = 18;
    self.btnAcquista.layer.borderWidth = 1;
    self.btnAcquista.layer.borderColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f].CGColor;
    [self.btnAcquista setTitle:NSLocalizedString(@"ACQUISTO_DA_CATALOGO", @"ACQUISTO_DA_CATALOGO") forState:UIControlStateNormal];
    
    self.quantityTextView.layer.borderWidth = 1.0;
    self.quantityTextView.clipsToBounds = YES;
    self.quantityTextView.layer.cornerRadius = 4;
    if([self.product objectForKey:@"QtaOrder"]){
        [self.espStepper setValue:[[self.product objectForKey:@"QtaOrder"] intValue]];
    } else {
        [self.espStepper setValue:1];
    }
    self.quantityTextView.text = [NSString stringWithFormat:@"%.0f", self.espStepper.value];
    //self.quantityTextView.text = [NSString stringWithFormat:@"%d", 1];
    
    self.codiceArticolo = [elem objectForKey:@"CodArticolo"];
    self.quantita = 1;
}

- (IBAction)btnCnt:(UIStepper *)sender {
    double value = [sender value];
    if(value == sender.maximumValue){
        [sender setValue:value-1];
        value = value - 1;
    } else if(value == sender.minimumValue){
        [sender setValue:value + 1];
        value = value + 1;
    }
    
    self.quantityTextView.text = [NSString stringWithFormat:@"%d", (int)value];
    self.quantita = (int)value;
    
    NSMutableDictionary* _mut = self.product.mutableCopy;
    [_mut setObject:@(value) forKey:@"QtaOrder"];
    self.product = [[NSDictionary alloc] initWithDictionary:_mut];
    
    [self.delegate updateQuantity:self.quantita];
}

- (IBAction)AcquistaCatalogo:(id)sender {
    [KVNProgress show];
    _manager = [MembershipManager sharedManager];
    [_manager.cart addProduct:self.product];
    [KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSERIMENTO_RIUSCITO", @"INSERIMENTO_RIUSCITO")];
}

- (IBAction)ScaricoProdEsp:(id)sender {
    [KVNProgress show];

    _manager = [MembershipManager sharedManager];
    int qnt = 1;
    if(_product[@"QtaOrder"]){
        qnt = [_product[@"QtaOrder"] intValue];
    }

    [_manager downloadExpositor:qnt codArticolo:self.codiceArticolo onSuccess:^(NSString *str) {
        
        _acquisto = str;
        if([_acquisto hasPrefix:@"Operation complete. New stock is"]){
            [KVNProgress showSuccessWithStatus:NSLocalizedString(@"DOWNLOAD_COMPLETATO", @"DOWNLOAD_COMPLETATO")];
        } else if([_acquisto hasPrefix:@"Stock is"]) {
            [KVNProgress showErrorWithStatus: [NSString stringWithFormat: NSLocalizedString(@"DOWNLOAD_ERROR_QUANTITY", @"DOWNLOAD_ERROR_QUANTITY"),qnt]];
        }
        NSLog(@"%@",str);
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
}

@end
