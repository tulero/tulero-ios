//
//  DetailEspositoreVC.m
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailEspositoreVC.h"
#import "ProductCell.h"
#import "MembershipManager.h"
#import "Settings.h"
#import "DetailProdEspVC.h"

@interface DetailEspositoreVC ()

@end

@implementation DetailEspositoreVC{
    MembershipManager* _manager;
    NSArray* _list;
}

@synthesize commodity;
@synthesize productCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    _manager = [MembershipManager sharedManager];
    [self initList];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = [productCategory uppercaseString];
    [APP_DELEGATE sendPageViewGA:@"Detail Exhibitor"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @" ";
    [self initList];
}

-(NSArray*)filterList:(NSArray*)_list{
    
    NSPredicate *predicateToShow = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if( evaluatedObject[@"Commodity"] &&
           [evaluatedObject[@"Commodity"] isEqualToString:commodity] &&
           [evaluatedObject[@"IsEspositore"] intValue] == 1 // se arrivo da catalogo non faccio questo controllo, se arrivo da espositore si
           ){
            return YES;
        }
        return NO;
    }];
    
    return [_list filteredArrayUsingPredicate:predicateToShow];
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getProducts:^(NSArray *list) {
        [KVNProgress dismiss];
        _list = [[NSArray alloc] initWithArray:list];
        _list = [self filterList:_list];
        
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
    [cell configureEspositore:_list[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    DetailProdEspVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProdEspVC class])];
    detail.product = _list[indexPath.row];
    detail.productCategory = productCategory;
    
    [self.navigationController pushViewController:detail animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}

@end
