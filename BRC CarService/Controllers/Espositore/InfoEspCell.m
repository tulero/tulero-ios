//
//  InfoEspCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "InfoEspCell.h"

@implementation InfoEspCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(double)val {
    
    if(val >= 150) {
        self.infoEspLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ESPOSITORE_VALORE_RIORDINO", @"ESPOSITORE_VALORE_RIORDINO"),0.0];
    } else {
        self.infoEspLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ESPOSITORE_VALORE_RIORDINO", @"ESPOSITORE_VALORE_RIORDINO"),150 - val];
    }
    
    self.infoEspView.clipsToBounds = YES;
    self.infoEspView.layer.cornerRadius = 8;
}

@end
