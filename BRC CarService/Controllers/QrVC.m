//
//  QrVC.m
//  arredanet
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import "QrVC.h"
#import "QRCodeReader.h"
#import "QRCodeReaderView.h"
#import "Settings.h"
#import <AVFoundation/AVFoundation.h>
#import "MembershipManager.h"

@interface QrVC (){
    AVAudioPlayer *_audioPlayer;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoMessage;

@property (weak, nonatomic) IBOutlet UIImageView *loader1;
@property (weak, nonatomic) IBOutlet UIImageView *loader2;
@property (weak, nonatomic) IBOutlet UIImageView *loader3;
@property (weak, nonatomic) IBOutlet UIView *topOverlay;
@property (weak, nonatomic) IBOutlet UIView *bottomOverlay;
@end

@implementation QrVC {
    NSArray* _phrase;
    NSTimer* _phraseTimer;
    long indexPhrase;
    QRCodeReader *_reader;
    QRCodeReaderView *_cameraView;
    NSDictionary* lastProductScanned;
    NSMutableArray* _productsScanned;
    BOOL flagSound;
    MembershipManager* _manager;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
//-(BOOL)prefersStatusBarHidden {
//    return YES;
//}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if([self.navigationController respondsToSelector:@selector(applyTransparentNavBar)]){
        [self.navigationController performSelector:@selector(applyTransparentNavBar)];
    }

    [self showScanner];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    _reader.previewLayer.frame = self.view.bounds;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self hideScanner];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _productsScanned = [[NSMutableArray alloc] initWithArray:@[].mutableCopy];
    flagSound = NO;
    _manager = [MembershipManager sharedManager];
    
    // Construct URL to sound file
    NSString *path = [NSString stringWithFormat:@"%@/Beep-tone.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    self.title = NSLocalizedString(@"SCANNER_TITLE", @"SCANNER_TITLE");
    
    UIBarButtonItem* btnClose = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_white_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCloseTapped:)];
    btnClose.tintColor = [UIColor whiteColor];
    UIBarButtonItem* btnFlash = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"flash"] style:UIBarButtonItemStylePlain target:self action:@selector(btnFlashTapped:)];
    self.navigationItem.leftBarButtonItem = btnClose;
    self.navigationItem.rightBarButtonItem = btnFlash;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startLoader];
    });
    
    [self setupScanner];
    
    _phrase = @[NSLocalizedString(@"SCANNER_DESCR_1", @"SCANNER_DESCR_1"), NSLocalizedString(@"SCANNER_DESCR_2", @"SCANNER_DESCR_2"), NSLocalizedString(@"SCANNER_DESCR_3", @"SCANNER_DESCR_3")];
    _lblInfoMessage.text = _phrase[indexPhrase];
    
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = self.btnConferma.frame.size.height/2;
    
    //[self setNeedsStatusBarAppearanceUpdate];
    [APP_DELEGATE sendPageViewGA:@"Scanner Barcode"];
}

-(void)changePhrase:(id)sender {
    indexPhrase++;
    if (indexPhrase >= _phrase.count) {
        indexPhrase = 0;
    }
    
    [UIView animateWithDuration:1 animations:^{
        _lblInfoMessage.alpha = 0;
        _lblInfoMessage.text = _phrase[indexPhrase];
        _lblInfoMessage.alpha = 1;
    }];
}

-(void)startLoader {
    [self runSpinAnimationOnView:_loader1 duration:3 inverse:NO];
    [self runSpinAnimationOnView:_loader2 duration:2 inverse:YES];
    [self runSpinAnimationOnView:_loader3 duration:1 inverse:NO];
}
-(void)stopLoader {
    [_loader1.layer removeAnimationForKey:@"SpinAnimation"];
    [_loader2.layer removeAnimationForKey:@"SpinAnimation"];
    [_loader3.layer removeAnimationForKey:@"SpinAnimation"];
}

- (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration inverse:(BOOL)inverse;
{
    if ([view.layer animationForKey:@"SpinAnimation"] == nil) {
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        if (inverse) {
            animation.fromValue = [NSNumber numberWithFloat:2*M_PI];
            animation.toValue = [NSNumber numberWithFloat:0.0f];
        }
        else {
            animation.fromValue = [NSNumber numberWithFloat:0.0f];
            animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
        }
        
        animation.duration = duration;
        animation.repeatCount = INFINITY;
        [view.layer addAnimation:animation forKey:@"SpinAnimation"];
    }
}

-(void)btnCloseTapped:(id)sender {
    if(_manager.actualUser.isAgente){
        [self.delegate productScanned:_productsScanned];
        [self dismissViewControllerAnimated:NO completion:nil];
    } else {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    //PUBLISH([EventScannerDismissed new]);
}
-(void)btnFlashTapped:(id)sender {
    [_reader toggleTorch];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideScanner {
    [_phraseTimer invalidate];
    _phraseTimer = nil;
    indexPhrase = 0;
}
    
-(void)showScanner {
    indexPhrase = 0;
    _lblInfoMessage.text = _phrase[indexPhrase];
    _phraseTimer = [NSTimer timerWithTimeInterval:5 target:self selector:@selector(changePhrase:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_phraseTimer forMode:NSDefaultRunLoopMode];
    _lblErrorMessage.alpha = 0;
}

-(void)setupScanner {
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code]]) {
        _reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code]];
        [self setupUIComponents];
        //[self setupAutoLayoutConstraints];
        [self.contentView.layer insertSublayer:_reader.previewLayer atIndex:0];
        
         __weak __typeof__(self) weakSelf = self;
        [_reader setCompletionWithBlock:^(NSString *result) {
            [weakSelf readScanResults:result];
        }];
        
        [_reader startScanning];
    }
    else {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"SCANNER_NOT_SUPPORTED", @"SCANNER_NOT_SUPPORTED") completion:nil];
    }
}

- (void)setupUIComponents
{
    UIColor* color = [UIColor blackColor];
    [_topOverlay layoutIfNeeded];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topOverlay.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[color colorWithAlphaComponent:0.8] CGColor],(id)[[color colorWithAlphaComponent:0] CGColor], nil];
    [_topOverlay.layer insertSublayer:gradient atIndex:0];
    
    [_bottomOverlay layoutIfNeeded];
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    gradient2.frame = _bottomOverlay.bounds;
    gradient2.colors = [NSArray arrayWithObjects:(id)[[color colorWithAlphaComponent:0] CGColor],(id)[[color colorWithAlphaComponent:1] CGColor], nil];
    [_bottomOverlay.layer insertSublayer:gradient2 atIndex:0.6];
    
    
    [self.contentView layoutIfNeeded];
    [_reader.previewLayer setFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    
    if ([_reader.previewLayer.connection isVideoOrientationSupported]) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        _reader.previewLayer.connection.videoOrientation = [QRCodeReader videoOrientationFromInterfaceOrientation:orientation];
    }
}

- (void)setupAutoLayoutConstraints
{
    NSDictionary *views = NSDictionaryOfVariableBindings(_cameraView);
    
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_cameraView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cameraView]|" options:0 metrics:nil views:views]];
}

-(void)readScanResults:(NSString*)result {
    
    _lblErrorMessage.text = result;
    
    /*
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAutoreverse animations:^{
        _lblErrorMessage.alpha = 1;
    } completion:^(BOOL finished) {
        _lblErrorMessage.alpha = 0;
        [_reader startScanning];
    }];
     */

    for(NSDictionary *dic in self.productList ){
        
        if([dic[@"EAN"] isEqualToString:result]){
            //[_reader stopScanning];
            if(flagSound && ![lastProductScanned[@"EAN"] isEqualToString:result]){
                [_audioPlayer play];
            }
            lastProductScanned = dic;
            
            [self updateConfirmBtn];
            //[self.delegate productScanned:dic];
            //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    /*
    DDLogDebug(@"result QRCode: %@", result);
    
    if ([result rangeOfString:@"www."].location == 0) {
        result = [NSString stringWithFormat:@"http://%@", result];
    }
    
    if ([self qrIsInternal:result]) {
        NSURL* url = [NSURL URLWithString:result];
        
        //[APP_DELEGATE trackGoogleEvent:@"qrcode" action:@"scan-ok" label:result];
        
        [KVNProgress showWithStatus:@"attendi..."];
        NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
        requestObj.HTTPShouldHandleCookies = YES;
        
        [self hideScanner];
        
        [self dismissViewControllerAnimated:YES completion:^{
     
     
        }];
    }
    else {
        NSURL* url = [NSURL URLWithString:result];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            //[APP_DELEGATE trackGoogleEvent:@"qrcode" action:@"scan-ok" label:result];
            [[UIApplication sharedApplication] openURL:url];
            [self dismissViewControllerAnimated:NO completion:nil];
        }
        else {
            //[APP_DELEGATE trackGoogleEvent:@"qrcode" action:@"scan-ko" label:result];
            
            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAutoreverse animations:^{
                _lblErrorMessage.alpha = 1;
            } completion:^(BOOL finished) {
                _lblErrorMessage.alpha = 0;
                [_reader startScanning];
            }];
        }
    }
     */
}

-(void)updateConfirmBtn{
    if(!flagSound){
        [_audioPlayer play];
        flagSound = YES;
    }
    self.lblInfoMessage.hidden = YES;
    self.btnConferma.hidden = NO;
    if(lastProductScanned[@"Descrizione"]){
        [self.btnConferma setTitle:[NSString stringWithFormat:NSLocalizedString(@"SCANNER_BTN_TITLE", @"SCANNER_BTN_TITLE"), lastProductScanned[@"Descrizione"]] forState:UIControlStateNormal];
    } else {
        [self.btnConferma setTitle:[NSString stringWithFormat:NSLocalizedString(@"SCANNER_BTN_TITLE", @"SCANNER_BTN_TITLE"), lastProductScanned[@"Descrizione1"]] forState:UIControlStateNormal];
    }
    
}

//-(BOOL)isPresent:(NSDictionary*)elem{
//    for(NSDictionary* d in _productsScanned){
//        if([d[@"EAN"] isEqualToString:elem[@"EAN"]]){
//            return YES;
//        }
//    }
//    return NO;
//}

-(BOOL)qrIsInternal:(NSString*)result {
    return [result rangeOfString:@"lacasamoderna.com/qr-code"].location != NSNotFound;
}

- (IBAction)onConferma:(id)sender {
    self.btnConferma.hidden = YES;
    self.lblInfoMessage.hidden = NO;
    //if(![self isPresent:lastProductScanned]){
    [_productsScanned addObject:lastProductScanned];
    flagSound = NO;
    if(!_manager.actualUser.isAgente){
        [self.delegate productScanned:_productsScanned];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    //}
    //[self.delegate productScanned:lastProductScanned];
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}
@end
