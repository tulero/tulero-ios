//
//  QrVC.h
//  arredanet
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BarcodeReaderDelegate;

@interface QrVC : UIViewController

@property (strong, nonatomic) NSArray<NSDictionary*>* productList;
@property (strong, nonatomic) id<BarcodeReaderDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;

- (IBAction)onConferma:(id)sender;
@end

@protocol BarcodeReaderDelegate

-(void)productScanned: (NSMutableArray* )productsScanned;
//-(void)eanScanned:(NSDictionary*) product;

@end
