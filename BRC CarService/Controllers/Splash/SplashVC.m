//
//  SplashVC.m
//  TripCityMap
//
//  Created by Fabio Donatelli
//  Copyright (c) 2015 Etinet s.r.l. All rights reserved.
//

#import "SplashVC.h"
#import "AppDelegate.h"
#import "Tolo.h"
//#import "Utils.h"
#import "KVNProgress.h"
#import "MembershipManager.h"
#import "LoginNVC.h"
//#import "MainNVC.h"
#import "Settings.h"
#import "LoginNVC.h"
#import "MainNVC.h"
#import "AgenteNVC.h"

@interface SplashVC ()

@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UIImageView *imgSplash;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation SplashVC
{
    BOOL prevConnectivityOK;
    int nSerFails;
    
    BOOL _catOK;
    BOOL _serOK;
    BOOL _tagOK;
    BOOL _cityOK;
    
    //LocalStoreManager* _localStoreManager;
    MembershipManager* _membershipManager;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //APP_DELEGATE.actualVC = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DDLogDebug(@"%@",self.class);
    
    [_activityIndicator setColor:DEFAULT_COLOR];
    
    [_activityIndicator startAnimating];
    
    //REGISTER();
    
    //IMAGE
    // Load launch image
    NSArray *allPngImageNames = [[NSBundle mainBundle] pathsForResourcesOfType:@"png"
                                                                   inDirectory:nil];
    for (NSString *imgName in allPngImageNames){
        // Find launch images
        NSLog(@"--------- %@", imgName);
        if ([imgName containsString:@"LaunchImage"]){
            UIImage *img = [UIImage imageNamed:imgName];
            // Has image same scale and dimensions as our current device's screen?
            if (img.scale == [UIScreen mainScreen].scale && CGSizeEqualToSize(img.size, [UIScreen mainScreen].bounds.size)) {
                DDLogDebug(@"Found launch image for current device %@", img.description);
                _imgSplash.image = img;
            }
        }
    }

    //_localStoreManager = [LocalStoreManager sharedManager];
    _membershipManager = [MembershipManager sharedManager];
    //_membershipManager.dgxService = [DGXService sharedManager];
    
    KVNProgressConfiguration *configuration = [KVNProgressConfiguration defaultConfiguration];
    configuration.statusFont = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
    configuration.successColor = COLOR_GREEN;
    configuration.errorColor = DEFAULT_COLOR;
    configuration.showStop = YES;
    configuration.fullScreen = YES;
    configuration.minimumSuccessDisplayTime = 2;
    configuration.minimumErrorDisplayTime = 4;
    configuration.tapBlock = ^(KVNProgress *progressView) {
        // Do what you want
        //[KVNProgress dismiss];
    };
    
    [KVNProgress setConfiguration:configuration];
    
    MembershipManager *member = [MembershipManager sharedManager];
    
    [member initCommonts:^(NSArray *array) {
        NSLog(@"COMMON INIT SUCCESS");
        [self start];
    } onFailure:^(NSError *error) {
        NSLog(@"ERROR COMMON INIT");
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR",@"GENERIC_ERROR")];
    }];

    
}

-(void)start {
    //looking for local user is logged in
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* userSaved = [defaults objectForKey:@"user"];
    
    if(userSaved){
        User * _actualUser = [[User alloc] initWithDictionary:[defaults objectForKey:@"user"]];
        _membershipManager.actualUser = _actualUser;
       
        if(_actualUser.isRememberMe){
            [_membershipManager loginWithUsername:_actualUser.username password:_actualUser.password remember: YES onSuccess:^(NSDictionary *array) {
                if(_actualUser.isAgente){
                    [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
                } else {
                    [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
                }
                //[APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])];
                //[APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
                
            } onFailure:^(NSError *error) {
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([LoginNVC class])];
            }];
            // faccio login
            // login con successo : [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])]; isAgente: false
            //[APP_DELEGATE setRootViewControllerByName:NSStringFromClass([AgenteNVC class])]; isAgente : true
        }else{
            // non sono loggato
            [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([LoginNVC class])];
        }
    } else {
        [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([LoginNVC class])];
    }
    /*
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
    }
  
    
    if (_membershipManager.actualUser.token && ![_membershipManager.actualUser.token isEqualToString:@""]) {
        //autologin...
     
        NSLog(@"AUTOLOGIN");
        [_membershipManager autologin:^(NSDictionary *array) {
            _membershipManager.actualUser.isLoggedIn = YES;
            [_activityIndicator stopAnimating];
            NSLog(@"AUTENTICATO");
            
            if(!APP_DELEGATE.isDEEPLINK){
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
            }
            
        } onFailure:^(NSError *error) {
            [_activityIndicator stopAnimating];
            NSLog(@"NON AUTENTICATO");
            if(!APP_DELEGATE.isDEEPLINK){
                [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
            }
        }];
     
        //GO to DESTINATION VC (MAIN or another one if exists a push destination directive)
        
    }else{
     
        if(_membershipManager.actualUser){
            _membershipManager.actualUser.isLoggedIn = NO;
        }
        [_activityIndicator stopAnimating];
        NSLog(@"NON AUTENTICATO");
        if(!APP_DELEGATE.isDEEPLINK){
            [APP_DELEGATE setRootViewControllerByName:NSStringFromClass([MainNVC class])];
        }
    }
     */
}

-(void)checkStart {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma TOLO EVENTS

/*
 SUBSCRIBE(EventOptionsRequestCompleted)
 {
 if ([APP_DELEGATE.localStoreManager appIsUpdated]) {
 
 [APP_DELEGATE.dgxService getAllCities];
 //[APP_DELEGATE.dgxService getAllCategories];
 [APP_DELEGATE.dgxService getAllServices];
 [APP_DELEGATE.dgxService getAllTags];
 }
 else {
 DDLogWarn(@"APP necessita di aggiornamento");
 //NEW APP ON STORE!
 UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Nuovo aggiornamento disponibile" message:@"Per continuare è necessario scaricare la nuova versione dall'app store." preferredStyle:UIAlertControllerStyleAlert];
 UIAlertAction* storeAction = [UIAlertAction actionWithTitle:@"Vai allo store" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 NSString* path = STORE_URL;
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
 }];
 [alertController addAction:storeAction];
 [self presentViewController:alertController animated:YES completion:nil];
 }
 }
 
 SUBSCRIBE(EventCheckStart) {
 
 if ([event.type isEqualToString:@"CAT"])
 _catOK = YES;
 else if ([event.type isEqualToString:@"SER"])
 _serOK = YES;
 else if ([event.type isEqualToString:@"TAG"])
 _tagOK = YES;
 else if ([event.type isEqualToString:@"CITY"])
 _cityOK = YES;
 
 [self checkStart];
 }
 
 SUBSCRIBE(ConnectivityEvent)
 {
 if (APP_DELEGATE.networkOk) {
 [APP_DELEGATE.dgxService getAllOptions];
 }
 else {
 //[APP_DELEGATE checkConnectivity];
 }
 }
 */
@end
