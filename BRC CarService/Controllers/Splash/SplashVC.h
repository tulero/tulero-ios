//
//  SplashVC.h
//  TripCityMap
//
//  Created by Fabio Donatelli
//  Copyright (c) 2015 Etinet s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashVC : UIViewController
@property (strong, nonatomic) UIWindow *window;
@property (retain, strong) UIStoryboard* mainStoryboard;
@property (retain, strong) NSString* globalToken;
@property BOOL isDEEPLINK;

-(void)setRootViewControllerByName:(NSString*)vcName;

-(void)sendPageViewGA:(NSString*) page;


@end
