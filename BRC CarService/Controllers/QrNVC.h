//
//  QrNVC.h
//  arredanet
//
//  Created by Fabio Donatelli
//  Copyright © 2016 DigitalX srl. All rights reserved.
//

//#import "BaseNVC.h"
#import <UIKit/UIKit.h>


@interface QrNVC : UINavigationController
-(void)applyTranslucentNavBar;
-(void)applyTransparentNavBar;
-(void)applyWhiteNavBar;
@end
