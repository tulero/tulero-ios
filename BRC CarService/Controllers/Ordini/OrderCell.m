//
//  OrderCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "OrderCell.h"
#import "Settings.h"

@implementation OrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem price:(float)price{
    NSString *status = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Stato"] orPlaceholder:@"-"]];
    if([status isEqualToString:@"ORDER_SHIPPED"]) {
        self.orderImg.image = [UIImage imageNamed:@"status_black_icon"];
        self.orderStatus.text = NSLocalizedString(@"STATUS_SHIPPED", @"STATUS_SHIPPED");
        self.orderStatus.textColor = [UIColor blackColor];
    } else if([status isEqualToString:@"ORDER_RECEIVED"]) {
        self.orderImg.image = [UIImage imageNamed:@"status_grey_icon"];
        self.orderStatus.text = NSLocalizedString(@"STATUS_RECEIVED", @"STATUS_RECEIVED");
        self.orderStatus.textColor = [UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
    } else {
        self.orderImg.image = [UIImage imageNamed: @"status_red_icon"];
        self.orderStatus.text = NSLocalizedString(@"STATUS_SHIPPING_PROGRESS", @"STATUS_SHIPPING_PROGRESS");
        self.orderStatus.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
    }
    NSString *date = [Utils formatISODateOrders:[Utils Object:[elem objectForKey:@"DataSpedizione"] orPlaceholder:@""] toFormat:@"dd/MM/yyyy"];
    self.orderDate.text = [NSString stringWithFormat:@"%@",[Utils Object:date orPlaceholder:@"-"]];
    self.orderNum.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_ID", @"ORDER_ID"),[Utils Object:[elem objectForKey:@"IDOrdine"] orPlaceholder:@""]];
//    NSString* przUnitario = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"PrzUnitario"] orPlaceholder:@"-"]];
//    NSString* qnt = [NSString stringWithFormat:@"%@",[Utils Object:[elem objectForKey:@"QtaOriginale"] orPlaceholder:@"-"]];
//    int qntInt = 0;
//    if(![qnt isEqualToString:@"-"]){
//       qntInt = [qnt intValue];
//    }
//    float przNum = 0;
//    if(![przUnitario isEqualToString:@"-"]){
//        przNum = [przUnitario floatValue];
//    }
//    float przTot = przNum * qntInt;
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString *productCount = [NSString stringWithFormat:@"%lu", (unsigned long)((NSArray*)[elem objectForKey:@"products"]).count];
    self.orderPrice.text = [NSString stringWithFormat:NSLocalizedString(@"INFO_ORDER", @"INFO_ORDER"), price /*- (price * sconto)/100 */,[Utils Object:productCount orPlaceholder:@"N.D."]];

}

@end
