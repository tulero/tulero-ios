//
//  SummaryOrderCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SummaryOrderCell.h"
#import "Settings.h"

@implementation SummaryOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem price:(double)price{
    
    self.viewContent.clipsToBounds = YES;
    self.viewContent.layer.cornerRadius = 8;
    
    NSString* date = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"DataSpedizione"] orPlaceholder:@"-"]];
    if(![date isEqualToString:@"-"]){
        date = [Utils formatISODateOrders:date toFormat:@"dd/MM/yyyy"];
    }
    int sconto = 0;
    if(elem[@"Sconto"]){
        sconto = [elem[@"Sconto"] intValue];
    }
    NSString *productCount = [NSString stringWithFormat:@"%lu", (unsigned long)((NSArray*)[elem objectForKey:@"products"]).count];
    //double price = [elem[@"PrzUnitario"] doubleValue];
    self.summaryPrice.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_PRICE_TOT", @"ORDER_PRICE_TOT"), price, [Utils Object:productCount orPlaceholder:@"N.D."], date];
    self.summaryNumber.text = [NSString stringWithFormat: NSLocalizedString(@"ORDER_NUMBER", @"ORDER_NUMBER"), [Utils Object:[elem objectForKey:@"IDOrdine"] orPlaceholder:@"N.D."]];
    NSString *status = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Stato"] orPlaceholder:@"N.D."]];
    if([status isEqualToString:@"ORDER_SHIPPED"]){
        self.summaryImage.image = [UIImage imageNamed:@"spedito_black_icon"];
        self.summaryStatus.text = [NSString stringWithFormat:NSLocalizedString(@"STATUS_TITLE", @"STATUS_TITLE"), NSLocalizedString(@"STATUS_SHIPPED", @"STATUS_SHIPPED")];
    } else if ([status isEqualToString:@"ORDER_RECEIVED"]){
        self.summaryImage.image = [UIImage imageNamed:@"attesa_grey_icon"];
        self.summaryStatus.text = [NSString stringWithFormat:NSLocalizedString(@"STATUS_TITLE", @"STATUS_TITLE"), NSLocalizedString(@"STATUS_RECEIVED", @"STATUS_RECEIVED")];
    } else {
        self.summaryImage.image = [UIImage imageNamed:@"accettato_red_icon"];
        self.summaryStatus.text = [NSString stringWithFormat:NSLocalizedString(@"STATUS_TITLE", @"STATUS_TITLE"), NSLocalizedString(@"STATUS_SHIPPING_PROGRESS", @"STATUS_SHIPPING_PROGRESS")];
    }
    
}

@end
