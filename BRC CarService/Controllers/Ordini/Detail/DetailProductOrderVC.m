//
//  DetailProductOrderVC.m
//  BRC CarService
//
//  Created by Etinet on 05/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailProductOrderVC.h"
#import "Settings.h"
#import "DetailCell.h"
#import "infoCell.h"

@interface DetailProductOrderVC ()

@end

@implementation DetailProductOrderVC

@synthesize product;
@synthesize qnt;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 474;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"DETAIL_PRODUCT", @"DETAIL_PRODUCT");
    [APP_DELEGATE sendPageViewGA:[NSString stringWithFormat:@"Detail Product Order: %@", product[@"CodArticolo"]]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 6;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configureOrdine: product];
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureOrdine: product index:indexPath.row];
        return cell;
    } else {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureInfoOrdine:product index:indexPath.row quantity: qnt];
        //cell.delegate = self;
        //NSLog(@"RICARICO CELLAAAAAAAA!!!!!!!!!");
        return cell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1) {
        return 44;
    } else {
        return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 2)
        return 10;
    else
        return 0;
}


@end
