//
//  DetailOrderVC.h
//  BRC CarService
//
//  Created by Etinet on 04/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailOrderVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSArray* orderDetail;
@property NSDictionary* order;
@property double orderPrice;

@end
