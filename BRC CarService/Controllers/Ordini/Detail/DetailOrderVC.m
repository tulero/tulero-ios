//
//  DetailOrderVC.m
//  BRC CarService
//
//  Created by Etinet on 04/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailOrderVC.h"
#import "SummaryOrderCell.h"
#import "ProductCell.h"
#import "Settings.h"
#import "DetailProductOrderVC.h"
#import "MembershipManager.h"

@interface DetailOrderVC ()

@end

@implementation DetailOrderVC {
    //float _orderPrice;
    MembershipManager* _manager;
}

@synthesize orderDetail;
@synthesize order;
@synthesize orderPrice;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SummaryOrderCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SummaryOrderCell class])];
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _manager = [MembershipManager sharedManager];
    
    //_orderPrice = 0;

    [KVNProgress show];
    NSMutableArray* tmp = @[].mutableCopy;
    int index = 0;
    long counter = (unsigned long)((NSArray*)self.order[@"products"]).count;
    
    for(NSDictionary *d in self.order[@"products"]){
        
         [_manager getProductById:[d[@"CodBreve"] stringValue] onSuccess:^(NSArray *object) {
          
             [tmp addObject:object[0]];
             
             if(index == counter-1){
                 //ho finito la getProductByID di tutti gli elementi di self.order[@"products"]
                 [self loadData:[[NSArray alloc] initWithArray:tmp]];
             }
             
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"") completion:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                NSLog(@"Error");
         }];
        
        index++;
    }

}

-(void)loadData:(NSArray*)data{
    [KVNProgress dismiss];
    self.orderDetail = data;
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = [NSString stringWithFormat:NSLocalizedString(@"ORDER_ID_TITLE", @"ORDER_ID_TITLE"), [order objectForKey:@"IDOrdine"]];
    [APP_DELEGATE sendPageViewGA:[NSString stringWithFormat:@"Detail Order: %@",[order objectForKey:@"IDOrdine"]]];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @" ";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return 1;
    return orderDetail.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0){
        //tipo uno di cella
        SummaryOrderCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SummaryOrderCell class]) forIndexPath:indexPath];
        [cell configure:order price:orderPrice];
        return cell;
    } else {
        // tipo due
        ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
        if([[order objectForKey:@"products"][indexPath.row] objectForKey:@"QtaOriginale"]){
            [cell configureOrdine:orderDetail[indexPath.row] quantity:[[[order objectForKey:@"products"][indexPath.row] objectForKey:@"QtaOriginale"] intValue]];
        } else {
            [cell configureOrdine:orderDetail[indexPath.row] quantity:0];
        }
        
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 1){
        
        DetailProductOrderVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductOrderVC class])];

        detail.product = orderDetail[indexPath.row];
        if([[order objectForKey:@"products"][indexPath.row] objectForKey:@"QtaOriginale"]){
            detail.qnt = [[[order objectForKey:@"products"][indexPath.row] objectForKey:@"QtaOriginale"] intValue];
        } else {
            detail.qnt = 0;
        }
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0){
        return 115;
    } else {
        return 98;
    }
}

@end
