//
//  OrdiniVC.m
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "OrdiniVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "OrderCell.h"
#import "DetailOrderVC.h"

@interface OrdiniVC ()

@end

@implementation OrdiniVC{
    MembershipManager* _manager;
    NSArray* _list;
    BOOL _isLoaded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    _manager = [MembershipManager sharedManager];
    _isLoaded = NO;
    //[self initList];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([OrderCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([OrderCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_ORDER", @"TITLE_ORDER");
    [APP_DELEGATE sendPageViewGA:@"Orders"];
    [self initList];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getOrders:^(NSArray *list) {
        [KVNProgress dismiss];
        _isLoaded = YES;
        _list = [self grouppedList:list];
        
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        _isLoaded = YES;
        
    }];
    
}

-(NSArray* )grouppedList:(NSArray*)_orders{
    
    NSMutableDictionary* _tmp = @{}.mutableCopy;
    
    for(NSDictionary* d in _orders){
        if(_tmp[d[@"IDOrdine"]]){
            [(NSMutableArray*)_tmp[d[@"IDOrdine"]][@"products"] addObject:d];
        }else{
            NSMutableDictionary* _orderTmp = d.mutableCopy;
            [_orderTmp setObject:@[_orderTmp].mutableCopy forKey:@"products"];
            [_tmp setObject:_orderTmp forKey:d[@"IDOrdine"]];
            
        }
    }
    
    NSMutableArray *_return = @[].mutableCopy;
    for(NSString * key in [_tmp allKeys]){
        [_return addObject:_tmp[key]];
    }
    
    return [[NSArray alloc] initWithArray:_return];
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderCell class]) forIndexPath:indexPath];
    
    float price = 0;
    for(NSDictionary* d in [_list[indexPath.row] objectForKey:@"products"]){
        price = price + ([[d objectForKey:@"QtaOriginale"] floatValue] * [[d objectForKey:@"PrzUnitario"] floatValue]);
    }
    
    [cell configure:_list[indexPath.row] price:price];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    float price = 0;
    for(NSDictionary* d in [_list[indexPath.row] objectForKey:@"products"]){
        price = price + ([[d objectForKey:@"QtaOriginale"] floatValue] * [[d objectForKey:@"PrzUnitario"] floatValue]);
    }
    
    DetailOrderVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailOrderVC class])];
    
    detail.order = _list[indexPath.row];
    detail.orderPrice = price;
    [self.navigationController pushViewController:detail animated:YES];
    
}

#pragma mark DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"ordini_placeholder_grey_icon"];
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = NSLocalizedString(@"EMPTY_ORDER", @"EMPTY_ORDER");
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return self.tableView.backgroundColor;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"EMPTY_ORDER_DESCR", @"EMPTY_ORDER_DESCR");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
{
    return YES;
}

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return _isLoaded;
}

@end
