//
//  SearchBRCCodeVC.m
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchBRCCodeVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "DetailProductCatalogoVC.h"
#import "DetailProdEspVC.h"

@interface SearchBRCCodeVC ()

@end

@implementation SearchBRCCodeVC{
    NSArray* _list;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = 18;
    [self.btnConferma setTitle:NSLocalizedString(@"BTN_CONFERMA", @"BTN_CONFERMA") forState:UIControlStateNormal];
    
    self.codeBRCView.clipsToBounds = YES;
    self.codeBRCView.layer.cornerRadius = 8;
    
    self.searchDescription.text = NSLocalizedString(@"SEARCH_CODE_DETAIL", @"SEARCH_CODE_DETAIL");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH_BRC_CODE", @"TITLE_SEARCH_BRC_CODE");
    [APP_DELEGATE sendPageViewGA:@"Search Product by BRC Code"];
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

- (IBAction)searchBRCCode:(id)sender {
    
    _list = @[];
    MembershipManager* _manager = [MembershipManager sharedManager];
    
    [KVNProgress show];
    NSString* code = self.searchBRCCode.text;
    code = [code uppercaseString];
    
    NSString *pattern = @"^[A-Z]{3}[0-9]+$";
    NSError  *error = nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    NSUInteger matches = [regex numberOfMatchesInString:code options:0 range:NSMakeRange(0,[code length])];
    if(matches != 1) {
        NSLog(@"Input non valido");
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"CODE_INPUT_ERROR", @"")];
    } else {
        NSArray* codes = [[NSArray alloc] initWithObjects:code, nil];
        [_manager searchProductByBRCCode:codes onSuccess:^(NSArray *list) {
            if(list && [list isKindOfClass:[NSArray class]] && list.count>0){
                [KVNProgress dismiss];
                _list = [[NSArray alloc] initWithArray:list];
                NSLog(@"%@",_list[0]);
                if([_list[0] objectForKey:@"IsEspositore"]==0){
                    DetailProductCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductCatalogoVC class])];
                    detail.product = _list[0];
                    detail.productCategory = [_list[0] objectForKey:@"Commodity"];
                    [self.navigationController pushViewController:detail animated:YES];
                } else {
                    DetailProdEspVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProdEspVC class])];
                    detail.product = _list[0];
                    detail.productCategory = [_list[0] objectForKey:@"Commodity"];
                    [self.navigationController pushViewController:detail animated:YES];
                }
            } else {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"CODE_NOT_FOUND", @"")];
            }
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        }];
    }
}
@end
