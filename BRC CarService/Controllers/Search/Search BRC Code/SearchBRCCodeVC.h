//
//  SearchBRCCodeVC.h
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBRCCodeVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *searchDescription;
@property (strong, nonatomic) IBOutlet UITextField *searchBRCCode;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;
@property (strong, nonatomic) IBOutlet UIView *codeBRCView;
- (IBAction)searchBRCCode:(id)sender;

@end
