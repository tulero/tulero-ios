//
//  DetailCarCell.h
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCarCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *versioneTitle;
@property (strong, nonatomic) IBOutlet UILabel *versionePotenza;
@property (strong, nonatomic) IBOutlet UILabel *versioneMotore;
@property (strong, nonatomic) IBOutlet UILabel *versioneCaratteristiche;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImg;

@end
