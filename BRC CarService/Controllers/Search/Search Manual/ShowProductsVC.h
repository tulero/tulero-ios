//
//  ShowProductsVC.h
//  BRC CarService
//
//  Created by Etinet on 07/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowProductsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property NSArray* products;
@property NSString* valvole;

@property NSDictionary* car;

@end
