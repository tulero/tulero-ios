//
//  SearchMarcaVC.h
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface SearchMarcaVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *arrCars;

-(void)loadData;

@end
