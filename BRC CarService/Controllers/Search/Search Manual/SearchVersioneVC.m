//
//  SearchVersioneVC.m
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchVersioneVC.h"
#import "DetailCarCell.h"
#import "MembershipManager.h"
#import "Settings.h"
#import "ShowProductsVC.h"

@interface SearchVersioneVC ()

@end

@implementation SearchVersioneVC{
    MembershipManager* _manager;
    NSArray* _list;
}

@synthesize marca;
@synthesize modello;
@synthesize modelloAnno;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    _manager = [MembershipManager sharedManager];
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"NuovoDBVeicoli.sql"];
    
    NSString *query = [@"select * from cars where Marca = '" stringByAppendingString:self.marca];
    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
    query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
    query = [query stringByAppendingString:@"' and Alimentazione = 'B' group by Versione"];
    
    // Get the results.
    if (self.arrCarsB != nil) {
        self.arrCarsB = nil;
    }
    self.arrCarsB = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if([self.arrCarsB count]>0){
        self.cnt = self.cnt + 1;
    }
    
    query = [@"select * from cars where Marca = '" stringByAppendingString:self.marca];
    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
    query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
    query = [query stringByAppendingString:@"' and Alimentazione = 'D' group by Versione"];
    
    // Get the results.
    if (self.arrCarsD != nil) {
        self.arrCarsD = nil;
    }
    self.arrCarsD = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if([self.arrCarsD count]>0){
        self.cnt = self.cnt + 1;
    }
    
    query = [@"select * from cars where Marca = '" stringByAppendingString:self.marca];
    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
    query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
    query = [query stringByAppendingString:@"' and Alimentazione = 'G' group by Versione"];
    
    // Get the results.
    if (self.arrCarsG != nil) {
        self.arrCarsG = nil;
    }
    self.arrCarsG = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if([self.arrCarsG count]>0){
        self.cnt = self.cnt + 1;
    }
    
    query = [@"select * from cars where Marca = '" stringByAppendingString:self.marca];
    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
    query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
    query = [query stringByAppendingString:@"' and Alimentazione = 'M' group by Versione"];
    
    // Get the results.
    if (self.arrCarsM != nil) {
        self.arrCarsM = nil;
    }
    self.arrCarsM = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    if([self.arrCarsM count]>0){
        self.cnt = self.cnt + 1;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCarCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCarCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH_VERSION", @"TITLE_SEARCH_VERSION");
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0) {
        return [self.arrCarsB count];
    } else if(section == 1) {
        return [self.arrCarsD count];
    } else if(section == 2) {
        return [self.arrCarsG count];
    } else
        return [self.arrCarsM count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        DetailCarCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCarCell class]) forIndexPath:indexPath];
        cell.versioneTitle.text = [NSString stringWithFormat:@"%@ (%@ / %@)", [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:5], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:6], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:7]];
        cell.versionePotenza.text = [NSString stringWithFormat:@"%@ Cm³ - %@ Kw - %@ Cv", [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:8], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:9], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:10]];
        cell.versioneMotore.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLO", @"DETTAGLI_VEICOLO"), [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:11], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:12]];
        cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLI_BODY_BENZINA", @"DETTAGLI_VEICOLI_BODY_BENZINA"), [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:14], [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:15]];
        return cell;
    } else if(indexPath.section == 1) {
        DetailCarCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCarCell class]) forIndexPath:indexPath];
        cell.versioneTitle.text = [NSString stringWithFormat:@"%@ (%@ / %@)", [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:5], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:6], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:7]];
        cell.versionePotenza.text = [NSString stringWithFormat:@"%@ Cm³ - %@ Kw - %@ Cv", [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:8], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:9], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:10]];
        cell.versioneMotore.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLO", @"DETTAGLI_VEICOLO"), [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:11], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:12]];
        cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLI_BODY_DIESEL", @"DETTAGLI_VEICOLI_BODY_DIESEL"), [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:14], [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:15]];
        return cell;
    } else if(indexPath.section == 2) {
        DetailCarCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCarCell class]) forIndexPath:indexPath];
        cell.versioneTitle.text = [NSString stringWithFormat:@"%@ (%@ / %@)", [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:5], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:6], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:7]];
        cell.versionePotenza.text = [NSString stringWithFormat:@"%@ Cm³ - %@ Kw - %@ Cv", [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:8], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:9], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:10]];
        cell.versioneMotore.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLO", @"DETTAGLI_VEICOLO"), [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:11], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:12]];
        cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLI_BODY_GPL", @"DETTAGLI_VEICOLI_BODY_GPL"), [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:14], [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:15]];
        return cell;
    } else {
        DetailCarCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCarCell class]) forIndexPath:indexPath];
        cell.versioneTitle.text = [NSString stringWithFormat:@"%@ (%@ / %@)", [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:5], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:6], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:7]];
        cell.versionePotenza.text = [NSString stringWithFormat:@"%@ Cm³ - %@ Kw - %@ Cv", [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:8], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:9], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:10]];
        cell.versioneMotore.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLO", @"DETTAGLI_VEICOLO"), [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:11], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:12]];
        cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLI_BODY_METANO", @"DETTAGLI_VEICOLI_BODY_METANO"), [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:14], [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:15]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 0) {
        [KVNProgress show];
        
        NSString *query = [@"select CodiceArticolo from cars where Marca = '" stringByAppendingString:self.marca];
        query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
        query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
        query = [[query stringByAppendingString:@"' and Versione = '"] stringByAppendingString:[[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:5]];
        query = [query stringByAppendingString:@"' and Alimentazione = 'B'"];
        
        NSArray *codes = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        NSMutableArray* tmp = @[].mutableCopy;
        for(NSMutableArray* arr in codes){
            [tmp addObject:arr[0]];
        }
        NSArray* codici = [[NSArray alloc] initWithArray:tmp];
        
        if(codici.count>0){
            [_manager searchProductByBRCCode:codici onSuccess:^(NSArray *array) {
                if(array.count == 0){
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND2", @"")];
                } else {
                    [KVNProgress dismiss];
                    //NSLog(@"%@",array);
                    _list = [[NSArray alloc] initWithArray:array];
                    ShowProductsVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ShowProductsVC class])];
                    detail.products = _list;
                    //detail.versione = [[self.arrCarsB objectAtIndex:indexPath.row] objectAtIndex:5];
                    [self.navigationController pushViewController:detail animated:YES];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            }];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND", @"")];
        }
    } else if (indexPath.section == 1){
        [KVNProgress show];
        
        NSString *query = [@"select CodiceArticolo from cars where Marca = '" stringByAppendingString:self.marca];
        query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
        query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
        query = [[query stringByAppendingString:@"' and Versione = '"] stringByAppendingString:[[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:5]];
        query = [query stringByAppendingString:@"' and Alimentazione = 'D'"];
       
        NSArray *codes = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        NSMutableArray* tmp = @[].mutableCopy;
        for(NSMutableArray* arr in codes){
            [tmp addObject:arr[0]];
        }
        NSArray* codici = [[NSArray alloc] initWithArray:tmp];
        if(codici.count>0){
            [_manager searchProductByBRCCode:codici onSuccess:^(NSArray *array) {
                if(array.count == 0){
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND2", @"")];
                } else {
                    [KVNProgress dismiss];
                    //NSLog(@"%@",array);
                    _list = [[NSArray alloc] initWithArray:array];
                    ShowProductsVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ShowProductsVC class])];
                    detail.products = _list;
                    //detail.versione = [[self.arrCarsD objectAtIndex:indexPath.row] objectAtIndex:5];
                    [self.navigationController pushViewController:detail animated:YES];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            }];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND", @"")];
        }
    } else if(indexPath.section==2){
        [KVNProgress show];
        
        NSString *query = [@"select CodiceArticolo from cars where Marca = '" stringByAppendingString:self.marca];
        query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
        query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
        query = [[query stringByAppendingString:@"' and Versione = '"] stringByAppendingString:[[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:5]];
        query = [query stringByAppendingString:@"' and Alimentazione = 'G'"];
        
        NSArray *codes = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        NSMutableArray* tmp = @[].mutableCopy;
        for(NSMutableArray* arr in codes){
            [tmp addObject:arr[0]];
        }
        NSArray* codici = [[NSArray alloc] initWithArray:tmp];
        
        if(codici.count>0){
            [_manager searchProductByBRCCode:codici onSuccess:^(NSArray *array) {
                if(array.count == 0){
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND2", @"")];
                } else {
                    [KVNProgress dismiss];
                    //NSLog(@"%@",array);
                    _list = [[NSArray alloc] initWithArray:array];
                    ShowProductsVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ShowProductsVC class])];
                    detail.products = _list;
                    //detail.versione = [[self.arrCarsG objectAtIndex:indexPath.row] objectAtIndex:5];
                    [self.navigationController pushViewController:detail animated:YES];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            }];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND", @"")];
        }
    } else {
        [KVNProgress show];
        
        NSString *query = [@"select CodiceArticolo from cars where Marca = '" stringByAppendingString:self.marca];
        query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
        query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:self.modelloAnno];
        query = [[query stringByAppendingString:@"' and Versione = '"] stringByAppendingString:[[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:5]];
        query = [query stringByAppendingString:@"' and Alimentazione = 'M'"];
        
        NSArray *codes = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        NSMutableArray* tmp = @[].mutableCopy;
        for(NSMutableArray* arr in codes){
            [tmp addObject:arr[0]];
        }
        NSArray* codici = [[NSArray alloc] initWithArray:tmp];
        
        if(codici.count>0){
            [_manager searchProductByBRCCode:codici onSuccess:^(NSArray *array) {
                if(array.count == 0){
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND2", @"")];
                } else {
                    [KVNProgress dismiss];
                    //NSLog(@"%@",array);
                    _list = [[NSArray alloc] initWithArray:array];
                    ShowProductsVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ShowProductsVC class])];
                    detail.products = _list;
                    //detail.versione = [[self.arrCarsM objectAtIndex:indexPath.row] objectAtIndex:5];
                    [self.navigationController pushViewController:detail animated:YES];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            }];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND", @"")];
        }
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if(section == 0){
        if(self.arrCarsB.count > 0){
            return [self viewTitleForSection:NSLocalizedString(@"BENZINA", @"BENZINA")];
        }else{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
    }else if(section == 1){
        if(self.arrCarsD.count > 0){
            return [self viewTitleForSection:NSLocalizedString(@"DIESEL", @"DIESEL")];
        }else{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
    }else if(section == 2){
        if(self.arrCarsG.count > 0){
            return [self viewTitleForSection:NSLocalizedString(@"GPL", @"GPL")];
        }else{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
    }else{
        if(self.arrCarsM.count > 0){
            return [self viewTitleForSection:NSLocalizedString(@"METANO", @"METANO")];
        }else{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        if(self.arrCarsB.count > 0){
            return 30;
        }else{
            return 0;
        }
    }else if(section == 1){
        if(self.arrCarsD.count > 0){
            return 30;
        }else{
            return 0;
        }
    }else if(section == 2){
        if(self.arrCarsG.count > 0){
            return 30;
        }else{
            return 0;
        }
    }else{
        if(self.arrCarsM.count > 0){
            return 30;
        }else{
            return 0;
        }
    }
}

-(void)loadData {
    // Form the query.
    NSString *query = [@"select * from cars where Marca = '" stringByAppendingString:self.marca];
    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:self.modello];
    query = [query stringByAppendingString:@"' and Alimentazione = 'B'"];
    
    // Get the results.
    if (self.arrCars != nil) {
        self.arrCars = nil;
    }
    self.arrCars = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    // Reload the table view.
    [self.tableView reloadData];
}

-(UIView*)viewTitleForSection:(NSString*)string{
    
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    
    CGRect frame = CGRectMake(20, 5, viewHeader.frame.size.width-20, 20);
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:frame];
    [lbl setTextColor:[UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f]];
    [lbl setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
    
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = string;
    [viewHeader addSubview:lbl];
    return viewHeader;
    
}
@end
