//
//  SearchVersioneVC.h
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface SearchVersioneVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property NSString* marca;
@property NSString* modello;
@property NSString* modelloAnno;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *arrCars;
@property (nonatomic, strong) NSArray *arrCarsB;
@property (nonatomic, strong) NSArray *arrCarsD;
@property (nonatomic, strong) NSArray *arrCarsG;
@property (nonatomic, strong) NSArray *arrCarsM;
@property int cnt;

-(void)loadData;

@end
