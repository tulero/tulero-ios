//
//  SearchModelloVC.m
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchModelloVC.h"
#import "SearchVersioneVC.h"

@interface SearchModelloVC ()

@end

@implementation SearchModelloVC

@synthesize marca;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"NuovoDBVeicoli.sql"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Load the data.
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH_MODEL", @"TITLE_SEARCH_MODEL");
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrCars.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Dequeue the cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idModelloCell" forIndexPath:indexPath];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"idModelloCell"];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    NSInteger indexOfModello = [self.dbManager.arrColumnNames indexOfObject:@"Modello"];
    NSInteger indexOfModelloAnno = [self.dbManager.arrColumnNames indexOfObject:@"ModelloAnno"];
    
    // Set the loaded data to the appropriate cell labels.
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", [[self.arrCars objectAtIndex:indexPath.row] objectAtIndex:indexOfModello], [[self.arrCars objectAtIndex:indexPath.row] objectAtIndex:indexOfModelloAnno]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    SearchVersioneVC *versione = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchVersioneVC class])];
    versione.marca = [NSString stringWithFormat:@"%@",[[self.arrCars objectAtIndex: indexPath.row] objectAtIndex:2]];
    versione.modello = [NSString stringWithFormat:@"%@",[[self.arrCars objectAtIndex: indexPath.row] objectAtIndex:3]];
    versione.modelloAnno = [NSString stringWithFormat:@"%@",[[self.arrCars objectAtIndex: indexPath.row] objectAtIndex:4]];
    [self.navigationController pushViewController:versione animated:YES];
}

-(void)loadData {
    // Form the query.
    NSString *query = [[@"select * from cars where Marca = '" stringByAppendingString:self.marca] stringByAppendingString:@"' group by Modello, ModelloAnno;"];
    
    // Get the results.
    if (self.arrCars != nil) {
        self.arrCars = nil;
    }
    self.arrCars = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    // Reload the table view.
    [self.tableView reloadData];
}


@end
