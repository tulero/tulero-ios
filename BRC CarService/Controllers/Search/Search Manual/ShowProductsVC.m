//
//  ShowProductsVC.m
//  BRC CarService
//
//  Created by Etinet on 07/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ShowProductsVC.h"
#import "MembershipManager.h"
#import "Settings.h"
#import "ProductCell.h"
#import "DetailProductCatalogoVC.h"
#import "DetailProdEspVC.h"
#import "DetailCarCell.h"

@interface ShowProductsVC ()

@end

@implementation ShowProductsVC{
    MembershipManager *_manager;
}

@synthesize products;
@synthesize valvole;
@synthesize car;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCarCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCarCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _manager = [MembershipManager sharedManager];
    
    //NSLog(@"NUM. VALVOLE: %@",valvole);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_white_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"PRODOTTI_TROVATI", @"PRODOTTI_TROVATI");
    [APP_DELEGATE sendPageViewGA:@"Available Products"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(IBAction)closeView:(id)sender{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 1;
    } else {
        return products.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        DetailCarCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCarCell class]) forIndexPath:indexPath];
        if(car){
            cell.versioneTitle.text = [NSString stringWithFormat:@"%@ (%@ / %@)", car[@"Versione"], car[@"Dal"], car[@"Al"]];
            cell.versionePotenza.text = [NSString stringWithFormat:@"%@ Cm³ - %@ Kw - %@ Cv", car[@"Cm3"], car[@"Kw"], car[@"Cv"]];
            cell.versioneMotore.text = [NSString stringWithFormat:NSLocalizedString(@"DETTAGLI_VEICOLO", @"DETTAGLI_VEICOLO"), car[@"Motore"], valvole];
            if([car[@"Alim"] isEqualToString:@"B"]){
                cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"INFO_VEICOLO_BENZINA", @"INFO_VEICOLO_BENZINA"), car[@"Body"]];
            } else if([car[@"alim"] isEqualToString:@"D"]){
                cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"INFO_VEICOLO_DIESEL", @"INFO_VEICOLO_DIESEL"), car[@"Body"]];
            } else if([car[@"alim"] isEqualToString:@"M"]){
                cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"INFO_VEICOLO_METANO", @"INFO_VEICOLO_METANO"), car[@"Body"]];
            } else {
                cell.versioneCaratteristiche.text = [NSString stringWithFormat:NSLocalizedString(@"INFO_VEICOLO_GPL", @"INFO_VEICOLO_GPL"), car[@"Body"]];
            }
            cell.arrowImg.hidden = YES;
            cell.userInteractionEnabled = NO;
        } else {
            cell.hidden = YES;
        }
        return cell;
    } else {
        ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
        [cell configureCatalogo:products[indexPath.row]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(indexPath.section == 1){
        if([[products[indexPath.row] objectForKey:@"IsEspositore"] intValue]==0){
            DetailProductCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductCatalogoVC class])];
            detail.product = products[indexPath.row];
            detail.productCategory = [products[indexPath.row] objectForKey:@"Commodity"];
            [self.navigationController pushViewController:detail animated:YES];
        } else {
            DetailProdEspVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProdEspVC class])];
            detail.product = products[indexPath.row];
            detail.productCategory = [products[indexPath.row] objectForKey:@"Commodity"];
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(section == 0){
        if(products.count>0 && car){
            return [self viewTitleForSection:[NSString stringWithFormat:@"%@ - %@(%@)",car[@"Marca"], car[@"Modello"], car[@"Anno"]]];
        }else{
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
    } else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        if(products.count > 0 && car){
            return 40;
        }else{
            return 0;
        }
    }else {
        return 0;
    }
}

-(UIView*)viewTitleForSection:(NSString*)string{
    
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    
    CGRect frame = CGRectMake(20, 10, viewHeader.frame.size.width-20, 20);
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:frame];
    [lbl setTextColor:[UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f]];
    [lbl setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
    
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = string;
    [viewHeader addSubview:lbl];
    return viewHeader;
    
}
    

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(car){
        return 98;
    } else if(indexPath.section==0){
        return 0;
    } else {
        return 98;
    }
}

@end
