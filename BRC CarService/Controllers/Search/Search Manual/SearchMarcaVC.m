//
//  SearchMarcaVC.m
//  FirstProject
//
//  Created by Etinet on 16/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchMarcaVC.h"
#import "SearchModelloVC.h"
#import "Settings.h"

@interface SearchMarcaVC ()

@end

@implementation SearchMarcaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"NuovoDBVeicoli.sql"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Load the data.
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH_BRAND", @"TITLE_SEARCH_BRAND");
    [APP_DELEGATE sendPageViewGA:@"Manual Search"];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrCars.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Dequeue the cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idMarcaCell" forIndexPath:indexPath];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"idMarcaCell"];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    NSInteger indexOfMarca = [self.dbManager.arrColumnNames indexOfObject:@"Marca"];
    
    // Set the loaded data to the appropriate cell labels.
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [[self.arrCars objectAtIndex:indexPath.row] objectAtIndex:indexOfMarca]];
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    SearchModelloVC *modello = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchModelloVC class])];
    modello.marca = [NSString stringWithFormat:@"%@",[[self.arrCars objectAtIndex: indexPath.row] objectAtIndex:2]];
    [self.navigationController pushViewController:modello animated:YES];
}

-(void)loadData {
    // Form the query.
    NSString *query = @"select * from cars group by Marca;";
    
    // Get the results.
    if (self.arrCars != nil) {
        self.arrCars = nil;
    }
    self.arrCars = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    // Reload the table view.
    [self.tableView reloadData];
}


@end
