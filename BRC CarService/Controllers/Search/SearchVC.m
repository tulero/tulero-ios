//
//  SearchVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchVC.h"
#import "SearchCell.h"
#import "SearchEANManualVC.h"
#import "SearchTargaVC.h"
#import "SearchBRCCodeVC.h"
#import "SearchMarcaVC.h"
#import "Settings.h"
#import "DetailProductCatalogoVC.h"
#import "DetailProdEspVC.h"
#import "MembershipManager.h"


@interface SearchVC ()

@end

@implementation SearchVC {
    NSArray* ricercaTarga;
    NSArray* ricercaEAN;
    MembershipManager* _manager;
    NSArray* _list;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ricercaTarga = @[
                @{
                    @"searchImg": [UIImage imageNamed:@"scan_targa_red_icon"],
                    @"searchTitle": NSLocalizedString(@"TITLE_SEARCH_CAR_LICENSE_PLATE_1", @"TITLE_SEARCH_CAR_LICENSE_PLATE_1"),
                    @"searchDescription": NSLocalizedString(@"SEARCH_DESCR_CAR_LICENSE_PLATE_1", @"SEARCH_DESCR_CAR_LICENSE_PLATE_1")
                    },
                @{
                    @"searchImg": [UIImage imageNamed:@"insert_targa_black_icon"],
                    @"searchTitle": NSLocalizedString(@"TITLE_SEARCH_CAR_LICENSE_PLATE_2", @"TITLE_SEARCH_CAR_LICENSE_PLATE_2"),
                    @"searchDescription": NSLocalizedString(@"SEARCH_DESCR_CAR_LICENSE_PLATE_2", @"SEARCH_DESCR_CAR_LICENSE_PLATE_2")
                    }
                ];
    
    ricercaEAN = @[
                     @{
                         @"searchImg": [UIImage imageNamed:@"scan_barcode_red_icon"],
                         @"searchTitle": NSLocalizedString(@"TITLE_SEARCH_EAN_1", @"TITLE_SEARCH_EAN_1"),
                         @"searchDescription": NSLocalizedString(@"SEARCH_DESCR_EAN_1", @"SEARCH_DESCR_EAN_1")
                         },
                     @{
                         @"searchImg": [UIImage imageNamed:@"insert_code_black_icon"],
                         @"searchTitle": NSLocalizedString(@"TITLE_SEARCH_EAN_2", @"TITLE_SEARCH_EAN_2"),
                         @"searchDescription": NSLocalizedString(@"SEARCH_DESCR_EAN_2", @"SEARCH_DESCR_EAN_2")
                         }
                     ];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /** back image nav bar **/
    UIImage *backButtonImage = [[UIImage imageNamed:@"back_white_icon-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationController.navigationBar.backIndicatorImage = backButtonImage;
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = backButtonImage;
    
    [self initList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH", @"TITLE_SEARCH");
    [APP_DELEGATE sendPageViewGA:@"Search"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @" ";
}

-(void)initList{
    [KVNProgress show];
    _list = @[];
    _manager = [MembershipManager sharedManager];
    [_manager getProducts:^(NSArray *list) {
        [KVNProgress dismiss];
        _list = [[NSArray alloc] initWithArray:list];
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1) {
        return [ricercaTarga count];
    } else if(section == 3) {
        return [ricercaEAN count];
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        NSDictionary* search = @{
                                 @"searchImg" : [UIImage imageNamed:@"insert_code_black_icon"],
                                 @"searchTitle" : NSLocalizedString(@"TITLE_SEARCH_CODE", @"TITLE_SEARCH_CODE"),
                                 @"searchDescription" : NSLocalizedString(@"SEARCH_DESCR_CODE", @"SEARCH_DESCR_CODE")
                                 };
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:search];
        return cell;
    } else if(indexPath.section == 1){
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:ricercaTarga[(indexPath.row)]];
        
        if(indexPath.row == 0){
            cell.hidden = YES;
        }
        
        if(_manager.actualUser.ntarghe == 0){
            cell.userInteractionEnabled = NO;
        }
        
        return cell;
    } else if(indexPath.section == 2){
        NSDictionary* search = @{
                                 @"searchImg" : [UIImage imageNamed:@"veicolo_black_icon"],
                                 @"searchTitle" : NSLocalizedString(@"TITLE_SEARCH_MANUAL", @"TITLE_SEARCH_MANUAL"),
                                 @"searchDescription" : NSLocalizedString(@"SEARCH_DESCR_MANUAL", @"SEARCH_DESCR_MANUAL")
                                 };
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:search];
        return cell;
    } else {
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:ricercaEAN[(indexPath.row)]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section==3 && indexPath.row==1){
        SearchEANManualVC *searchEAN = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchEANManualVC class])];
        [self.navigationController pushViewController:searchEAN animated:YES];
    } else if(indexPath.section==1 && indexPath.row==1){
        SearchTargaVC *targa = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchTargaVC class])];
        [self.navigationController pushViewController:targa animated:YES];
    } else if(indexPath.section == 0 && indexPath.row == 0) {
        SearchBRCCodeVC *codeBRC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchBRCCodeVC class])];
        [self.navigationController pushViewController:codeBRC animated:YES];
    } else if(indexPath.section == 2 && indexPath.row == 0) {
        SearchMarcaVC *marca = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchMarcaVC class])];
        [self.navigationController pushViewController:marca animated:YES];
    } else if(indexPath.section == 3 && indexPath.row == 0){
        
        QrVC* qrViewController = [APP_DELEGATE.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([QrVC class])];
        qrViewController.delegate = self;
        qrViewController.productList = _list;
        
        QrNVC* qrNVC = [[QrNVC alloc] initWithRootViewController:qrViewController];
        qrNVC.modalPresentationStyle = UIModalPresentationPageSheet;
        [self presentViewController:qrNVC animated:YES completion:nil];
       
//        [KVNProgress show];
//        _list = @[];
//        _manager = [MembershipManager sharedManager];
//        [_manager getProducts:^(NSArray *list) {
//            [KVNProgress dismiss];
//            _list = [[NSArray alloc] initWithArray:list];
//            QrVC* qrViewController = [APP_DELEGATE.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([QrVC class])];
//            qrViewController.delegate = self;
//            qrViewController.productList = _list;
//
//            QrNVC* qrNVC = [[QrNVC alloc] initWithRootViewController:qrViewController];
//            qrNVC.modalPresentationStyle = UIModalPresentationPageSheet;
//            [self presentViewController:qrNVC animated:YES completion:nil];
//        } onFailure:^(NSError *error) {
//            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
//        }];
        
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];

    CGRect frame = CGRectMake(20, 24, viewHeader.frame.size.width-20, 18);
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:frame];
    [lbl setTextColor:[UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f]];
    [lbl setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
    
    lbl.textAlignment = NSTextAlignmentLeft;
    if(section == 0) {
        lbl.text = NSLocalizedString(@"SEARCH_TITLE_SECTION_CODE", @"SEARCH_TITLE_SECTION_CODE");
    } else if(section == 1) {
        lbl.text = [NSString stringWithFormat: NSLocalizedString(@"SEARCH_TITLE_SECTION_CAR_LICENSE_PLATE", @"SEARCH_TITLE_SECTION_CAR_LICENSE_PLATE"), _manager.actualUser.ntarghe];
    } else if(section == 2) {
        lbl.text = NSLocalizedString(@"SEARCH_TITLE_SECTION_MANUAL", @"SEARCH_TITLE_SECTION_MANUAL");
    } else {
        lbl.text = NSLocalizedString(@"SEARCH_TITLE_SECTION_EAN", @"SEARCH_TITLE_SECTION_EAN");
    }
    
    [viewHeader addSubview:lbl];
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if(section == 1 )
//        return 0;
//    else
    return 50;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"DATA!";
}*/

/*- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
 
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    UIView* viewHeader = [[UIView alloc] init];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    [header setBackgroundView:viewHeader];
    header.textLabel.textColor = [UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f];
    header.textLabel.font = [UIFont boldSystemFontOfSize:14];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentLeft;
    if(section == 0) {
        header.textLabel.text = @"RICERCA PER CODICE";
    } else if(section == 1) {
        header.textLabel.text = @"RICERCA PER TARGA (50 utilizzi residui)";
    } else if(section == 2) {
        header.textLabel.text = @"RICERCA PER VEICOLO";
    } else {
        header.textLabel.text = @"RICERCA PER  CODICE EAN";
    }
}*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1 && indexPath.row == 0){
        return 0;
    } else {
        return 85;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeView:(id)sender {
}

#pragma BarCode Scanner delegate

-(void)productScanned:(NSMutableArray *)productsScanned{
    if([productsScanned[0] objectForKey:@"IsEspositore"]==0){
        DetailProductCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductCatalogoVC class])];
        detail.product = productsScanned[0];
        detail.productCategory = [productsScanned[0] objectForKey:@"Commodity"];
        [self.navigationController pushViewController:detail animated:YES];
    } else {
        DetailProdEspVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProdEspVC class])];
        detail.product = productsScanned[0];
        detail.productCategory = [productsScanned[0] objectForKey:@"Commodity"];
        [self.navigationController pushViewController:detail animated:YES];
    }
}
@end
