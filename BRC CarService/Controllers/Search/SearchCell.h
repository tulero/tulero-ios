//
//  SearchCell.h
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *searchImg;
@property (strong, nonatomic) IBOutlet UILabel *searchTitle;
@property (strong, nonatomic) IBOutlet UILabel *searchDescription;

-(void) configure:(NSDictionary*) elem;

@end
