//
//  SearchEANManualVC.h
//  BRC CarService
//
//  Created by Etinet on 30/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchEANManualVC : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *codeEAN;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;
@property (strong, nonatomic) IBOutlet UIView *codeEANView;
@property (strong, nonatomic) IBOutlet UILabel *descrLabel;
- (IBAction)SearchEANCode:(id)sender;

@end
