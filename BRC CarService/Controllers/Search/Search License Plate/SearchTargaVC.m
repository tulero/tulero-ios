//
//  SearchTargaVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchTargaVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "DBManager.h"
#import "ShowProductsVC.h"

@interface SearchTargaVC ()

@end

@implementation SearchTargaVC{
    MembershipManager* _manager;
    NSString* _key;
    NSDictionary* _car;
    DBManager* _dbManager;
    NSArray* _list;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = 18;
    [self.btnConferma setTitle:NSLocalizedString(@"BTN_CONFERMA", @"BTN_CONFERMA") forState:UIControlStateNormal];
    
    self.descrLabel.text = NSLocalizedString(@"SEARCH_TARGA_DESCR", @"SEARCH_TARGA_DESCR");
    
    self.targaView.clipsToBounds = YES;
    self.targaView.layer.cornerRadius = 8;
    
    self.targa1.delegate = self;
    self.targa1.layer.borderWidth = 1;
    self.targa1.clipsToBounds = YES;
    self.targa1.layer.cornerRadius = 4;
    self.targa1.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa1];
    
    self.targa2.delegate = self;
    self.targa2.layer.borderWidth = 1;
    self.targa2.clipsToBounds = YES;
    self.targa2.layer.cornerRadius = 4;
    self.targa2.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa2];
    
    self.targa3.delegate = self;
    self.targa3.layer.borderWidth = 1;
    self.targa3.clipsToBounds = YES;
    self.targa3.layer.cornerRadius = 4;
    self.targa3.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa3];
    
    self.targa4.delegate = self;
    self.targa4.layer.borderWidth = 1;
    self.targa4.clipsToBounds = YES;
    self.targa4.layer.cornerRadius = 4;
    self.targa4.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa4];
    
    self.targa5.delegate = self;
    self.targa5.layer.borderWidth = 1;
    self.targa5.clipsToBounds = YES;
    self.targa5.layer.cornerRadius = 4;
    self.targa5.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa5];
    
    self.targa6.delegate = self;
    self.targa6.layer.borderWidth = 1;
    self.targa6.clipsToBounds = YES;
    self.targa6.layer.cornerRadius = 4;
    self.targa6.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa6];
    
    self.targa7.delegate = self;
    self.targa7.layer.borderWidth = 1;
    self.targa7.clipsToBounds = YES;
    self.targa7.layer.cornerRadius = 4;
    self.targa7.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa7];
    
    [self.targa1 becomeFirstResponder];
    
    _dbManager = [[DBManager alloc] initWithDatabaseFilename:@"NuovoDBVeicoli.sql"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_SEARCH_LICENSE_PLATE_MANUAL", @"TITLE_SEARCH_LICENSE_PLATE_MANUAL");
    [APP_DELEGATE sendPageViewGA:@"Search License Plate"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(void)adjustContentSize:(UITextView*)targa{
    CGFloat deadSpace = ([targa bounds].size.height - [targa contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    targa.contentInset = UIEdgeInsetsMake(inset, targa.contentInset.left, inset, targa.contentInset.right);
}

- (IBAction)searchPlate:(id)sender {
    if([self checkPlate]){
        
        [KVNProgress show];
        _manager = [MembershipManager sharedManager];
        [_manager loginKromeda:^(NSString *str) {
            
            _key = str;
            NSString* plate = [self readPlate];
            
            [_manager getPlate:_key plate:plate onSuccess:^(NSArray *arr) {
                if(arr.count > 0) {
                    _car = arr[0];
                    
                    NSString *query = [@"select CodiceArticolo from cars where idVeicolo = '" stringByAppendingString:_car[@"idVeicolo"]];
                    query = [[query stringByAppendingString:@"' and Modello = '"] stringByAppendingString:_car[@"Modello"]];
                    query = [[query stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:_car[@"Anno"]];
                    query = [[query stringByAppendingString:@"' and Versione = '"] stringByAppendingString:_car[@"Versione"]];
                    query = [[query stringByAppendingString:@"' and Alimentazione = '"] stringByAppendingString:_car[@"Alim"]];
                    query = [query stringByAppendingString:@"'"];
                    
                    NSArray *codes = [[NSArray alloc] initWithArray:[_dbManager loadDataFromDB:query]];
                    NSMutableArray* tmp = @[].mutableCopy;
                    for(NSMutableArray* arr in codes){
                        [tmp addObject:arr[0]];
                    }
                    NSArray* codici = [[NSArray alloc] initWithArray:tmp];
                    
                    if(codici.count>0){
                        [_manager searchProductByBRCCode:codici onSuccess:^(NSArray *array) {
                            if(array.count == 0){
                                [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND2", @"")];
                            } else {
                                
                                NSString *query2 = [@"select Valvole from cars where idVeicolo = '" stringByAppendingString:_car[@"idVeicolo"]];
                                query2 = [[query2 stringByAppendingString:@"' and Modello = '"] stringByAppendingString:_car[@"Modello"]];
                                query2 = [[query2 stringByAppendingString:@"' and ModelloAnno = '"] stringByAppendingString:_car[@"Anno"]];
                                query2 = [[query2 stringByAppendingString:@"' and Versione = '"] stringByAppendingString:_car[@"Versione"]];
                                query2 = [[query2 stringByAppendingString:@"' and Alimentazione = '"] stringByAppendingString:_car[@"Alim"]];
                                query2 = [query2 stringByAppendingString:@"'"];
                                
                                NSArray *valvole = [[NSArray alloc] initWithArray:[_dbManager loadDataFromDB:query2]];
                                
                                [KVNProgress dismiss];
                                //NSLog(@"%@",array);
                                _list = [[NSArray alloc] initWithArray:array];
                                ShowProductsVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ShowProductsVC class])];
                                detail.products = _list;
                                NSString* numValvole = [NSString stringWithFormat:@"%@", valvole[0][0]];
                                detail.valvole = numValvole;
                                detail.car = _car;
                                [self.navigationController pushViewController:detail animated:YES];
                            }
                        } onFailure:^(NSError *error) {
                            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
                        }];
                    } else {
                        [KVNProgress showErrorWithStatus:NSLocalizedString(@"PRODUCTS_NOT_FOUND", @"")];
                    }
                } else {
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"RICERCA_AUTO_FALLITA", @"RICERCA_AUTO_FALLITA")];
                }
            } onFailure:^(NSError *error) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
            }];
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        }];
    } else {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"INPUT_TARGA_WRONG", @"INPUT_TARGA_WRONG")];
    }
}

-(void)textViewDidChange:(UITextView *)textView{
    [self goToNextResponder:textView];
}

-(void)goToNextResponder:(UITextView *)textView{
    
    NSString *pattern = @"[a-zA-Z]{1,1}";

    if(textView == self.targa1 && self.targa1.text.length == 1){
        self.targa1.text = [self.targa1.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa1.text options:0 range:NSMakeRange(0,[self.targa1.text length])];
        if(matches > 0){
            [self.targa2 becomeFirstResponder];
        }else{
            self.targa1.text = @"";
        }
    }else if(textView == self.targa2  && self.targa2.text.length == 1){
        self.targa2.text = [self.targa2.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa2.text options:0 range:NSMakeRange(0,[self.targa2.text length])];
        if(matches > 0){
            [self.targa3 becomeFirstResponder];
        }else{
            self.targa2.text = @"";
        }
    }else if(textView == self.targa3  && self.targa3.text.length == 1){
        [self.targa4 becomeFirstResponder];
    }else if(textView == self.targa4  && self.targa4.text.length == 1){
        [self.targa5 becomeFirstResponder];
    }else if(textView == self.targa5  && self.targa5.text.length == 1){
        [self.targa6 becomeFirstResponder];
    }else if(textView == self.targa6  && self.targa6.text.length == 1){
        self.targa6.text = [self.targa6.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa6.text options:0 range:NSMakeRange(0,[self.targa6.text length])];
        if(matches > 0){
            [self.targa7 becomeFirstResponder];
        }else{
            self.targa6.text = @"";
        }
    }else if(textView == self.targa7  && self.targa7.text.length == 1){
        self.targa7.text = [self.targa7.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa7.text options:0 range:NSMakeRange(0,[self.targa7.text length])];
        if(matches > 0){
            
            //[self.targa3 becomeFirstResponder];
        }else{
            self.targa7.text = @"";
        }
    }
}

-(BOOL)checkPlate{
    if([self.targa1.text isEqualToString:@""] || [self.targa2.text isEqualToString:@""] || [self.targa3.text isEqualToString:@""] || [self.targa4.text isEqualToString:@""] || [self.targa5.text isEqualToString:@""] || [self.targa6.text isEqualToString:@""] || [self.targa7.text isEqualToString:@""]){
        return NO;
    } else {
        return YES;
    }
}

-(NSString*)readPlate{
    return [[[[[[self.targa1.text stringByAppendingString:self.targa2.text] stringByAppendingString:self.targa3.text] stringByAppendingString:self.targa4.text] stringByAppendingString:self.targa5.text] stringByAppendingString:self.targa6.text] stringByAppendingString:self.targa7.text];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
