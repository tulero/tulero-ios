//
//  SearchVC.h
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QrVC.h"
#import "QrNVC.h"

@interface SearchVC : UIViewController<UITableViewDelegate, UITableViewDataSource,BarcodeReaderDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)back:(id)sender;


@end
