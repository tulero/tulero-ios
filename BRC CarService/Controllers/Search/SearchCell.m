//
//  SearchCell.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configure:(NSDictionary*) elem {
    self.searchImg.image = [elem objectForKey:@"searchImg"];
    self.searchTitle.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"searchTitle"]];
    self.searchDescription.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"searchDescription"]];
    if([self.searchTitle.text isEqualToString:@"Scansiona Targa"] || [self.searchTitle.text isEqualToString:@"Scansiona EAN"]){
        self.searchTitle.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
    }
}

@end
