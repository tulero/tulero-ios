//
//  OfficineAgenteVC.m
//  BRC CarService
//
//  Created by Etinet on 31/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "OfficineAgenteVC.h"
#import "MembershipManager.h"
#import "Settings.h"
#import "AgenteCell.h"
#import "InventarioVC.h"
#import "ModalVC.h"
#import "YCFirstTime.h"

@interface OfficineAgenteVC ()

@end

@implementation OfficineAgenteVC{
    MembershipManager* _manager;
    NSArray* _list;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    _manager = [MembershipManager sharedManager];

    [self initList];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AgenteCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AgenteCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    [[YCFirstTime shared] executeOnce:^{
        
        ModalVC* model = [[ModalVC alloc] initWithNibName:NSStringFromClass([ModalVC class]) bundle:nil];
        
        [model configureWithTitle:NSLocalizedString(@"LOGIN_MODAL_TITLE", @"LOGIN_MODAL_TITLE") description:NSLocalizedString(@"LOGIN_MODAL_AGENTE_DESCR", @"LOGIN_MODAL_AGENTE_DESCR") andBtnLabel:NSLocalizedString(@"LOGIN_MODAL_BTN", @"LOGIN_MODAL_BTN")];
        
        model.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
        [self presentViewController:model animated:NO completion:nil];

    } forKey:@"MODAL_AGENTE"];
    //-(void)configureWithTitle:(NSString* )title description:(NSString* )description andBtnLabel:(NSString*) btnLbl
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    self.navigationItem.title = NSLocalizedString(@"TITLE_WORKSHOP_AGENT", @"TITLE_WORKSHOP_AGENT");
    [APP_DELEGATE sendPageViewGA:@"Workshops"];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getOfficineAgente:^(NSArray *list) {
        [KVNProgress dismiss];
        _list = [[NSArray alloc] initWithArray:list];
        
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        
    }];
    
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //tipo uno di cella
    AgenteCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AgenteCell class]) forIndexPath:indexPath];
    [cell configure:_list[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
   
    InventarioVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([InventarioVC class])];
    detail.codiceCliente = [[_list[indexPath.row] objectForKey:@"CodCliente"] stringValue];
    detail.rifCliente = [_list[indexPath.row] objectForKey:@"Nome"];
    [self.navigationController pushViewController:detail animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

@end
