//
//  InventarioCell.h
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface InventarioCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *inventoryProduct;
@property (strong, nonatomic) IBOutlet UILabel *inventoryCode;
@property (strong, nonatomic) IBOutlet UILabel *inventoryEAN;
@property (strong, nonatomic) IBOutlet UILabel *inventoryPrice;
@property (strong, nonatomic) IBOutlet UIImageView *inventoryCheck;
@property (strong, nonatomic) IBOutlet UILabel *inventoryQuantity;

-(void)configure:(NSDictionary*)elem;
@end
