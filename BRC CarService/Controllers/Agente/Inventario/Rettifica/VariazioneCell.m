//
//  VariazioneCell.m
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "VariazioneCell.h"

@implementation VariazioneCell{
    int _qntEspositore;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.variationStepper.wraps = true;
    self.variationStepper.autorepeat = true;
    self.variationStepper.maximumValue = 100;
    self.variationStepper.minimumValue = -99;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configure:(int)variazione qnt:(int)quantityExpected{
    self.quantity.text = [NSString stringWithFormat:@"%d",variazione];
    self.btnModifica.clipsToBounds = YES;
    self.btnModifica.layer.cornerRadius = 20;
    self.quantity.layer.borderWidth = 1.0;
    self.quantity.clipsToBounds = YES;
    self.quantity.layer.cornerRadius = 4;
    [self.variationStepper setValue:variazione];
    _qntEspositore = quantityExpected;
}

- (double)getVariation{
    return [self.variationStepper value];
}

- (IBAction)cnt:(UIStepper *)sender {
    int value = (int)[sender value];
    
    if(value + _qntEspositore < 0) {
        [self.variationStepper setValue:value+1];
        self.quantity.text = [NSString stringWithFormat:@"%d", (int)value+1];
        [self.delegate variationUpdated:(int)value+1];
    } else if(value == self.variationStepper.maximumValue){
        [self.variationStepper setValue:value-1];
        self.quantity.text = [NSString stringWithFormat:@"%d", (int)value-1];
        [self.delegate variationUpdated:(int)value-1];
    } else {
        self.quantity.text = [NSString stringWithFormat:@"%d", (int)value];
        [self.delegate variationUpdated:(int)value];
    }
}


@end
