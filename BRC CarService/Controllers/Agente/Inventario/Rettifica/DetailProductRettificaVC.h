//
//  DetailProductRettificaVC.h
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VariazioneCell.h"

@protocol DettaglioRettificaDelegate;

@interface DetailProductRettificaVC : UIViewController<UITableViewDelegate, UITableViewDataSource,VariazioneCellDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSDictionary* product;
@property int variation;
@property int index;
@property NSArray* update;
@property (strong, nonatomic) id<DettaglioRettificaDelegate> delegate;

@end

@protocol DettaglioRettificaDelegate

-(void)variazioneChanged:(int)variazione;

@end
