//
//  RettificaCell.m
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "RettificaCell.h"
#import "Settings.h"

@implementation RettificaCell{
    int _variazione;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem{
    self.elemSelected = elem.mutableCopy;
    
    self.rettificaName.text = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Descrizione"] orPlaceholder:@"N.D."]];
    self.rettificaCode.text = [NSString stringWithFormat: NSLocalizedString(@"INVENTORY_PRODUCT_CODE", @"INVENTORY_PRODUCT_CODE"), [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.rettificaEAN.text = [NSString stringWithFormat:NSLocalizedString(@"INVENTORY_PRODUCT_EAN", @"INVENTORY_PRODUCT_EAN"), [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Image"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.image sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    
    if(elem[@"Conta"]){
        int var = [elem[@"Conta"] intValue] - [elem[@"QtaEspositore"] intValue];
        if(var>0){
            self.rettificaVariazione.text = [NSString stringWithFormat:@"%@ +%d", NSLocalizedString(@"VARIAZIONE_RETTIFICA", @"VARIAZIONE_RETTIFICA"),var];
        } else {
            self.rettificaVariazione.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"VARIAZIONE_RETTIFICA", @"VARIAZIONE_RETTIFICA"),var];
        }
        _variazione = var;
    } else {
        self.rettificaVariazione.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"VARIAZIONE_RETTIFICA", @"VARIAZIONE_RETTIFICA"),0];
        _variazione = 0;
    }
}

-(int)getVariation{
    return _variazione;
}

#pragma delegate

-(void)variazioneChanged:(int)variazione{
    if(variazione > 0){
        self.rettificaVariazione.text = [NSString stringWithFormat:@"%@ +%d", NSLocalizedString(@"VARIAZIONE_RETTIFICA", @"VARIAZIONE_RETTIFICA"),variazione];
    }else{
        self.rettificaVariazione.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"VARIAZIONE_RETTIFICA", @"VARIAZIONE_RETTIFICA"), variazione];
    }
    _variazione = variazione;
    
    self.elemSelected[@"Conta"] = @([self.elemSelected[@"QtaEspositore"] intValue] + _variazione);
    
    [self.delegate onChangeConta:self.elemSelected];
}

@end
