//
//  VariazioneCell.h
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VariazioneCellDelegate;

@interface VariazioneCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextView *quantity;
@property (strong, nonatomic) IBOutlet UIButton *btnModifica;
@property (strong, nonatomic) IBOutlet UIStepper *variationStepper;
@property (strong, nonatomic) id<VariazioneCellDelegate> delegate;

- (void)configure:(int)variazione qnt:(int)quantityExpected;
- (double)getVariation;

- (IBAction)cnt:(UIStepper *)sender;

@end

@protocol VariazioneCellDelegate

-(void)variationUpdated:(int)variation;

@end
