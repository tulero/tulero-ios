//
//  EsitoRettificaVC.m
//  BRC CarService
//
//  Created by Etinet on 12/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EsitoRettificaVC.h"
#import "OfficineAgenteVC.h"
#import "CarrelloVC.h"
#import "Settings.h"

@interface EsitoRettificaVC ()

@end

@implementation EsitoRettificaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.btnClose.clipsToBounds = YES;
    self.btnClose.layer.cornerRadius = 22;
   
    if([self.navController.topViewController isKindOfClass:[CarrelloVC class]]){
        self.backLabel.text = NSLocalizedString(@"ORDINE_INVIATO", @"ORDINE_INVIATO");
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.backLabel.attributedText];
        [attrStr addAttribute:  NSFontAttributeName
                        value:[UIFont systemFontOfSize:16.0f weight:UIFontWeightBold]
                        range:[[attrStr string] rangeOfString:NSLocalizedString(@"ORDINI", @"ORDINI")]];
        
        self.backLabel.attributedText = attrStr;
    } else {
        self.backLabel.text = NSLocalizedString(@"RETTIFICA_INVIATA", @"RETTIFICA_INVIATA");
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.backLabel.attributedText];
        [attrStr addAttribute:  NSFontAttributeName
                                value:[UIFont systemFontOfSize:16.0f weight:UIFontWeightBold]
                                range:[[attrStr string] rangeOfString:NSLocalizedString(@"OFFICINE", @"OFFICINE")]];
        
        self.backLabel.attributedText = attrStr;
    }
    
    //[APP_DELEGATE sendPageViewGA:@"Outcome Correction"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {

    [self.navController popToRootViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:YES completion:^{
        //[self.navController popViewControllerAnimated:YES];
    }];
    
}
@end
