//
//  DetailProductRettificaVC.m
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailProductRettificaVC.h"
#import "DetailCell.h"
#import "infoCell.h"
#import "VariazioneCell.h"
#import "RettificaVC.h"
#import "Settings.h"

@interface DetailProductRettificaVC ()

@end

@implementation DetailProductRettificaVC

@synthesize product;
@synthesize variation;
@synthesize index;
@synthesize update;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([VariazioneCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([VariazioneCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 474;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"DETAIL_PRODUCT", @"DETAIL_PRODUCT");
    [APP_DELEGATE sendPageViewGA:[NSString stringWithFormat:@"Detail Product Correction: %@", product[@"CodArticolo"]]];
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 6;
    } else if(section == 2){
        return 3;
    } else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configureInventory:product];
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureInventory: product index:indexPath.row];
        return cell;
    } else if(indexPath.section == 2){
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureQuantityInventory:product index:indexPath.row variation:variation];
        return cell;
    } else {
        VariazioneCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VariazioneCell class]) forIndexPath:indexPath];
        [cell configure:variation qnt:[[product objectForKey:@"QtaEspositore"] intValue]];
        [cell.btnModifica addTarget:self action:@selector(onModificaTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.delegate = self;
        return cell;
    }
    
}

-(IBAction)onModificaTapped:(id)sender{
    
    [self.delegate variazioneChanged:self.variation];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1 || indexPath.section == 2) {
        return 44;
    } else {
        return 158;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 2)
        return 12;
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,tableView.bounds.size.width, 30)];
    if (section == 2)
        [headerView setBackgroundColor:[UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f]];
    else
        [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}

#pragma delegate

-(void)variationUpdated:(int)variation{
    self.variation = variation;
    [self.tableView reloadData];
    
}

@end
