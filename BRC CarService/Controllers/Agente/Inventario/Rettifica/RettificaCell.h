//
//  RettificaCell.h
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailProductRettificaVC.h"

@protocol RettificaCellDelegate;

@interface RettificaCell : UITableViewCell<DettaglioRettificaDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *rettificaName;
@property (strong, nonatomic) IBOutlet UILabel *rettificaCode;
@property (strong, nonatomic) IBOutlet UILabel *rettificaEAN;
@property (strong, nonatomic) IBOutlet UILabel *rettificaVariazione;
@property (strong, nonatomic) NSMutableDictionary *elemSelected;

@property (strong, nonatomic) id<RettificaCellDelegate> delegate;

-(void)configure:(NSDictionary*)elem;
-(int)getVariation;
@end

@protocol RettificaCellDelegate

-(void)onChangeConta:(NSDictionary*)elem;

@end
