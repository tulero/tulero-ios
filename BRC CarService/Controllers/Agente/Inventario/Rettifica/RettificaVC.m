//
//  RettificaVC.m
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "RettificaVC.h"
#import "Settings.h"
#import "DetailProductRettificaVC.h"
#import "MembershipManager.h"
#import "EsitoRettificaVC.h"

@interface RettificaVC ()

@end

@implementation RettificaVC{
    MembershipManager* _manager;
    NSString* _str;
}

@synthesize inventario;
@synthesize codiceCliente;
@synthesize rifCliente;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RettificaCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RettificaCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.btnconferma.clipsToBounds = YES;
    self.btnconferma.layer.cornerRadius = 18;
    
    _manager = [MembershipManager sharedManager];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"RETTIFICA_TITLE", @"RETTIFICA_TITLE");
    [APP_DELEGATE sendPageViewGA:@"Corrections"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

- (IBAction)confermaRettifica:(id)sender {
    [KVNProgress show];

    NSMutableArray* codes = @[].mutableCopy;
    
    for(NSDictionary* elem in self.inventario){
    
        if(elem[@"Conta"]){
            [codes addObject:@{
                               @"codArticolo":elem[@"CodArticolo"],
                               @"qtaprevista":elem[@"QtaEspositore"],
                               @"qtaconta": elem[@"Conta"]
                               }];
        }
        
    }
    
    [_manager confirmCorrection:codes codCliente:codiceCliente rifCliente:rifCliente onSuccess:^(NSString *list) {
        
        _str = [NSString stringWithFormat:@"%@",list];
        if([_str isEqualToString:ESITO_RETTIFICA]){
            [KVNProgress dismiss];
            EsitoRettificaVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EsitoRettificaVC"];
            controller.navController = self.navigationController;
            [self presentViewController:controller animated:YES completion:nil];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        }
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return inventario.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //tipo uno di cella
    RettificaCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RettificaCell class]) forIndexPath:indexPath];
    cell.delegate = self;
    [cell configure:inventario[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    DetailProductRettificaVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductRettificaVC class])];
    detail.product = inventario[indexPath.row];
    RettificaCell* cell = (RettificaCell* )[tableView cellForRowAtIndexPath:indexPath];
    detail.variation = [cell getVariation];
    detail.delegate = cell;
    
    [self.navigationController pushViewController:detail animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}


#pragma
-(void)onChangeConta:(NSDictionary *)elem{
    
    NSMutableArray* _newlist = @[].mutableCopy;
    
    for(NSDictionary* e in self.inventario){
        if( e[@"CodBreve"] && [e[@"CodBreve"] intValue] == [elem[@"CodBreve"] intValue] ){
            [_newlist addObject:elem];
        }else{
            [_newlist addObject:e];
        }
    }
    
    self.inventario = [[NSMutableArray alloc] initWithArray:_newlist];
}
@end
