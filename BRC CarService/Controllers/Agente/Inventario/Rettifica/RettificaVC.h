//
//  RettificaVC.h
//  BRC CarService
//
//  Created by Etinet on 08/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RettificaCell.h"

@interface RettificaVC : UIViewController<UITableViewDelegate, UITableViewDataSource, RettificaCellDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnconferma;

@property NSMutableArray* inventario;
@property NSString* codiceCliente;
@property NSString* rifCliente;

- (IBAction)confermaRettifica:(id)sender;

@end
