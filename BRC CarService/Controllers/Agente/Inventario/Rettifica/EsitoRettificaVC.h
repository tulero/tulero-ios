//
//  EsitoRettificaVC.h
//  BRC CarService
//
//  Created by Etinet on 12/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EsitoRettificaVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *backLabel;
@property (strong, nonatomic) IBOutlet UILabel *esitoLabel;
@property (strong, nonatomic) IBOutlet UILabel *thanksLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) UINavigationController *navController;

- (IBAction)close:(id)sender;
@end
