//
//  InventarioCell.m
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "InventarioCell.h"
#import "Settings.h"

@implementation InventarioCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem{ //variation:(int)var{
    self.inventoryProduct.text = [Utils Object:[elem objectForKey:@"Descrizione"] orPlaceholder:@"N.D."];
    self.inventoryCode.text = [NSString stringWithFormat:NSLocalizedString(@"INVENTORY_PRODUCT_CODE", @"INVENTORY_PRODUCT_CODE"), [Utils Object:[elem objectForKey:@"CodArticolo"] orPlaceholder:@"N.D."]];
    self.inventoryEAN.text = [NSString stringWithFormat:NSLocalizedString(@"INVENTORY_PRODUCT_EAN", @"INVENTORY_PRODUCT_EAN"), [Utils Object:[elem objectForKey:@"EAN"] orPlaceholder:@"N.D."]];
    NSString* price = [NSString stringWithFormat:@"%@", [Utils Object:[elem objectForKey:@"Prezzo"] orPlaceholder:@"N.D."]];
    if([price isEqualToString:@"N.D."]){
        self.inventoryPrice.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_ORDERED_PRICE_2", @"QUANTITY_ORDERED_PRICE_2"), price];
    } else {
        self.inventoryPrice.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_ORDERED_PRICE", @"QUANTITY_ORDERED_PRICE"),[price floatValue]];
    }
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Image"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.image sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder_products_list"]];
    if(elem[@"Conta"]){
        if(([elem[@"Conta"] intValue] - [elem[@"QtaEspositore"] intValue]) == [elem[@"QtaEspositore"] intValue]){
            self.inventoryCheck.image = [UIImage imageNamed:@"check_green_icon"];
            self.inventoryQuantity.text = [NSString stringWithFormat:@"%dx", [elem[@"Conta"] intValue] - [elem[@"QtaEspositore"] intValue]];
            self.inventoryQuantity.textColor = [UIColor colorWithRed:103.0f/255.0f green:197.0f/255.0f blue:52.0f/255.0f alpha:1];
            self.inventoryQuantity.hidden = NO;
        } else {
            self.inventoryCheck.image = [UIImage imageNamed:@"check_red_icon"];
            self.inventoryQuantity.text = [NSString stringWithFormat:@"%dx", [elem[@"Conta"] intValue] - [elem[@"QtaEspositore"] intValue]];
            self.inventoryQuantity.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1];
            self.inventoryQuantity.hidden = NO;
        }
    } else {
        self.inventoryCheck.image = [UIImage imageNamed:@"check_grey_icon"];
        self.inventoryQuantity.hidden = YES;
    }
}

@end
