//
//  InventarioVC.h
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QrVC.h"
#import "QrNVC.h"

@interface InventarioVC : UIViewController<UITableViewDelegate, UITableViewDataSource, BarcodeReaderDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;

@property NSString* codiceCliente;
@property NSString* rifCliente;

- (IBAction)confermaInventario:(id)sender;

@end
