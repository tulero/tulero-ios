//
//  InventarioVC.m
//  BRC CarService
//
//  Created by Etinet on 06/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "InventarioVC.h"
#import "MembershipManager.h"
#import "Settings.h"
#import "InventarioCell.h"
#import "RettificaVC.h"


@interface InventarioVC ()

@end

@implementation InventarioVC{
    MembershipManager* _manager;
    NSArray* _list;
    NSMutableArray* _copyList;
}

@synthesize codiceCliente;
@synthesize rifCliente;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    _manager = [MembershipManager sharedManager];
    
    [self initList];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InventarioCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InventarioCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = 18;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"INVENTORY_TITLE", @"INVENTORY_TITLE");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAdd target:self action:@selector(addProduct:)];
    [APP_DELEGATE sendPageViewGA:@"Inventory"];
}

-(void) viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(IBAction)addProduct:(id)sender{
    /*
    NSLog(@"Registra un nuovo prodotto nell'inventario!");
    if(_copyList[0][@"Conta"]){
        _copyList[0][@"Conta"] = @([_copyList[0][@"Conta"] intValue] + 1);
    } else {
        _copyList[0][@"Conta"] = @([_copyList[0][@"QtaEspositore"] intValue] + 1);
    }
    [self.tableView reloadData];
     */
    QrVC* qrViewController = [APP_DELEGATE.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([QrVC class])];
    
    qrViewController.delegate = self;
    qrViewController.productList = _copyList;
    
    QrNVC* qrNVC = [[QrNVC alloc] initWithRootViewController:qrViewController];
    qrNVC.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:qrNVC animated:YES completion:nil];
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getInventory: codiceCliente onSuccess:^(NSArray *list) {
        [KVNProgress dismiss];
        _list = [[NSArray alloc] initWithArray:list];
        _copyList = _list.mutableCopy;
//        int i = 0;
//        for(NSDictionary* d in _copyList){
//            _copyList[i][@"Conta"] = @(0);
//            i++;
//        }
        int i = 0;
        for(NSDictionary* d in _list){
            NSMutableDictionary* mut = d.mutableCopy;
            [_copyList replaceObjectAtIndex:i withObject:mut];
            _copyList[i][@"Check"] = @(0);
            _copyList[i][@"Variation"] = @(0);
//            if(!_copyList[i][@"Conta"]){
//               _copyList[i][@"Conta"] = @([_copyList[i][@"QtaEspositore"] intValue]);
//            }
            i = i+1;
        }
        
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
    
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //tipo uno di cella
    InventarioCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InventarioCell class]) forIndexPath:indexPath];
    [cell configure:_copyList[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}

- (IBAction)confermaInventario:(id)sender {
    RettificaVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([RettificaVC class])];
    detail.inventario = _copyList;
    detail.codiceCliente = codiceCliente;
    detail.rifCliente = rifCliente;
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma qrcode delegate
-(void)productScanned:(NSMutableArray *)productsScanned{
    int i = 0;
    for(NSDictionary* product in productsScanned){
        i = 0;
        for(NSDictionary* d in _copyList){
            if([product[@"EAN"]  isEqualToString:d[@"EAN"] ]){
                if(_copyList[i][@"Conta"]){
                    _copyList[i][@"Conta"] = @([_copyList[i][@"Conta"] intValue] + 1);
                    _copyList[i][@"Check"] = @(1);
                    _copyList[i][@"Variation"] = @([_copyList[i][@"Conta"] intValue] - [_copyList[i][@"QtaEspositore"] intValue]);
                } else {
                    _copyList[i][@"Conta"] = @([_copyList[i][@"QtaEspositore"] intValue] + 1);
                    _copyList[i][@"Check"] = @(1);
                    _copyList[i][@"Variation"] = @([_copyList[i][@"Conta"] intValue] - [_copyList[i][@"QtaEspositore"] intValue]);
                }
            }
            i++;
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Check" ascending:NO];
    NSSortDescriptor *contaDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Variation" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor, contaDescriptor];
    NSArray *ordered = [_copyList sortedArrayUsingDescriptors:sortDescriptors];
    [_copyList removeAllObjects];
    [_copyList addObjectsFromArray:ordered];
    [self.tableView reloadData];
    
}

@end
