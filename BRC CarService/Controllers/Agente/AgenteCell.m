//
//  AgenteCell.m
//  BRC CarService
//
//  Created by Etinet on 31/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "AgenteCell.h"
#import "Settings.h"

@implementation AgenteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.officinaName.text = [NSString stringWithFormat:@"%@",[Utils Object: [elem objectForKey:@"Nome"] orPlaceholder:@"N.D."]];
    self.officinaAddress.text = [NSString stringWithFormat:@"%@,%@ - %@, %@, %@", [Utils Object:[elem objectForKey:@"Indirizzo"] orPlaceholder:@"-"],[Utils Object:[elem objectForKey:@"NCivico"] orPlaceholder:@"-"],[Utils Object:[elem objectForKey: @"Citta"] orPlaceholder:@"-"],[Utils Object:[elem objectForKey:@"CAP"] orPlaceholder:@"-"], [Utils Object:[elem objectForKey:@"Provincia"] orPlaceholder:@"-"]];
}

@end
