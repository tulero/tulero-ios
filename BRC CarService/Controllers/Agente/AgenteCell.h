//
//  AgenteCell.h
//  BRC CarService
//
//  Created by Etinet on 31/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgenteCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *officinaName;
@property (strong, nonatomic) IBOutlet UILabel *officinaAddress;
@property (strong, nonatomic) IBOutlet UILabel *officinaContact;

-(void)configure:(NSDictionary*)elem;

@end
