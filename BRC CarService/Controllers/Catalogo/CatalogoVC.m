//
//  CatalogoVC.m
//  BRC CarService
//
//  Created by Etinet on 28/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "CatalogoVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "CatalogoCell.h"
#import "EspositoreCell.h"
#import "DetailCatalogoVC.h"
#import "ModalVC.h"
#import "YCFirstTime.h"
#import "PromoDetailVC.h"

@interface CatalogoVC ()

@end

@implementation CatalogoVC{
    MembershipManager* _manager;
    NSArray* _list;
    NSArray* _listPromotions;
    NSDictionary* _promo;
    //BOOL _isLoaded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CatalogoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CatalogoCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EspositoreCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EspositoreCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _manager = [MembershipManager sharedManager];
    //_manager.actualUser.mail

    //_isLoaded = NO;
    
    [self initList];
    
    [[YCFirstTime shared] executeOnce:^{
        
        ModalVC* model = [[ModalVC alloc] initWithNibName:NSStringFromClass([ModalVC class]) bundle:nil];
        
        [model configureWithTitle:NSLocalizedString(@"LOGIN_MODAL_TITLE", @"LOGIN_MODAL_TITLE") description:NSLocalizedString(@"LOGIN_MODAL_DESCR", @"LOGIN_MODAL_DESCR") andBtnLabel:NSLocalizedString(@"LOGIN_MODAL_BTN", @"LOGIN_MODAL_BTN")];
        
        //model.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        model.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
        [self presentViewController:model animated:NO completion:nil];
        
    } forKey:@"MODAL_OFFICINA"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_CATALOG", @"TITLE_CATALOG");
    [APP_DELEGATE sendPageViewGA:@"Catalog"];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getCategoryListCatalogo:^(NSArray *list) {
        [KVNProgress dismiss];
        //_isLoaded = YES;
        _list = [[NSArray alloc] initWithArray:list];
        
        [KVNProgress show];
        
        [_manager getPromotions:^(NSArray *list) {
            [KVNProgress dismiss];
            //_listPromotions = [[NSArray alloc] initWithArray:list];
            for(NSDictionary* d in list){
                if(d[@"Evidenza"] && [d[@"Evidenza"] boolValue] == true){
                    _promo = [[NSDictionary alloc] initWithDictionary:d];
                }
            }
            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
            [self.tableView reloadData];
        } onFailure:^(NSError *error) {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        }];

        
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        //_isLoaded = YES;
        
    }];
    
}


-(NSArray*)filterList:(NSArray*)_list{
    
    NSPredicate *predicateToShow = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        
        if( evaluatedObject[@"Commodity"] &&
           [evaluatedObject[@"Commodity"] isEqualToString:@"<COMMODITY>"] &&
           [evaluatedObject[@"IsEspositore"] intValue] == 1 // se arrivo da catalogo non faccio questo controllo, se arrivo da espositore si
           ){
            return YES;
        }
        return NO;
    }];
    
    return [_list filteredArrayUsingPredicate:predicateToShow];
}

#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0)
        return 1;
    else
        return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        /**
            DA CONTROLLARE!!!!
         **/
//        NSDictionary* banner = @{
//                                 @"bannerImg" : [UIImage imageNamed:@"logo_login"],
//                                 @"bannerTitle" : @"PAZZESCO! 50+40% di sconto!",
//                                 @"bannerDescription" : @"Sulle pastiglie freni elencate in tabella. Combo Box Stahlwille in OMAGGIO!",
//                                 @"bannerTime" : @"Scade tra 5 giorni! AFFRETTATEVI!"
//                                 };
        
        CatalogoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CatalogoCell class]) forIndexPath:indexPath];
        [cell configure:_promo];
        return cell;
    } else {
        EspositoreCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EspositoreCell class]) forIndexPath:indexPath];
        [cell configureForCatalogoCategory:_list[indexPath.row]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 0) {

        PromoDetailVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([PromoDetailVC class])];

        detail.promo = _promo;
        [self.navigationController pushViewController:detail animated:YES];
    } else if(indexPath.section == 1){
        //newList[indexPath.row]
        DetailCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailCatalogoVC class])];
        
        detail.commodity = [_list[indexPath.row] objectForKey:@"Commodity"];
        detail.productCategory = [_list[indexPath.row] objectForKey:@"Descrizione"];
        
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        return 142;
    } else {
        return 85;
    }
}

//#pragma mark DZNEmptyDataSetSource
//
//- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
//{
//    return [UIImage imageNamed:@"carrello_placeholder_grey_icon"];
//}
//
//- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
//{
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
//
//    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
//
//    animation.duration = 0.25;
//    animation.cumulative = YES;
//    animation.repeatCount = MAXFLOAT;
//
//    return animation;
//}
//
//- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
//{
//    return 0;
//}
//- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
//
//    NSString *text = NSLocalizedString(@"EMPTY_CART", @"EMPTY_CART");
//
//    NSDictionary *attributes = @{
//                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
//                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
//
//    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
//
//}
//
//- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
//{
//    return self.tableView.backgroundColor;
//}
//
//- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
//{
//    NSString *text = NSLocalizedString(@"EMPTY_CART_DESCR", @"EMPTY_CART_DESCR");
//
//    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
//    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
//    paragraph.alignment = NSTextAlignmentCenter;
//
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
//                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
//                                 NSParagraphStyleAttributeName: paragraph};
//
//    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
//}
//
//- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
//{
//    return YES;
//}
//
//-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
//    return _isLoaded;
//}

@end
