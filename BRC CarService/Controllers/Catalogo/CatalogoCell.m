//
//  CatalogoCell.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "CatalogoCell.h"
#import "Settings.h"

@implementation CatalogoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.promoImg.image = [UIImage imageNamed:@"logo_login"];
    self.promoTitle.text = [NSString stringWithFormat:@"%@", [Utils Object:elem[@"Titolo"] orPlaceholder:@"N.D."]];
    self.promoDescription.text = [NSString stringWithFormat:@"%@", [Utils Object:elem[@"Testo"] orPlaceholder:@"N.D."]];
//    self.promoDescription.frame = CGRectMake(92, 57, 233, 34);
//    [self.promoDescription sizeToFit];
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
//    NSInteger day = [components day];
    NSString* dataValidita = [Utils formatISODateOrders:[elem objectForKey:@"DataValidita"] toFormat:@"dd/MM/yyyy"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDate* dateReal = [formatter dateFromString:dataValidita];
//    components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dateReal];
//    NSInteger dayPromo = [components day];
    double scadenza = [dateReal timeIntervalSinceNow]/86400;
    //long giorniRimanenti = dayPromo - day;
    self.promoTime.text = [NSString stringWithFormat:NSLocalizedString(@"TEMPO_PROMO", @"TEMPO_PROMO"),scadenza];
    
    self.btnPromo.clipsToBounds = YES;
    self.btnPromo.layer.cornerRadius = 8;
    
    self.promoImg.clipsToBounds = YES;
    self.promoImg.layer.cornerRadius = 27.5;
    
    UIImage *image = [UIImage imageNamed:@"info_black_icon"];
    self.infoImg.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.infoImg setTintColor:[UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:140.0f/255.0f alpha:1.0f]];

}

@end
