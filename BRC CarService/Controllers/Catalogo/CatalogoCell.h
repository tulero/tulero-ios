//
//  CatalogoCell.h
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *promoImg;
@property (strong, nonatomic) IBOutlet UILabel *promoTitle;
@property (strong, nonatomic) IBOutlet UILabel *promoDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnPromo;
@property (strong, nonatomic) IBOutlet UIImageView *infoImg;
@property (strong, nonatomic) IBOutlet UILabel *promoTime;

-(void)configure:(NSDictionary*)elem;

@end
