//
//  DetailProductCatalogo.h
//  BRC CarService
//
//  Created by Etinet on 04/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PriceCell.h"
#import "DetailCell.h"

@interface DetailProductCatalogoVC : UIViewController<UITableViewDelegate, UITableViewDataSource,PriceCellDelegate,AddProductDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSDictionary* product;
@property NSString* productCategory;

@end
