//
//  DetailProductCatalogo.m
//  BRC CarService
//
//  Created by Etinet on 04/06/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailProductCatalogoVC.h"
#import "DetailCell.h"
#import "infoCell.h"
#import "PriceCell.h"
#import "MembershipManager.h"
#import "Settings.h"

@interface DetailProductCatalogoVC ()

@end

@implementation DetailProductCatalogoVC{
    MembershipManager* _manager;
    NSString* _str;
}

@synthesize productCategory;
@synthesize product;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PriceCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PriceCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 474;
    
    _manager = [MembershipManager sharedManager];
    
    if([[product objectForKey:@"Preferito"] intValue]==0){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_outline_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(onPreferito:)];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_full_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(onRimuoviPreferito:)];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
    }
    
    [APP_DELEGATE sendPageViewGA:[NSString stringWithFormat:@"Detail Product: %@", product[@"CodArticolo"]]];
    
}

-(IBAction)onPreferito:(id)sender{
    
    _str = @"";
    [KVNProgress show];
    NSString* articolo = [NSString stringWithFormat:@"%@",[Utils ObjectOrEmptyString:[product objectForKey:@"CodArticolo"]]];
    [_manager addFavorite:articolo onSuccess: ^(NSString *list) {
        _str = [[NSString alloc] initWithFormat:@"%@",list];
        if([_str isEqualToString:@"Operation complete"]){
            [KVNProgress showSuccessWithStatus:NSLocalizedString(@"AGGIUNTO_PREFERITO", @"AGGIUNTO_PREFERITO")];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_full_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(onRimuoviPreferito:)];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
        } else {
            NSLog(@"NON AGGIUNTO");
        }
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        
    }];
    
    
}

-(IBAction)onRimuoviPreferito:(id)sender{
    _str = @"";
    
    [KVNProgress show];
    NSString* articolo = [NSString stringWithFormat:@"%@",[Utils ObjectOrEmptyString:[product objectForKey:@"CodArticolo"]]];
    [_manager deleteFavorite:articolo onSuccess: ^(NSString *list) {
        [KVNProgress dismiss];
        _str = [[NSString alloc] initWithFormat:@"%@",list];
        if([_str isEqualToString:@"Operation complete"]){
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_outline_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(onPreferito:)];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
        } else {
            NSLog(@"NON ELIMINATO");
        }
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"DETAIL_PRODUCT", @"DETAIL_PRODUCT");
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 6;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configureCatalogo:product];
        cell.delegate = self;
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configureCatalogo: product index:indexPath.row category:productCategory];
        return cell;
    } else {
        PriceCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PriceCell class]) forIndexPath:indexPath];
        [cell configure: product];
        cell.delegate = self;
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1) {
        return 44;
    } else {
        return 158;
    }
}

#pragma delegate

-(void)priceUpdated:(double)quantity{
    //NSLog(@"RICARICO TABELLAAAAAAAA!!!!!!!!!");
    
    NSMutableDictionary * _mut = product.mutableCopy;
    [_mut setObject:@(quantity) forKey:@"QtaOrder"];
    product = [[NSDictionary alloc] initWithDictionary:_mut];
    [self.tableView reloadData];
    
}

#pragma addProduct delegate
-(void)addProduct:(NSDictionary *)prod{
    [_manager.cart addProduct:prod];
}

@end
