//
//  DetailCatalogoVC.m
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailCatalogoVC.h"
#import "ProductCell.h"
#import "DetailProductCatalogoVC.h"
#import "MembershipManager.h"
#import "Settings.h"

@interface DetailCatalogoVC ()

@end

@implementation DetailCatalogoVC {
    MembershipManager* _manager;
    NSArray* _list;
    BOOL _isLoaded;
}

@synthesize commodity;
@synthesize productCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _manager = [MembershipManager sharedManager];
    _isLoaded = NO;
    
    [self initList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = [productCategory uppercaseString];
    
    [APP_DELEGATE sendPageViewGA:@"Detail Catalog"];
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getProducts:^(NSArray *list) {
        [KVNProgress dismiss];
        _list = [[NSArray alloc] initWithArray:list];
        _list = [self filterList:_list];
        _isLoaded = YES;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        _isLoaded = YES;
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
    }];
    
}

-(NSArray*)filterList:(NSArray*)_list{
    
    NSPredicate *predicateToShow = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        //@"<COMMODITY>"
        if( evaluatedObject[@"Commodity"] &&
           [evaluatedObject[@"Commodity"] isEqualToString:commodity] //&&
           //[evaluatedObject[@"IsEspositore"] intValue] == 1 // se arrivo da catalogo non faccio questo controllo, se arrivo da espositore si
           ){
            return YES;
        }
        return NO;
    }];
    
    return [_list filteredArrayUsingPredicate:predicateToShow];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
    [cell configureCatalogo:_list[indexPath.row]];
    //cell.delegateAdd = self;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    DetailProductCatalogoVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProductCatalogoVC class])];
        
    detail.product = _list[indexPath.row];
    detail.productCategory = productCategory;
    [self.navigationController pushViewController:detail animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}

#pragma mark DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"itemslist_placeholder_grey_icon"];
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = NSLocalizedString(@"EMPTY_CATEGORY_LIST", @"EMPTY_CATEGORY_LIST");
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return self.tableView.backgroundColor;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"EMPTY_CATEGORY_LIST_DESCRIPTION", @"EMPTY_CATEGORY_LIST_DESCRIPTION");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
{
    return YES;
}

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return _isLoaded;
}


@end
