//
//  DetailCatalogoVC.h
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCell.h"
#import "UIScrollView+EmptyDataSet.h"

@interface DetailCatalogoVC : UIViewController<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSString* commodity;
@property NSString* productCategory;

@end
