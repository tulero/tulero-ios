//
//  PromoImgCell.m
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromoImgCell.h"
#import "Settings.h"

@implementation PromoImgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configure:(NSDictionary*)elem {
    NSString *path = [Utils ObjectOrEmptyString:[elem objectForKey:@"Immagine"]];
    path = [SERVICE_IMAGE_URL stringByAppendingString:path];
    [self.promoImg sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"edit_grey_icon"]];
//    if([img objectForKey:@"Immagine"])
//        self.promoImg.image = [UIImage imageNamed: [img objectForKey:@"Immagine"]];
}

@end
