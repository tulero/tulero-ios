//
//  PromoDetailVC.m
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromoDetailVC.h"
#import "PromoImgCell.h"
#import "PromoDescriptionCell.h"
#import "infoCell.h"
#import "Settings.h"

@interface PromoDetailVC ()

@end

@implementation PromoDetailVC

@synthesize promo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PromoImgCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PromoImgCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PromoDescriptionCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PromoDescriptionCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = NSLocalizedString(@"PROMO_DETAIL_TITLE", @"PROMO_DETAIL_TITLE");
    [APP_DELEGATE sendPageViewGA:@"Detail Promotion"];
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if([promo objectForKey:@"Immagine"]){
            return 1;
        }
        
        return 0;
    }
    
    if(section == 2) {
        return 3;
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        PromoImgCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PromoImgCell class]) forIndexPath:indexPath];
        [cell configure: promo];
        return cell;
    } else if(indexPath.section == 1){
        PromoDescriptionCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PromoDescriptionCell class]) forIndexPath:indexPath];
        [cell configure: promo];
        return cell;
    } else {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configurePromo: promo index:indexPath.row];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 2 && indexPath.row == 1) {
        if(promo[@"Allegato2"] && ![promo[@"Allegato2"] isEqualToString:@"NULL"]){
            NSString* link = promo[@"Allegato2"];
            if([link hasPrefix:@"/"]){
                UIApplication *application = [UIApplication sharedApplication];
                link = [link stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
                link = [SERVICE_BRC_URL stringByAppendingString:link];
               
                NSURL *URL = [NSURL URLWithString: link];
                [application openURL:URL options:@{} completionHandler:nil];
            } else {
                UIApplication *application = [UIApplication sharedApplication];
                link = [link stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
                link = [SERVICE_BRC_URL2 stringByAppendingString:link];
               
                NSURL *URL = [NSURL URLWithString: link];
                [application openURL:URL options:@{} completionHandler:nil];
            }
        } else {
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:SERVICE_BRC_URL];
            [application openURL:URL options:@{} completionHandler:nil];
        }
    } else if(indexPath.section == 2 && indexPath.row == 2) {
        if(promo[@"Allegato1"] && ![promo[@"Allegato1"] isEqualToString:@"NULL"]){
            NSString* link = promo[@"Allegato1"];
            if([link hasPrefix:@"/"]){
                UIApplication *application = [UIApplication sharedApplication];
                link = [link stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
                link = [SERVICE_BRC_URL stringByAppendingString:link];
              
                NSURL *URL = [NSURL URLWithString: link];
                [application openURL:URL options:@{} completionHandler:nil];
            } else {
                UIApplication *application = [UIApplication sharedApplication];
                link = [link stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
                link = [SERVICE_BRC_URL2 stringByAppendingString:link];
                
                NSURL *URL = [NSURL URLWithString: link];
                [application openURL:URL options:@{} completionHandler:nil];
            }
        } else {
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:SERVICE_BRC_URL];
            [application openURL:URL options:@{} completionHandler:nil];
        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if([promo objectForKey:@"Immagine"]){
           return 375;
        } else {
            return 0;
        }
    } else if(indexPath.section == 1){
        return UITableViewAutomaticDimension;
    } else {
        return 44;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
