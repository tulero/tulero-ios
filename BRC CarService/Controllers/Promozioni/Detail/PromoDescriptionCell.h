//
//  PromoDescriptionCell.h
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoDescriptionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *promoOrg;
@property (strong, nonatomic) IBOutlet UILabel *promoTitle;
@property (strong, nonatomic) IBOutlet UILabel *promoDescription;
@property (strong, nonatomic) IBOutlet UILabel *promoTime;

-(void) configure:(NSDictionary*)elem;

@end
