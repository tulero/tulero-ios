//
//  PromoImgCell.h
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface PromoImgCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *promoImg;

- (void) configure:(NSDictionary*)elem;

@end
