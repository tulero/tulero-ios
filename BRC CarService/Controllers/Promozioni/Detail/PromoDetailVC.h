//
//  PromoDetailVC.h
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoDetailVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property NSDictionary* promo;

@end
