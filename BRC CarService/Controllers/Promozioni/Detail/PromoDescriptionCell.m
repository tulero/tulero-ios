//
//  PromoDescriptionCell.m
//  FirstProject
//
//  Created by Etinet on 15/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromoDescriptionCell.h"

@implementation PromoDescriptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configure:(NSDictionary*)elem {
    self.promoTitle.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"Titolo"]];
    self.promoDescription.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"Testo"]];
    self.promoDescription.translatesAutoresizingMaskIntoConstraints = NO;
    [self.promoDescription sizeToFit];
    
    [self.promoDescription layoutIfNeeded];
    
    self.promoTime.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"PROMO_TIME_DETAIL", @"PROMO_TIME_DETAIL")];
}

@end
