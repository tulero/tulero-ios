//
//  PromozioniVC.m
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromozioniVC.h"
#import "Settings.h"
#import "MembershipManager.h"
#import "PromotionCell.h"
#import "PromoDetailVC.h"

@interface PromozioniVC ()

@end

@implementation PromozioniVC{
    MembershipManager* _manager;
    NSArray* _list;
    BOOL _isLoaded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PromotionCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PromotionCell class])];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _manager = [MembershipManager sharedManager];
    _isLoaded = NO;
    [self initList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = NSLocalizedString(@"TITLE_PROMOTION", @"TITLE_PROMOTION");
    [APP_DELEGATE sendPageViewGA:@"Promotion"];
}

-(void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.title = @"";
}

-(void)initList{
    _list = @[];
    
    [KVNProgress show];
    
    [_manager getPromotions:^(NSArray *list) {
        [KVNProgress dismiss];
        _isLoaded = YES;
        NSMutableArray* tmpList = [[NSMutableArray alloc] init];
        NSLocale *locale = [NSLocale currentLocale];
        
        NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                                 value:[locale localeIdentifier]];
        //NSLog(@"LINGUA: %@",language);
        if([language isEqualToString:@"italiano (Italia)"]){
            for(NSDictionary* d in list){
                if([d[@"Language"] isEqualToString:@"ITA"]){
                    [tmpList addObject:d];
                }
            }
        } else {
            for(NSDictionary* d in list){
                if(![d[@"Language"] isEqualToString:@"ITA"]){
                    [tmpList addObject:d];
                }
            }
        }
       
        _list = [[NSArray alloc] initWithArray:tmpList];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView reloadData];
    
    } onFailure:^(NSError *error) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"GENERIC_ERROR", @"")];
        _isLoaded = YES;

    }];
    
}


#pragma tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    cell.textLabel.text = _list[indexPath.row][@"Titolo"];
    PromotionCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PromotionCell class]) forIndexPath:indexPath];
    [cell configure:_list[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 0){
        PromoDetailVC *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([PromoDetailVC class])];

        detail.promo = _list[indexPath.row];
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}


#pragma mark DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"promozioni_placeholder_grey_icon"];
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = NSLocalizedString(@"EMPTY_PROMOTION", @"EMPTY_PROMOTION");
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightMedium],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return self.tableView.backgroundColor;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"EMPTY_PROMOTION_DESCR", @"EMPTY_PROMOTION_DESCR");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                 NSForegroundColorAttributeName: COLOR(125, 131, 129, 1),
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView
{
    return YES;
}

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return _isLoaded;
}

@end
