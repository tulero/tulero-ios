//
//  PromotionCell.h
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *promotionTitle;
@property (strong, nonatomic) IBOutlet UILabel *promotionDescription;
@property (strong, nonatomic) IBOutlet UILabel *promotionTime;

-(void)configure:(NSDictionary*)elem;

@end
