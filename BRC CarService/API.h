//
//  API.h
//  BRC CarService
//
//  Created by Etinet on 28/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#ifndef API_h
#define API_h

#define GET_TOKEN @"/get_token" // GET
#define USER_LOGIN @"/login" // POST
#define PROMOTION_LIST @"/promozioni/:user_id" // GET
#define PRODUCTS_LIST @"/lista_prodotti/:user_id/0" //GET
#define FAVORITE_PRODUCTS_LIST @"/lista_prodotti/:user_id/1" //GET
#define LISTA_CATEGORIE_CATALOGO @"/lista_categorie_catalogo/:user_id" //GET
#define LISTA_CATEGORIE_ESPOSITORE @"/lista_categorie_espositore/:user_id" //GET
#define ORDERS_LIST @"/lista_ordini/:client_code" //GET
#define WORKSHOP_AGENT_LIST @"/officine_agente/:user_id" //GET
#define CREATION_USER @"/creazione_utente_ospite" //POST
#define INVENTARIO @"/inventario_espositore/:client_code" //GET
#define ADD_FAVORITE @"/add_preferito" //POST
#define REMOVE_FAVORITE @"/delete_preferito/:client_code" //POST
#define SEARCH_BRC_CODE @"/ricerca_codice_brc" //POST
#define SEARCH_EAN_CODE @"/ricerca_ean" //POST
#define CONFERMA_RETTIFICHE @"/inserimento_rettifiche" //POST
#define BUY_CATALOG @"/acquisto_catalogo" //POST
#define DOWNLOAD_EXPOSITOR @"/scarico_espositore" //POST
#define PASSWORD_RECOVERY @"/recupero_password" //POST
#define REORDER_VALUE @"/valore_riordino/:client_code" //GET

#define LOGIN_KROMEDA @"/CreateSessionKey/user/pwd" //GET
#define SEARCH_PLATE @"/CP_SearchPlate/':key'/':plate'/':lang'" //GET

#endif /* API_h */
