//
//  User.m
//  BRC CarService
//
//  Created by Etinet on 02/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "User.h"

@implementation User

-(instancetype)init{
    if(!self){
        self = [super init];
        
        self.username = @"";
        self.password = @"";
        self.mail = @"";
        // fixme: forse float
        self.valoremerceriordino = 0.0;
        self.alias = @"";
        self.codcliente = @"";
        self.codagente = @"";
        
        self.ntarghe = 0;
        self.isAgente = NO;
        self.isEspositore = NO;
        
        self.isLogged = NO;
        self.isRememberMe = NO;
        self.isGuest = NO;
    }
    
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary* )dict{
    self = [self init];
    
    self.username = dict[@"Username"];
    self.password = dict[@"Password"];
    self.mail = dict[@"Mail"];
    // fixme: forse float
    self.valoremerceriordino = [dict[@"ValoreMerceRiordino"] doubleValue];
    self.alias = dict[@"NomeAlias"];
    self.codcliente = [dict[@"CodCliente"] stringValue];
    self.codagente = dict[@"CodAgente"];
    
    
    //numero disponibili -> solo se > 0
    
    self.ntarghe = [dict[@"NTarghe"] intValue];
    self.isAgente = [dict[@"IsAgente"] boolValue];
    self.isEspositore = [dict[@"IsEspositore"] boolValue];
    
    self.isRememberMe = [dict[@"isRememberMe"] boolValue];
    // fixme!
    self.isLogged = NO;
    // fixme!
    self.isGuest = NO;
    
    return self;
}


@end
