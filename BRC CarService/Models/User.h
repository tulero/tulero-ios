//
//  User.h
//  BRC CarService
//
//  Created by Etinet on 02/02/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString* username;
@property (strong, nonatomic) NSString* password;
@property (strong, nonatomic) NSString* mail;
// fixme: forse float
@property double valoremerceriordino;
@property (strong, nonatomic) NSString* alias;
@property (strong, nonatomic) NSString* codcliente;
@property (strong, nonatomic) NSString* codagente;

//numero disponibili -> solo se > 0
@property int ntarghe;
@property BOOL isAgente;
@property BOOL isEspositore;
@property BOOL isGuest;

@property BOOL isRememberMe;
@property BOOL isLogged;

-(instancetype)initWithDictionary:(NSDictionary* )dict;
@end
