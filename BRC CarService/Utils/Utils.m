//
//  Utils.m
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "Utils.h"

@implementation Utils


+(NSString* )formatISODate:(NSString *)date toFormat:(NSString* )format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.zz"];
    
    NSDate* dateReal = [formatter dateFromString:date];
    
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:dateReal];

}

+(NSString* )formatISODateOrders:(NSString *)date toFormat:(NSString* )format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate* dateReal = [formatter dateFromString:date];
    
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:dateReal];
    
}


+(id)ObjectOrEmptyString:(id) object{
    if(object == [NSNull null]){
        return @"";
    }
    return object ? : @"";
}

+(id)Object:(id) object orPlaceholder:(id)placeholder{
    if(object == [NSNull null]){
        return placeholder;
    }
    return object ? : placeholder;
}


+(id)ObjectOrEmptyArray:(id) object{
    if(object == [NSNull null]){
        return @[];
    }
    return object ? : @[];
}

+(id)ObjectOrEmptyDictionary:(id) object{
    if(object == [NSNull null]){
        return @{};
    }
    return object ? : @{};
}

+(id)ObjectOrND:(id) object{
    if(object == [NSNull null]){
        return @"-";
    }
    return object ? : @"-";
}


@end
