//
//  Utils.h
//  BRC CarService
//
//  Created by Etinet on 29/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(NSString* )formatISODate:(NSString *)date toFormat:(NSString* )format;
+(NSString* )formatISODateOrders:(NSString *)date toFormat:(NSString* )format;

+(id)ObjectOrEmptyString:(id)object;
+(id)ObjectOrEmptyArray:(id)object;
+(id)ObjectOrEmptyDictionary:(id)object;
+(id)ObjectOrND:(id) object;
+(id)Object:(id) object orPlaceholder:(id)placeholder;

@end
